<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;
use App\Models\Wallet;
use App\Helper\FundUserWallet;
use Illuminate\Support\Facades\Log;
use Mail;

class ApiTransactionsTest extends TestCase
{
    protected $active_user;
    protected $active_token;

    public function setUp()
    {
        parent::setUp();
        $user = factory(User::class)->create([
                "phone" => "2349021202163",
                "is_phone_verified" => 1,
                "status" => 1
            ]);
        $this->active_token = JWTAuth::fromUser($user);
        $this->active_user = JWTAuth::setToken($this->active_token)->toUser();

    }
    /**
     * 1. Gets list of banks successfully
     * 
     * GET: /api/get-banks-list
     * 
     * @test
     */
    public function it_successfully_gets_list_of_banks()
    {
        $this->withHeaders([
            'Authorization' => 'Bearer '.$this->active_token
        ])->get('/api/get-bank-list')
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
    }
    /**
     * 2. Gets list of banks successfully
     * 
     * GET: /api/get-banks-list
     * 
     * @test
     */
    public function it_successfully_gets_all_users_transactions()
    {
        $test_cards = [
            [
                'number' => '4084084084084081',
                'cvv' => '408',
                'expiry_month' => '9',
                'expiry_year' => '20'
            ],
            [
                'number' => '50785078507850784',
                'cvv' => '884',
                'expiry_month' => '11',
                'expiry_year' => '18'
            ]
        ];
        foreach($test_cards as $card)
        {
            $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/add-user-card',$card)
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
        }
        $card = $this->active_user->user_cards()->first();
        
        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/fund-user-wallet',['card_id'=>$card->id,'amount'=>3400,'pin'=>$this->active_user->pin])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data"=> ['amount'],
                "status",
                "status_code"
            ]);

        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/fund-user-wallet',['card_id'=>$card->id,'amount'=>3300,'pin'=>$this->active_user->pin])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data"=> ['amount'],
                "status",
                "status_code"
            ]);
        $wallet = Wallet::where(['user_id' => $this->active_user->id])->first();
        $this->assertEquals(6700, $wallet->amount);

        $this->withHeaders([
            'Authorization' => 'Bearer '.$this->active_token
        ])->get('/api/get-recent-transactions')
            ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
    }

    /**
     * 3. Successfully Transfer to bank account
     *
     *  @test
     */
    public function it_successfully_transfers_to_bank_account()
    {
        $wallet = $this->active_user->wallet;
        $wallet->amount = 8000;
        $wallet->save();

        $this->withHeaders([
            'Authorization' => 'Bearer '.$this->active_token
        ])->post('/api/transfer-to-account', [
            'pin' => $this->active_user->pin,
            'amount'=> '2000',
            'bank_acct_no' => '0002053915',
            'bank_id' => 9
        ])->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);

        
    }
      
}
