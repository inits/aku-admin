<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;

class ApiAuthTest extends TestCase
{
    /**
     * 1.Test signup: 
     * POST /api/auth/sign_up.
     * 
     * @test
     *
     */
    public function it_signs_up_a_user()
    {
        $faker = $this->faker;

        $user = [
            'name' => $this->faker->name,
            'email' => $faker->unique()->safeEmail,
            'phone' => str_replace(' ','',$faker->unique()->phoneNumber),
            'pin' => random_int(1000,9999),
        ];
        $this->post('/api/auth/sign_up', $user)
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data"=>["access_token","token_type","expires_in","user"],
                "status",
                "status_code"
            ]);
    }
    /**
     * 2.Test Signup Failure: 
     * POST /api/auth/sign_up.
     * 
     * @test
     *
     */
    public function it_fails_to_sign_up_a_user()
    {
        $faker = $this->faker;

        $user = [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            // 'phone' => str_replace(' ','',$faker->unique()->phoneNumber),
            'pin' => random_int(1000,9999),
        ];
        $this->post('/api/auth/sign_up', $user)
            ->assertStatus(404)
            ->assertJsonStructure(["message","code","status","status_code"]);
    }

    /**
     * 3.Test USSD Signup: 
     * POST /api/auth/ussd/sign_up.
     * 
     * @test
     *
     */
    public function it_signs_up_a_user_via_ussd()
    {
        $faker = $this->faker;

        $user = [
            'phone' => str_replace(' ','',$faker->unique()->phoneNumber),
        ];
        $this->post('/api/auth/ussd/sign_up', $user)
            ->assertOk()
            ->assertJsonStructure(["message","data"=>["access_token","token_type","expires_in","user"],"status","status_code"]);
    }
    
    /**
     * 4. Test Unverified User Login: 
     * POST /api/auth/login.
     * 
     * @test
     *
     */
    public function it_prevents_unverified_user_login()
    {
        $user = factory(User::class)->create();
        
        $this->post('/api/auth/login', ['phone' => $user->phone,'pin' => $user->pin])
            ->assertJsonStructure([
                "message","code","status","status_code"
            ]);
    }


    /**
     * 5. Test Login Works: 
     * POST /api/auth/login.
     * 
     * @test
     *
     */
    public function it_allows_user_login()
    {
        $user = factory(User::class)->create();
        $user->update([
            "is_phone_verified" => 1,
            "status" => 1
        ]);
        $reply = $this->call(
            'POST',
            '/api/auth/login',
            ['phone' => $user->phone,'pin' => $user->pin], 
            [], //cookies
            [], // files
            [], // server
            []
        );
        $statusCode = json_decode($reply->getContent(),true)['status_code'];

        if($statusCode != '200'){
            Log::error(json_encode($reply->getContent()));
        }
        $reply->assertJsonStructure([
                "message","data","status","status_code"
            ]);
    }

    /**
     * 6. Test Send Otp Endpoint Works: 
     * POST /api/auth/send/otp.
     * 
     * @test
     *
     */

    public function it_sends_otp_with_token()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();

        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', '/api/auth/send/otp')
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data" => [
                    "sms_response",
                    "otp",
                    "phone"
                ],
                "status",
                "status_code"
            ]);

    }
    
    /**
     * 7. Test Send Otp Without Token: 
     * POST /api/auth/send-otp-without-token.
     * 
     * @test
     *
     */
    public function it_sends_otp_without_token()
    {
        $user = factory(User::class)->create();
        
        $this->post('/api/auth/send-otp-without-token',['phone'=> $user->phone])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data" => [
                    "sms_response",
                    "otp",
                    "phone"
                ],
                "status",
                "status_code"
            ]);

    }

    /**
     * 8. Test Verify Otp: 
     * POST /api/auth/verify/otp.
     * 
     * @test
     *
     */
    public function it_verifies_otp_given_phone()
    {
        $otp = str_shuffle("" . substr(time(), -6));

        $user = factory(User::class)->create(['current_otp' => $otp]);

        $this->post('/api/auth/verify/otp',['otp' => $otp, 'phone' => $user->phone])
                ->assertOk()
                ->assertJsonStructure([
                    "message",
                    "data",
                    "status",
                    "status_code"
                ]);

    }

     /**
     * 9. Test Verify Pin Endpoint: 
     * POST /api/auth/verify/pin.
     * 
     * @test
     *
     */
    public function it_verifies_user_pin()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();

        // $this->withExceptionHandling();
        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', '/api/auth/verify/pin',['pin'=> $user->pin])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);

    }
     /**
     * 10. Test Update Pin Endpoint: 
     * POST /api/auth/update_pin.
     * 
     * @test
     *
     */
    public function it_updates_user_pin()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();
        $new_pin = random_int(1000,9999);
        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', '/api/auth/update_pin',['new_pin'=> $new_pin])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);

    }

     /**
     * 11. Test Get User Endpoint: 
     * POST /api/auth/user.
     * 
     * @test
     *
     */
    public function it_gets_current_user()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();
        
        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('GET', '/api/auth/user')
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);

    }

     /**
     * 12. Test Get User Endpoint: 
     * POST /api/auth/user.
     * 
     * @test
     *
     */
    public function it_updates_current_user()
    {
        $user = factory(User::class)->create([
            "is_phone_verified" => 1,
            "status" => 1,
            "custom_email" => true
        ]);
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();
        $updateData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'pin' => $user->pin
        ];
        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', '/api/auth/update_profile',$updateData)
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);

    }

     /**
     * 13. Test Get User Endpoint: 
     * POST /api/auth/user.
     * 
     * @test
     *
     */
    public function it_fails_to_update_current_user_due_to_wrong_pin()
    {
        $user = factory(User::class)->create([
            "is_phone_verified" => 1,
            "status" => 1,
            "custom_email" => true
        ]);
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();
        $updateData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'pin' => 0000
        ];
        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', '/api/auth/update_profile',$updateData)
            // ->dump()
            ->assertStatus(400)
            ->assertJsonStructure([
                "message",
                "code",
                "status",
                "status_code"
            ]);

    }

    /**
     * 14. Test Get User Endpoint: 
     * POST /api/auth/user.
     * 
     * @test
     *
     */
    public function it_fails_to_update_current_user_due_to_unverified_phone()
    {
        $user = factory(User::class)->create([
            "custom_email" => true
        ]);
        
        $token = JWTAuth::fromUser($user);
        $user = JWTAuth::setToken($token)->toUser();
        $updateData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'pin' => $user->pin
        ];
        $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', '/api/auth/update_profile',$updateData)
            // ->dump()
            ->assertStatus(400)
            ->assertJsonStructure([
                "message",
                "code",
                "status",
                "status_code"
            ]);

    }
    
   
}