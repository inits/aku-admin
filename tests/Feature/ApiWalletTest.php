<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;
use App\Models\Wallet;
use App\Helper\FundUserWallet;
use Illuminate\Support\Facades\Log;

class ApiWalletTest extends TestCase
{
    protected $active_user;
    protected $active_token;

    public function setUp()
    {
        parent::setUp();
        $user = factory(User::class)->create([
                "is_phone_verified" => 1,
                "status" => 1
            ]);
        $this->active_token = JWTAuth::fromUser($user);
        $this->active_user = JWTAuth::setToken($this->active_token)->toUser();

    }
    /**
     * 1. Adds User Card
     * 
     * POST: /api/add-user-card
     * 
     * @test
     */
    public function logged_in_user_can_add_card()
    {
        $card = [
            'number' => '50785078507850784',
            'cvv' => '884',
            'expiry_month' => '11',
            'expiry_year' => '2018'
        ];
        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/add-user-card',$card)
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
    }
    /**
     * 2. Checks invalid input failure for add-user-cad enpoint
     * 
     * POST: /api/add-user-card
     * 
     * @test
     */
    public function add_card_fails_on_invalid_input()
    {
        $card = [
            'number' => '50785078507850784',
            // 'cvv' => '884',
            'expiry_month' => '11',
            'expiry_year' => '20'
        ];
        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/add-user-card',$card)
            ->assertJsonStructure([
                "message",
                "code",
                "status",
                "status_code"
            ])
            ->assertStatus(400);
    }

    /**
     * 3. Prevents an unauthorized user from adding a card
     * 
     * POST: /api/add-user-card
     * 
     * @test
     */
    public function unauthorized_user_cannot_add_card()
    {
        $user = factory(User::class)->make([
                "is_phone_verified" => 1,
                "status" => 1
            ]);
        $token = JWTAuth::fromUser($user);
        $card = [
            'number' => '50785078507850784',
            'cvv' => '884',
            'expiry_month' => '11',
            'expiry_year' => '2018'
        ];
        $this->withHeaders(['Authorization' => 'Bearer '.$token])
            ->post('/api/add-user-card',$card)
            ->assertStatus(401)
            ->assertJsonStructure([
                "message",
                "status",
                "status_code"
            ]);
    }


    /**
     * 4. Gets user's cards
     * 
     * GET: /api/get-user-cards
     * 
     * @test
     */
    public function user_can_get_cards()
    {
        $test_cards = [
            [
                'number' => '4084084084084081',
                'cvv' => '408',
                'expiry_month' => '9',
                'expiry_year' => '20'
            ],
            [
                'number' => '50785078507850784',
                'cvv' => '884',
                'expiry_month' => '11',
                'expiry_year' => '18'
            ]
        ];
        foreach($test_cards as $card)
        {
            $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/add-user-card',$card)
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
        }
        
        $this->withHeaders([
            'Authorization' => 'Bearer '.$this->active_token
        ])
        ->get('/api/get-user-cards')
        ->assertOk()
        ->assertJsonStructure([
            "message",
            "data",
            "status",
            "status_code"
        ]);
    }

    /**
     * 5. Funds User's Wallet
     * 
     * POST: /api/fund-user-wallet
     * 
     * @test
     */
    public function it_funds_a_users_wallet()
    {
        $test_cards = [
            [
                'number' => '4084084084084081',
                'cvv' => '408',
                'expiry_month' => '9',
                'expiry_year' => '20'
            ],
            [
                'number' => '50785078507850784',
                'cvv' => '884',
                'expiry_month' => '11',
                'expiry_year' => '18'
            ]
        ];
        foreach($test_cards as $card)
        {
            $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/add-user-card',$card)
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
        }
        $card = $this->active_user->user_cards()->first();
        
        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/fund-user-wallet',['card_id'=>$card->id,'amount'=>3400,'pin'=>$this->active_user->pin])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data"=> ['amount'],
                "status",
                "status_code"
            ]);

        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/fund-user-wallet',['card_id'=>$card->id,'amount'=>3300,'pin'=>$this->active_user->pin])
            // ->dump()
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data"=> ['amount'],
                "status",
                "status_code"
            ]);
        $wallet = Wallet::where(['user_id' => $this->active_user->id])->first();
        $this->assertEquals(6700, $wallet->amount);
    }

    /**
     * 5. Funds User's Wallet
     * 
     * POST: /api/fund-user-wallet
     * 
     * @test
     */
    public function fund_wallet_returns_validation_error_with_wrong_input()
    {
        $test_cards = [
            [
                'number' => '4084084084084081',
                'cvv' => '408',
                'expiry_month' => '9',
                'expiry_year' => '20'
            ],
            [
                'number' => '50785078507850784',
                'cvv' => '884',
                'expiry_month' => '11',
                'expiry_year' => '18'
            ]
        ];
        foreach($test_cards as $card)
        {
            $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/add-user-card',$card)
            ->assertOk()
            ->assertJsonStructure([
                "message",
                "data",
                "status",
                "status_code"
            ]);
        }
        $card = $this->active_user->user_cards()->first();
        
        $data = [
            'card_id'=>$card->id,
            'amount'=>400,
            'pin' => $this->active_user->php
        ];

        $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
            ->post('/api/fund-user-wallet',$data)
            ->assertStatus(400)
            ->assertJsonStructure([
                "message",
                "code",
                "status",
                "status_code"
            ]);

        // $this->withHeaders(['Authorization' => 'Bearer '.$this->active_token])
        //     ->post('/api/fund-user-wallet',['card_id'=>$card->id,'amount'=>3300,'pin'=>$this->active_user->pin])
        //     // ->dump()
        //     ->assertOk()
        //     ->assertJsonStructure([
        //         "message",
        //         "data"=> ['amount'],
        //         "status",
        //         "status_code"
        //     ]);
        $wallet = Wallet::where(['user_id' => $this->active_user->id])->first();
        $this->assertEquals(0, $wallet->amount);
    }

}
