<?php

namespace Tests\Feature;

use App\Classes\InterswitchTransactions;
use App\Models\InterswitchRequest;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InterswitchTransactionTest extends TestCase
{
    protected $active_user;
    protected $active_token;

    public function setUp()
    {
        parent::setUp();
        $user = factory(User::class)->create([
            "phone" => str_replace('-','',$this->faker->phoneNumber),
            "is_phone_verified" => 1,
            "status" => 1
        ]);
        $this->active_token = JWTAuth::fromUser($user);
        $this->active_user = JWTAuth::setToken($this->active_token)->toUser();

    }
    /**
     * 1. TTID length is always lessthan or equal to 9
     *
     * A basic test example.
     *
     * @test
     */
    public function ttid_lessthan_or_equal_to_9_digits()
    {
        $trans = new InterswitchTransactions($this->active_user, rand(1000, 100000));

        $this->assertTrue(strlen($trans->getTTID()) <= 9, 'String in not greater than 9');


    }

    /**
     * 2. OTP length not more than 4
     *
     * @test
     */
    public function pin_or_otp_not_more_than_4_digits()
    {
        $trans = new InterswitchTransactions($this->active_user, rand(1000, 100000));
        $pin = $trans->paycodeRequest()['oneTimePin'];
        $this->assertTrue(strlen($pin) <= 4, 'PIN Generate not more than 4 digits');
    }
    /**
     * 3. paymentMethodTypeCode is passed in request
     *
     * @test
     */
    public function paymentMethodTypeCode_is_in_request()
    {
        $trans = new InterswitchTransactions($this->active_user, rand(1000, 100000));
        $request = $trans->paycodeRequest();
        $this->assertTrue(isset($request['paymentMethodTypeCode']), 'PIN Generate not more than 4 digits');
    }
    /**
     * 4. paymentMethodCode is passed in request
     *
     * @test
     */
    public function paymentMethodCode_is_in_request()
    {
        $trans = new InterswitchTransactions($this->active_user, rand(1000, 100000));
        $request = $trans->paycodeRequest();
        $this->assertTrue(isset($request['paymentMethodCode']), 'paymentMethodCode is passed');
    }

    /**
     * 5. Amount is in kobo
     *
     * @test
     */
    public function amount_is_in_kobo()
    {
        $amount = rand(1000, 100000);
        $trans = new InterswitchTransactions($this->active_user, $amount);
        $request = $trans->paycodeRequest();
        $this->assertTrue($request['amount'] == $amount *100, 'Amount is in kobo');
    }

    /**
     * 6. Frontend Partner Exists In Request and is correct
     *
     * @test
     */
    public function rightFrontendPartnerExistsInRequest()
    {
        $amount = rand(1000, 100000);
        $trans = new InterswitchTransactions($this->active_user, $amount);
        $request = $trans->paycodeRequest();
        $this->assertTrue(isset($request['frontEndPartnerId']) && $request['frontEndPartnerId'] == "AKU", 'Right Front end partner Exists');
    }

    /**
     * 7. Generates paycode
     *
     * @test
     */
    public function gets_paycode()
    {
        $tr = new InterswitchTransactions($this->active_user, 4000);
        $response  = $tr->getPaycode();
        if(isset($response['data']))
        {
            $paycode = $response['data']['payWithMobileToken'];
        }else{
            ddd('trials');
        }
        $transaction = InterswitchRequest::where('reference_code', $paycode)->first();
        ddd($transaction);
        $tr2 = new InterswitchTransactions($this->active_user, null, $transaction->id);
        $this->assertArrayHasKey($tr2->getPaycodeStatus(), 'status', 'Nope');
    }

}
