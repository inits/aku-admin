<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Faker\Factory as Faker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $faker;

    public function setUp()
    {
        parent::setup();
        $this->faker = Faker::create();
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
