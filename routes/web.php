<?php
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// dd('iu');

Route::get('/', function () {
    // return redirect()->to(url('/swagger'));
    return view('welcome');
});
Route::get('/Artisan_command', function(){
    Artisan::call('clear-compiled');
    dump(Artisan::output());
    Artisan::call('migrate');
    dump(Artisan::output());
    return ['done'=> 'true'];
});
Auth::routes();

// Route::get('/', 'AdminController@index');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin_login');
Route::post('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin_logout');

Route::any('/user/edit/{user_id}', "UserController@user_edit");
Route::any('/user/update_pin', "UserController@update_pin");
Route::any('/user/upload_excel', "UserController@upload_excel");
Route::any('/user/uploaded_files', "UserController@uploaded_files");
Route::any('/user/uploaded_users', "UserController@uploaded_users");


Route::group(['prefix' => 'admin', 'middleware' => ['auth.admin', 'role:super-admin']], function () {
    Route::get('/roles', 'RoleController@index');
    Route::get('/permissions', 'PermissionController@index');
    Route::get('/role/{role}', 'RoleController@show')->name('role.show');
    Route::get('/users/{input?}', 'UserController@index')->name('user.list');

    Route::post('/filter_users/{input?}', 'UserController@filter_users')->name('users.filter');
    Route::get('/user/{user}', 'UserController@show')->name('user.show');
    Route::post('/delete_user/{user}', 'UserController@destroy')->name('user.delete');

//    Route::any('user/edit/{user}', "UserController@edit");
//    Route::get('/user/edit', "UserController@edit");

    //Route::get('/user/edit/{user_id}', "UserController@user_edit");

    Route::get('/filter_users/{user?}', 'UserController@filter_users');

    Route::get('/list', 'AdminController@all_admins')->name('admin.list');
    Route::get('/create', 'AdminController@create')->name('admin.create');
    Route::post('/save', 'AdminController@save')->name('admin.save');
    Route::get('/edit/{admin}', 'AdminController@edit');
    Route::post('/update/{admin}', 'AdminController@update')->name('admin.update');
    Route::get('/view/{admin}', 'AdminController@show')->name('admin.show');
    Route::post('/delete/{admin}', 'AdminController@destroy')->name('admin.delete');

    Route::get('/loans', 'LoanController@index')->name('loans.list');

    Route::get('/transactions', 'TransactionController@index')->name('transactions.list');
    Route::any('/sort_transactions', 'TransactionController@sort')->name('transactions.sort');
    Route::get('/bank_transactions', 'TransactionController@bankTransactions')->name('transaction.banks');

    Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
    Route::get('/tradermoni_settings', 'LoanProviderController@tradermoni');
    Route::post('/save_tradermoni_status', 'LoanProviderController@save_tradermoni_status');


});