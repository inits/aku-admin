<?php

namespace App\Http\Controllers\Api;

$api = app('Dingo\Api\Routing\Router');


$api->version('v1', function ($api) {


    $api->group([
        'namespace' => 'App\Http\Controllers\Api'
    ],
        function ($api) {


            $api->get('/', 'BaseController@index');

            $api->post('/CoreBanking/LoanDisbursement', 'LoanController@disburseLoan')->middleware('validateLoanProvider');

            $api->post('/auth/login', 'AuthController@login');
            $api->post('/auth/sign_up', 'AuthController@sign_up');
            $api->post('/auth/deleteUserCompletely', 'AuthController@deleteUserCompletely');
            $api->post('/auth/ussd/sign_up', 'AuthController@ussd_sign_up');
            $api->post('/auth/tradermoni_whitelisted', 'AuthController@tradermoni_whitelisted');


            $api->get('welcome', "BaseController@welcome");

            $api->post('/auth/send-otp-without-token', 'AuthController@send_otp_without_token');
            $api->post('/auth/verify/otp', 'AuthController@verifyOTP');
            $api->post('/auth/verify/pin', 'AuthController@verifyPIN');
            $api->post('/help/reset_pin', 'AuthController::@admin_reset_user_pin');
            
            $api->group([
                'middleware' => 'api.auth'
            ],
                function ($api) {
                    $api->post('/auth/update_pin', 'AuthController@updatePin');
                    $api->post('/auth/mobile/update_pin', 'AuthController@updatePinMobile');
                    $api->post('/auth/update_profile', 'AuthController@updateProfile');
                    $api->post('/auth/send/otp', 'AuthController@sendOTP');
                    $api->post('/auth/reset-pin', 'AuthController@resetPin');
                    $api->post('/add-user-card', 'WalletController@add_user_card');
                    $api->post('/fund-user-wallet', 'WalletController@fund_wallet');
                    $api->post('/delete-user-card', 'WalletController@delete_card');
                    $api->post('/transfer-to-account', 'TransactionController@transfer_to_account');
                    $api->post('/transfer-to-individual', 'TransactionController@transfer_to_individual');
                    $api->post('/generate-cardless-withdrawal', 'TransactionController@generate_cardless_withdrawal');
                    $api->post('/buy-airtime', 'TransactionController@buy_airtime');
                    $api->post('/activate-card', 'WalletController@activate_card');
                    $api->post('/auth/complete-ussd-signup', 'AuthController@complete_ussd_signup');

                    $api->get('/auth/user', [
                        'uses' => 'AuthController@getUser',
                        'as' => 'api.auth.user'
                    ]);

                    $api->get('/get-user-wallet', 'WalletController@get_user_wallet');
                    $api->get('/get-user-cards', 'WalletController@get_user_cards');
                    $api->get('/get-recent-transactions', 'TransactionController@get_recent_transactions');
                    $api->get('/get_recent_cardless_withdrawals', 'TransactionController@get_recent_cardless_withdrawals');

                    $api->patch('/auth/refresh', [
                        'uses' => 'AuthController@patchRefresh',
                        'as' => 'api.auth.refresh'
                    ]);
                    $api->delete('/auth/invalidate', [
                        'uses' => 'AuthController@deleteInvalidate',
                        'as' => 'api.auth.invalidate'
                    ]);
                    $api->get('/get-bank-list', 'WalletController@get_bank_list');
                    $api->post('/new_user/generate_pin', 'AuthController@generateNewUserPin');
                    $api->post('/get_atm_paycode', 'TransactionController@generate_paycode');
                    $api->post('/get_paycode_status', 'TransactionController@get_paycode_status');
                    $api->post('/cancel_paycode', 'TransactionController@cancel_paycode');
                });


        });

});