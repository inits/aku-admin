<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li>
        <a href="<?php echo route('admin.dashboard'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li><a href="<?php echo route('transactions.sort'); ?>"><i class="fa fa-circle-o"></i> Sort Transactions</a>
    <li><a href="<?php echo route('transactions.list'); ?>"><i class="fa fa-circle-o"></i> Transactions</a>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-user"></i>
            <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo route('admin.list'); ?>"><i class="fa fa-circle-o"></i> Admin list</a></li>
            <li><a href="<?php echo route('admin.create'); ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-user"></i>
            <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo route('user.list'); ?>"><i class="fa fa-circle-o"></i> User list</a></li>
<!--            <li><a href="--><?php //echo url('/user/uploaded_users'); ?><!--"><i class="fa fa-circle-o"></i> Uploaded Users list</a></li>-->
            <!-- <li><a href="<?php //echo route('user.create'); ?>"><i class="fa fa-circle-o"></i> Add New User</a></li> -->
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-file"></i>
            <span> File Upload </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo url('/user/upload_excel'); ?>"><i class="fa fa-file"></i> Upload File </a>
            <li><a href="<?php echo url('/user/uploaded_files'); ?>"><i class="fa fa-file"></i> Uploaded Files </a>
        </ul>
    </li>
    <li><a href="<?php echo url('/admin/settings'); ?>"><i class="fa fa-file"></i>Settings</a>


<!--    <li class="treeview">-->
<!--        <a href="#">-->
<!--            <i class="fa fa-table"></i> <span>Tables</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--        </a>-->
<!--        <ul class="treeview-menu">-->
<!--            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>-->
<!--            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>-->
<!--        </ul>-->
<!--    </li>-->
<!--    <li class="treeview">-->
<!--        <a href="#">-->
<!--            <i class="fa fa-share"></i> <span>Multilevel</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--        </a>-->
<!--        <ul class="treeview-menu">-->
<!--            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>-->
<!--            <li class="treeview">-->
<!--                <a href="#"><i class="fa fa-circle-o"></i> Level One-->
<!--                    <span class="pull-right-container">-->
<!--                  <i class="fa fa-angle-left pull-right"></i>-->
<!--                </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>-->
<!--                    <li class="treeview">-->
<!--                        <a href="#"><i class="fa fa-circle-o"></i> Level Two-->
<!--                            <span class="pull-right-container">-->
<!--                      <i class="fa fa-angle-left pull-right"></i>-->
<!--                    </span>-->
<!--                        </a>-->
<!--                        <ul class="treeview-menu">-->
<!--                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>-->
<!--                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>-->
<!--        </ul>-->
<!--    </li>-->