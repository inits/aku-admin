@extends('layouts.admin_layout')

@section('content_title','All Loans')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Loans</li>
        <li class="active">All</li>
    </ol>
@endsection

@section('main_content')

<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Loans</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <form id="filter_form" class="form-group pull-right">  
                    <div class="search">
                        <input type="text" id="loans_search" class="searchTerm" placeholder="Search?">
                        <button type="submit" class="searchButton">
                          <i class="fa fa-search"></i>
                        </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div id="loans_list">
                @include('loans.ajax_table',['loans' => $loans]);
            </div>
            <!-- /.box-header -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
      </div>


@endsection
@section('additional_js')
<script>
var mainurl = "{{route('loans.list')}}";
$(document).on('submit', '#filter_form', (e) => {
    e.preventDefault();
    var input = $("#loans_search").val();
    var url = mainurl+`?search_input=${input}`;
    filterResult(url, "#loans_list");
    return;
})
$(document).on('click','.page-link', function (e){
  e.preventDefault();
  var input = $("#loans_search").val();
  if(input){
    var url = `{{route('loans.list')}}/${input}`;
    filterResult(url, "#loans_list", $(this).attr('href'));
  }else{
    filterResult(mainurl, "#loans_list", $(this).attr('href'));
  }
});
  
</script>
@endsection