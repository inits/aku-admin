            <div class="box-body">
                <?php //echo $admins->perPage().'|'.$admins->currentPage(); ?>
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Provider</th>
                  <th>Recipient</th>
                  <th>Amount</th>
				  <th>Status</th>
                  <!-- <th style="width: 150px">Action</th> -->
                </tr>
                @foreach($loans as $k => $loan)
                <tr>
                  <td>{{$k + 1}}.</td>
                  <td>{{$loan->name}}</td>
                  <td>{{$loan->phone}}</td>
                  <td>{{$loan->amount}}</td>
                  <td>{{$loan->status}}</td>
                 
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                {{$loans->links()}}
              </ul>
            </div>