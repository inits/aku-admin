@extends('layouts.admin_layout')

@section('content_title','All Transactions')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Transactions</li>
        <li class="active">All</li>
    </ol>
@endsection

@section('main_content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Transactions</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <form id="filter_form" class="form-group pull-right">
                                <div class="search">
                                    <input type="text" id="transactions_search" name="search_input" class="searchTerm"
                                           placeholder="Search?">
                                    <button type="submit" class="searchButton">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="transactions_list">
                    @include('transactions.ajax_table',['transactions' => $transactions])
                </div>
                <!-- /.box-header -->
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
    </div>


@endsection
@section('additional_js')
    <script>
        var mainurl = "{{route('transactions.list')}}";
        $(document).on('submit', '#filter_form', (e) => {
            e.preventDefault();
        var input = $("#transactions_search").val();
        var url = mainurl + '?search_input=' + input;
        filterResult(url, "#transactions_list");
        return;
        })
        ;
        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            var input = $("#transactions_search").val();
            if (input) {
                var url = '{{route('transactions.list')}}?search_input=' + input;
                var page = $(this).attr('href').split('?');
                filterResult(url, "#transactions_list", page[1]);
            } else {
                var page = $(this).attr('href').split('?');
                filterResult(mainurl, "#transactions_list", page[1]);
            }
        });

    </script>
@endsection