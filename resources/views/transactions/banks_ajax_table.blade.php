            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Reference</th>
                  <th>User</th>
                  <th>Amount</th>
				  <th>Status</th>
				  <th>Message</th>
                </tr>
                @foreach($transactions as $k => $transaction)
                <tr>
                  <td>{{$transaction->reference}}</td>
                  <td>{{$transaction->phone}}</td>
                  <td>{{$transaction->amount}}</td>
                  <td>{{$transaction->status}}</td>
                  <td>{{$transaction->message}}</td>
                 
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                {{$transactions->links()}}
              </ul>
            </div>