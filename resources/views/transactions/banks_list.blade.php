@extends('layouts.admin_layout')

@section('content_title','All Bank Transactions')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Transactions</li>
        <li class="active">All</li>
    </ol>
@endsection

@section('main_content')

<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Transactions</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <form id="filter_form" class="form-group pull-right">  
                    <div class="search">
                        <input type="text" id="transactions_search" name="search_input" class="searchTerm" placeholder="Search?">
                        <button type="submit" class="searchButton">
                          <i class="fa fa-search"></i>
                        </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div id="transactions_list">
                @include('transactions.ajax_table',['transactions' => $transactions])
            </div>
            <!-- /.box-header -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
      </div>


@endsection
@section('additional_js')
<script>
var mainurl = "{{route('transaction.banks')}}";
$(document).on('submit', '#filter_form', (e) => {
    e.preventDefault();
    var input = $("#transactions_search").val();
    var url = mainurl+`?search_input=${input}`;
    filterResult(url, "#transactions_list");
    return;
})
$(document).on('click','.page-link', function (e){
  e.preventDefault();
  var input = $("#transactions_search").val();
  if(input){
    var url = `{{route('transactions.list')}}/${input}`;

    filterResult(url, "#transactions_list", $(this).attr('href'));
  }else{
    filterResult(mainurl, "#loans_list", $(this).attr('href'));
  }
});
  
</script>
@endsection