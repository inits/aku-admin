@extends('layouts.admin_layout')

@section('content_title','Sort Transactions')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li> Sort Transactions</li>
        <li class="active">Sort</li>
    </ol>
@endsection

@section('main_content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sort Transactions</h3>

                    <div class="box-body">

                        <div class="row">

                            <div class="col-md-3"></div>
                            <div class="col-md-6 offset-md-3">
                                <div class="input-daterange">
                                    <h4> Select Date Range </h4>

                                    <form accept-charset="UTF-8" action="{{url()->current()}}" method="post"
                                          role="POST">
                                        {{ csrf_field() }}
                                        <div class="input-daterange">
                                            <div class="row">

                                                <div class="col-md-5 text-center">
                                                    <div class="card bg-white">
                                                        <div class="card-block">
                                                            <div id="datepicker" data-autoclose="false"
                                                                 class="search-datepicker datepicker-one"
                                                                 data-date="<?php echo $datepickerFormatCurrentMonth; ?>"></div>
                                                            <input id="from" type="hidden" name="from"
                                                                   value="<?php echo $datepickerFormatCurrentMonth; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-2"></div>

                                                <div class="col-md-5 text-center">

                                                    <div class="card bg-white">
                                                        <div class="card-block">
                                                            <div id="datepicker2"
                                                                 class="search-datepicker datepicker-two"
                                                                 data-date="<?php echo $datepickerFormatCurrentDayTime; ?>"></div>
                                                            <input id="to" type="hidden" name="to"
                                                                   value="<?php echo $datepickerFormatCurrentDayTime; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <hr>
                                        <button type="submit" class="btn btn-primary btn-round block the_end">
                                            Sort Report
                                        </button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.box-header -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="text-center -left col-md-12">
                        <div>
                            <h3>
                                <small> Transaction Report from <br></small>
                                <?php echo Date(
                                    'F jS, Y',
                                    strtotime($currentMonth)); ?>
                                -
                                <?php echo Date(
                                    'F jS, Y',
                                    strtotime($currentDayTime)); ?>
                            </h3>
                        </div>
                    </div>

                    <div class="tec">
                        <div class="row">
                            <?php

                            $itemSet = [];
                            foreach ($wallletlogsByType as $k => $item) {
                                $itemSet[$k] = $item;
                            }

                            foreach ($transactionDetails as $keyName => $transactionDetail) { ?>
                            <?php
                            if (array_key_exists($keyName, $itemSet)) {
                            //                             dd($transactionDetails);
                            //                             dd($keyName);
                            //                             dd($itemSet);
                            ?>
                            <div class="col-sm-2 text-center">
                                <div class="description-block border-right">
                                    <h5 class="description-header"
                                        style="font-size: 30px;"> {{ number_format($itemSet[$keyName]) }}</h5>
                                    <span class="description-text"> {{$transactionDetail}}
                                        <span>TRANSACTIONS</span> </span>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="col-sm-2">
                                <div class="description-block border-right">
                                    <h5 class="description-header"
                                        style="font-size: 30px;"> 0 </h5>
                                    <span class="description-text"> {{$transactionDetail}}
                                        <span>TRANSACTIONS</span> </span>
                                </div>
                            </div>
                            <?php } ?>

                            <?php } ?>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="description-block border-right">
                                <h5 class="description-header"
                                    style="font-size: 30px;"> {{$cardLessWithdrawCreated}} </h5>
                                <span class="description-text"> Card-less Withdraws </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="description-block border-right">
                                <h5 class="description-header"
                                    style="font-size: 30px;"> <?php echo NGN; ?> {{number_format($allCardlessWithdrawAmount)}} </h5>
                                <span class="description-text"> Card-less Withdraws Amount </span>
                            </div>
                        </div>
                    </div>

                    <!-- ./box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    {{--<span class="description-percentage text-green"><i--}}
                                    {{--class="fa fa-caret-up"></i> 17%</span>--}}
                                    <h5 class="description-header"
                                        style="font-size: 30px;"> <?php echo NGN; ?> {{ number_format($amountPaidTotal)  }} </h5>
                                    <span class="description-text"> Successful Transactions </span>
                                </div>
                            </div>
                            <!-- /.col -->


                            <div class="col-sm-2">
                                <div class="description-block border-right">
                                    {{--<span class="description-percentage text-yellow"><i--}}
                                    {{--class="fa fa-caret-left"></i> 0%</span>--}}
                                    <h5 class="description-header"
                                        style="font-size: 30px;"> {{ count($failedRawLgas) }} </h5>
                                    <span class="description-text"> Failed Transactions </span>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-2">
                                <div class="description-block border-right">
                                    {{--<span class="description-percentage text-green"><i--}}
                                    {{--class="fa fa-caret-up"></i> 20%</span>--}}
                                    <h5 class="description-header" style="font-size: 30px;"> {{$users}}</h5>
                                    <span class="description-text">Users</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    {{--<span class="description-percentage text-green"><i--}}
                                    {{--class="fa fa-caret-up"></i> 20%</span>--}}
                                    <h5 class="description-header"
                                        style="font-size: 30px;"> <?php echo NGN; ?> {{ number_format($amountToPay['amount_to_paid'])  }}</h5>
                                    <span class="description-text"> Total Amount In Wallet </span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="description-block border-right">
                                    <h5 class="description-header"
                                        style="font-size: 30px;"> {{ number_format($amountToPay['count'])  }}</h5>
                                    <span class="description-text"> Wallet </span>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->

                </div>
            </div>
        </div>


        <script>

            $('#datepicker').datepicker();
            $('#datepicker2').datepicker();
            //
            //    $('.input-daterange input').each(function () {
            //        $(this).datepicker('clearDates');
            //    });

            $('#datepicker').on('changeDate', function () {
                $('#from').val(
                    $('#datepicker').datepicker('getFormattedDate')
                );
            });
            $('#datepicker2').on('changeDate', function () {
                $('#to').val(
                    $('#datepicker2').datepicker('getFormattedDate')
                );
            });

//            $('#ReportsReportType').on('change', function () {
//                var value = $('#ReportsReportType :selected').text();
//                console.log(value);
//                if (value == 'Trip Reports') {
//                    $('#more_filter').show();
//                } else {
//                    $('#more_filter').hide();
//                }
//                if (value == 'Self Trip Reports') {
//                    $('#more_driver_filter').show();
//                } else {
//                    $('#more_driver_filter').hide();
//                }
//            });

        </script>

@endsection