<div class="box-body">
    <table class="table table-bordered">
        <tr>
            <th>Reference</th>
            <th>Details</th>
            <th>User</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Message</th>
            <th>created</th>
            <th>action</th>
        </tr>
        @foreach($transactions as $k => $transaction)
            <tr>
                <td>{{$transaction->response_code}}</td>
                <td>{{$transaction->name}}</td>
                <td>{{$transaction->phone}}</td>
                <td><?php echo NGN; ?>{{ $transaction->amount}}</td>
                <td>
                    @if($transaction->response_type == 0)
                        <span class="btn btn-danger btn-xs">Failed </span>
                    @else
                        <span class="btn btn-success btn-xs">Success</span>
                    @endif
                </td>
                <td>{{$transaction->message}}</td>
                <td>{{$transaction->created_at}}</td>
                <td>
                    {{--<button>{{$transaction->metadata}}</button>--}}
                </td>
            </tr>
        @endforeach
    </table>
</div>
<!-- /.box-body -->
<div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
        {{$transactions->links()}}
    </ul>
</div>