<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aku Dashbord | Log in</title>

  @component('components.main_styles')
    
    <link rel="stylesheet" href="{{asset('/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="{{asset('/css/login_styles.css')}}?v={{rand(10,99)}}">

  @endcomponent
</head>
<body class="hold-transition login-page">
@yield('main_content')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="{{asset('/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
