@extends('layouts.admin_layout')

@section('content_title','Add New Admin')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="'/'"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Admin</li>
        <li class="active">Add New</li>
    </ol>
@endsection

@section('main_content')

    <div class="row">
        <!-- right column -->
        <div class="col-md-8">
            <!-- Horizontal Form -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="create_admin" method="post" action="{{route('admin.save')}}"
                      enctype="multipart/form-data">
                    <div class="box-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="form-group">
                            @csrf
                            <label for="name" class="col-sm-2 control-label">Name</label>

                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}"
                                       placeholder="Name">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback text-danger" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                                       value="{{old('email')}}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback text-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role" class="col-sm-2 control-label">Role</label>

                            <div class="col-sm-10">
                                <select name="role" class="form-control" id="role">
                                    <option>Select Role</option>
                                    @foreach($roles as $k =>$role)
                                        <option value="{{$k}}">{{$role}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback text-danger" role="alert">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="avatar" class="col-sm-2 control-label">Avatar</label>

                            <div class="col-sm-10">
                                <input type="file" name="avatar" class="form-control pull-left" style="width: 40%"
                                       id="avatar" value="{{old('avatar')}}" placeholder="Password">
                                <div style="width: 50%" class="pull-right">
                                    <img alt="Avatar Preview" id="avatar_preview" style="display:none;" height="70px"/>
                                </div>
                                @if ($errors->has('avatar'))
                                    <span class="invalid-feedback text-danger" role="alert">
                            <strong>{{ $errors->first('avatar') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>

                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="password"
                                       placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="c_password" class="col-sm-2 control-label">Confirm Password</label>

                            <div class="col-sm-10">
                                <input type="password" name="password_confirmation" class="form-control" id="c_password"
                                       placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary pull-right col-md-4">Submit</button>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button onclick="return resetForm('#create_admin');" class="btn btn-default">Reset</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
@endsection
@section('additional_js')
    <script>
        $('#avatar').change(function () {
            readURL(this, '#avatar_preview');
        })
    </script>
@endsection
