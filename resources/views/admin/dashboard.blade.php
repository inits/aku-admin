@extends('layouts.admin_layout')

@section('content_title','Dashboard')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="'/'"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
@endsection

@section('main_content')


    <script src="{{ asset('js/highcharts.js') }}"></script>
    <script src="{{ asset('js/exporting.js') }}"></script>
    <script src="{{ asset('js/export-data.js') }}"></script>

    <style>
        text.highcharts-credits {
            display: none !important;
        }

        #container {
            font-family: Avenir, Helvetica, Arial, sans-serif !important;
        }

        .highcharts-exporting-group {
            display: none !important;
        }

        .row-equal {
            display: table;
            width: 100%;
            background: #ddd;
            padding: 18px;
            border-radius: 4px;
            color: #333;
        }

        .row-equal .text-muted {
            color: #47535c;
        }
    </style>

    <script>
        $(document).ready(function () {
            var currentYear = "<?php echo $currentYear; ?>";

            Highcharts.chart('highchartsHolder', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Transaction Activity for ' + currentYear
                },
//            subtitle: {
//                text: 'Source: WorldClimate.com'
//            },
                xAxis: {
                    categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                },
                yAxis: {
                    title: {
                        text: 'Wallet Transaction'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Successful Transactions',
                    data: {{$walletTransactionData}}
                }, {
                    name: 'Failed Transactions',
                    data: {{$failedRawLgas}}
                }]
            });
        });
    </script>


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <br>
                <br>
                <div class="box-body">
                    <div>
                        <div class="chart dashboard-line labels-white"
                             style="height: auto; padding: 0px; position: relative;">
                            <div id="highchartsHolder" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>

                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                {{--<span class="description-percentage text-green"><i--}}
                                {{--class="fa fa-caret-up"></i> 17%</span>--}}
                                <h5 class="description-header"
                                    style="font-size: 30px;"> <?php echo NGN; ?> {{ number_format($amountPaidTotal)  }} </h5>
                                <span class="description-text"> Successful Transactions </span>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-2">
                            <div class="description-block border-right">
                                {{--<span class="description-percentage text-yellow"><i--}}
                                {{--class="fa fa-caret-left"></i> 0%</span>--}}
                                <h5 class="description-header"
                                    style="font-size: 30px;"> {{ $failedCount }} </h5>
                                <span class="description-text"> Failed Transactions </span>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-2">
                            <div class="description-block border-right">
                                {{--<span class="description-percentage text-green"><i--}}
                                {{--class="fa fa-caret-up"></i> 20%</span>--}}
                                <h5 class="description-header" style="font-size: 30px;"> {{$users}}</h5>
                                <span class="description-text">Users</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                {{--<span class="description-percentage text-green"><i--}}
                                {{--class="fa fa-caret-up"></i> 20%</span>--}}
                                <h5 class="description-header"
                                    style="font-size: 30px;"> <?php echo NGN; ?> {{ number_format($amountToPay['amount_to_paid'])  }}</h5>
                                <span class="description-text"> Total Amount In Wallet </span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="description-block border-right">
                                <h5 class="description-header"
                                    style="font-size: 30px;"> {{ number_format($amountToPay['count'])  }}</h5>
                                <span class="description-text"> Wallet </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->

            <div class="tec">
                <div class="row">
                    <?php

                    $itemSet = [];
                    foreach ($wallletlogsByType as $k => $item) {
                        $itemSet[$k] = $item;
                    }

                    foreach ($transactionDetails as $keyName => $transactionDetail) { ?>
                    <?php
                         if (array_key_exists($keyName, $itemSet)) {
//                             dd($transactionDetails);
//                             dd($keyName);
//                             dd($itemSet);
                    ?>
                    <div class="col-sm-2">
                        <div class="description-block border-right">
                            <h5 class="description-header" style="font-size: 30px;"> {{ number_format($itemSet[$keyName]) }}</h5>
                            <span class="description-text"> {{$transactionDetail}} <span>TRANSACTIONS</span> </span>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="col-sm-2">
                        <div class="description-block border-right">
                            <h5 class="description-header"
                                style="font-size: 30px;"> 0 </h5>
                            <span class="description-text"> {{$transactionDetail}} <span>TRANSACTIONS</span> </span>
                        </div>
                    </div>
                    <?php } ?>

                    <?php } ?>
                </div>
            </div>


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@endsection