@extends('layouts.admin_layout')

@section('content_title','All Admins')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Admin</li>
        <li class="active">All</li>
    </ol>
@endsection

@section('main_content')

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Bordered Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php //echo $admins->perPage().'|'.$admins->currentPage(); ?>
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th style="width: 150px">Action</th>
                </tr>
                 @foreach($admins as $k => $admin)
                <tr>
                  <td>{{$k + 1}}.</td>
                  <td>{{$admin->name}}</td>
                  <td>{{$admin->email}}</td>
                  <td>{{$admin->getRoles()}}</td>
                  <td>
                      <a class="badge bg-light-blue" href="{{url('/admin/edit/'.$admin->id)}}" style="margin-right: 2em">Edit</a>
                      <a class="badge bg-red" href="{{route('admin.delete',['admin' => $admin->id])}}" onclick="alertBeforeDelete('{{route('admin.delete',['admin' => $admin->id])}}')">Delete</a></td>
                </tr>
                  @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                {{$admins->links()}}
              </ul>
            </div>
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
      </div>

@endsection