@extends('layouts.admin_layout')

@section('content_title','All Users')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active">Edit</li>
    </ol>
@endsection

@section('main_content')

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"> User Details </h3>
                </div>
                <div class="box-body">

                    <form accept-charset="UTF-8" action="{{url()->current()}}" method="post" role="POST">
                        {{ csrf_field() }}
                        <fieldset>

                            <input name="user_id" value="{{$userDetail['id']}}" type="hidden"/>

                            <div class="form-group">
                                <label class="control-label" for="name"> Full Name </label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Full Name" value="{{$userDetail['name']}}"
                                       data-validation="required"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="email"> Email Address </label>
                                <input type="text" class="form-control" id="name" name="email"
                                       placeholder="Email Address" value="{{$userDetail['email']}}"
                                       data-validation="email"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="phone"> Phone Number </label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Phone Number" value="{{$userDetail['phone']}}" readonly
                                       data-validation="required"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="status"> Status </label>
                                <select class="form-control" id="status" name="status"
                                        data-validation="required" style="width: 100%;">
                                    <?php
                                    $status = [
                                        '1' => 'Active',
                                        '2' => 'Deactivated',
                                    ];
                                    foreach ($status as $key => $statu) { ?>
                                    <option value="<?php echo $key; ?>" <?php if ($key === $userDetail['status']) {
                                        echo "selected = 'selected' ";
                                    } ?> ><?php echo $statu ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <?php if($userDetail['is_phone_verified'] === 0 ) { ?>
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Alert:</span>
                                User phone number not verified
                            </div>
                            <?php } else { ?>
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only"> Success:</span>
                                User phone number not verified
                            </div>
                            <?php } ?>

                            <h4>
                                <small>Account Created</small>
                                <br>
                                <?php echo date('', strtotime($userDetail['created_at'])) ?>
                            </h4>

                        </fieldset>
                        <button type="submit" class="btn btn-primary btn-round block the_end"> Update User Details
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-body">

                    <div>
                        <h4 style="
    font-size: 60px;
    letter-spacing: 47px;
    text-align: center;
    border-radius: 4px;
    background: #ddd;
    margin: 20px;
    padding: 20px 0;
"><?php echo $userDetail['current_otp']; ?></h4>
                    </div>

                    <form accept-charset="UTF-8" action="{{url('/user/update_pin')}}" method="post" role="POST">
                        {{ csrf_field() }}
                        <input name="user_id" value="{{$userDetail['id']}}" type="hidden"/>

                        <fieldset>
                            <div class="form-group">
                                <input type="number" value="{{$userDetail['current_otp']}}" class="form-control"
                                       id="current_otp" name="current_otp"
                                       data-validation="required"/>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary btn-round block the_end"> Update User OTP
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection