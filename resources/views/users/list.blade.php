@extends('layouts.admin_layout')

@section('content_title','All Users')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active">All</li>
    </ol>
@endsection

@section('main_content')

    <div class="row" id="userRow">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users List</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <form id="filter_form" class="form-group pull-right">
                                <div class="search">
                                    <input type="text" id="users_search" class="searchTerm" placeholder="Search?">
                                    <button type="submit" class="searchButton">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="users_list">
                    @include('users.ajax_table',['users' => $users]);
                    <div>
                    </div>
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
        </div>
        @endsection

        @section('additional_js')
            <script>
                var mainurl = "{{route('users.filter')}}";
                $(document).on('submit', '#filter_form', (e) => {
                    e.preventDefault();
                var input = $("#users_search").val();
                var url = mainurl + `/${input}`;
                filterResult(url, "#users_list");
                return;
                })
                $(document).on('click', '.page-link', function (e) {
                    e.preventDefault();
                    var input = $("#users_search").val();
                    if (input) {
                        var url = `{{route('users.filter')}}/${input}`;
                        page = $(this).attr('href').split('?');
                        filterResult(url, "#users_list", page);
                    } else {
                        page = $(this).attr('href').split('?');
                        filterResult(mainurl, "#users_list", page[1]);
                    }
                });

            </script>
@endsection