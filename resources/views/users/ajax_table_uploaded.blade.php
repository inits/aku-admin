<!-- /.box-header -->
<div class="box-body">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th style="width: 150px">Action</th>
        </tr>
        </thead>
        <tbody>
        @if($users)
            @foreach($users as $k => $user)
                <tr>
                    <td>{{$k + 1}}.</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>
                        {{--<a class="badge bg-light-blue" href="{{url('/user/edit/'.$user->id)}}"--}}
                           {{--style="margin-right: 2em">Edit</a>--}}
                        {{--<a class="badge bg-red" href="{{route('user.delete',['user' => $user->id])}}"--}}
                           {{--onclick="alertBeforeDelete('{{route('user.delete',['user' => $user->id])}}')">Delete</a>--}}
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="6"> No Results Found</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<!-- /.box-body -->
<div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
        {{$users->links()}}
    </ul>
</div>