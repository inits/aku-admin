@extends('layouts.admin_layout')

@section('content_title','All Users')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active">All</li>
    </ol>
@endsection

@section('main_content')

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 offset-md-3">
            <div class="box">
                <div class="box-body">
                    <div id="message_box"></div>
                    <form accept-charset="UTF-8" enctype="multipart/form-data" action="{{url()->current()}}"
                          method="post" role="POST" id="data">
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="form-group">
                                <label class="control-label" for="name"> Upload Excel File </label>
                                <input type="file" name="file" class="form-control" id="docData"
                                       data-validation="required"/>
                            </div>
                        </fieldset>

                        <button type="submit" id="submitbtn" class="btn btn-primary btn-round block the_end"> Upload doc
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script>

//        $("form#data").submit(function (e) {
//            e.preventDefault();
//            $('#submitbtn').hide();
//            var formData = new FormData(this);
//            var form = this;
//
//            var file_data = $('#docData').prop('files')[0];
//            $('#loader_holder').show();
//
//            if (typeof file_data === 'undefined'){
//                $('#message_box').html('Empty file. please try again');
//                endIt();
//                return;
//            }
//
//            if (file_data.type !== "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
//                $('#message_box').html('Sorry you are trying to upload a non-supported file formant');
//                endIt();
//                return;
//            }
//
//            $('#message_box').hide();
//
//            $.ajax({
//                url: $(form).prop("action"),
//                type: 'POST',
//                data: formData,
//                xhr: function() {
//                    var myXhr = $.ajaxSettings.xhr();
//                    if(myXhr.upload){
//                        myXhr.upload.addEventListener('progress',progress, false);
//                    }
//                    return myXhr;
//                },
//                cache:false,
//                contentType: false,
//                processData: false,
//                dataType: 'html',
//                success: function (data) {
//                    console.log(data);
//                    endIt();
//                },
//            });
//        });
//
//        function progress(e){
//
//            if(e.lengthComputable){
//                var max = e.total;
//                var current = e.loaded;
//
//                var Percentage = (current * 100)/max;
//                console.log(Percentage);
//
//
//                if(Percentage >= 100)
//                {
//                    // process completed
//                }
//            }
//        }
//
//        function endIt() {
//            $('#loader_holder').hide();
//            $('#submitbtn').show();
//        }

    </script>

@endsection