@extends('layouts.guest_layout')

@section('main_content')
<div class="login-box tab-content">
  <div class="login-logo">
    <a href="{{asset('/dashboard')}}"><img src="{{asset('images/aku.svg')}}" height="60px"/> <b>Aku</b></a>
  </div>
  <div class="login-box-body tab-pane fade in active" id="login_box">
    <p class="login-box-msg">{{ __('Reset Password') }}</p>

    <form method="post" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
      <div class="form-group has-feedback">                        
        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ __('E-Mail Address') }}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="invalid-feedback text-danger" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ __('Password') }}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="invalid-feedback text-danger" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password_confirmation" required placeholder="{{ __('Confirm Password') }}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Reset Password') }}</button>
        </div>
        <!-- /.col -->
      </div>
    </form><br>
  </div>
</div>
@endsection
