@extends('layouts.guest_layout')

@section('main_content')
<?php

  $login_active = ' in active';
  $forgot_active = '';
  if (session('status')){
    $login_active = '';
    $forgot_active = ' in active';
  }
?>
<div class="login-box tab-content">
  <div class="login-logo">
    <a href="{{asset('/dashboard')}}"><img src="{{asset('images/aku.svg')}}" height="60px"/> <b>Aku</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body tab-pane fade{{$login_active}}" id="login_box">
    <p class="login-box-msg">Sign in to start your session</p>

    <form method="post" action="{{url('/admin_login') }}" aria-label="{{ __('Login') }}">
        @csrf
      <div class="form-group has-feedback">                        
        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ __('E-Mail Address') }}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="invalid-feedback text-danger" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ __('Password') }}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="invalid-feedback text-danger" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Login') }}</button>
        </div>
        <!-- /.col -->
      </div>
    </form><br>
    <!-- /.social-auth-links -->

    <a href="#forgot_password_box" data-toggle="tab" class="text-warning">{{ __('Forgot Your Password?') }}</a><br>
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <div class="login-box-body tab-pane fade{{$forgot_active}}" id="forgot_password_box">
    <p class="login-box-msg">{{ __('Reset Password') }}</p>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <form method="post" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <div class="form-group has-feedback">                        
            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ __('E-Mail Address') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.col -->
        <div class="col-xs-12" style="margin-bottom: 2em;">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Send Password Reset Link') }}</button>
        </div>
    </form>
    <!-- /.social-auth-links -->

    <a href="#login_box" data-toggle="tab" class="text-warning">{{ __('Back to login') }}</a>
  </div>
  <!-- /.login-box-body -->
</div>
@endsection
