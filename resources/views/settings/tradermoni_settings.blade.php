@extends('layouts.admin_layout')

@section('content_title','All Transactions')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="url('/dashbord')"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">TraderMoni Settings</li>
    </ol>
@endsection

@section('page_styles')
{{--<link rel="stylesheet" href="{{asset('css/tradermoni_settings.css')}}">--}}


@section('main_content')
    <div class="row">
        @if(Session::has('error'))
        <div class="alert-danger">
            {{Session::get('error')}}
        </div>
        @endif
        @if(Session::has('success'))
            <div class="alert-success">
                {{Session::get('success')}}
            </div>
        @endif

            <div class="col-md-12"  style="align-content: center; display: flex;">
            <form action="{{url('/admin/save_tradermoni_status')}}"  method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="tradermoni_service">Tradermoni Service Toggle</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="tradermoni_service" id="optionsRadios2" <?php if($tradermoni->status == 'on'){echo 'checked';}?>  value="on">ON
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="tradermoni_service" id="optionsRadios2" <?php if($tradermoni->status == 'off'){echo 'checked';}?> value="off">OFF
                        </label>
                    </div>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
