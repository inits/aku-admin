<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\Role;
use App\Models\Permission;

class AdminsRolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //First Populate the permissions table
        
        //  1) Create Admin Role
        $role = [
            'name' => 'super-admin',
            'guard_name' => 'admin',
            'display_name' => 'Super Admin',
            'description' => 'Full Permission'
        ];
        $role = Role::create($role);

        //2) Set Role Permissions
        // Get all permission, swift through and attach them to the role
        $permission = Permission::get();
        foreach ($permission as $key => $value) {
            $role->givePermissionTo($value);
        }
        
        // 3) Create Admin User And Assign User Role
        $admins = [
            [
                'name' => 'Victoria Etim',
                'email' => 'victoria@initsng.com',
                'password' => bcrypt('admin@initng')
            ],
            [
                'name' => 'Second Admin',
                'email' => 'femi@initsng.com',
                'password' => bcrypt('akuadmin12')
            ],
            [
                'name' => 'Olatunde Dominic',
                'email' => 'hurlatunde@gmail.com',
                'password' => bcrypt('pa55word')
            ]
        ];
        foreach($admins as $admin)
        {
            $user = Admin::create($admin);
            $user->guard_name = 'admin';
            $user->assignRole($role);
            
        }
    }
}
