<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BanksTableSeeder::class,
            StatesTableSeeder::class,
            TransactionTypesTableSeeder::class,
            PermisionsTableSeeder::class,
            AdminsRolesAndPermissionsTableSeeder::class,
            LoanStatusTableSeeder::class,
        ]);
    }
}
