<?php

use Illuminate\Database\Seeder;
use App\Models\LoanStatus;

class LoanStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loan_statuses =  [
            [ 
                'value' => 0,
                'name' => 'Loan Unpaid',
                'description' => ''
            ],
            [ 
                'value' => 1,
                'name' => 'Loan Paid',
                'description' => ''
            ],
            [ 
                'value' => 2,
                'name' => 'Loan Processing',
                'description' => ''
            ],
            [ 
                'value' => 3,
                'name' => 'loan Failed',
                'description' => ''
            ]
        ];
        foreach($loan_statuses as $ls)
        {
            LoanStatus::create($ls);
        }
    }
}
