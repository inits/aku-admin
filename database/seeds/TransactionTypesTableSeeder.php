<?php

use Illuminate\Database\Seeder;

class TransactionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_types')->insert([
            ['name' => 'Bank transfer', 'description' => 'Transfer to bank account', 'account_type' => 'debit'],
            ['name' => 'Failed Bank Transfer', 'description' =>'Failed transfer to bank account', 'account_type' => 'debit'],
            ['name' => 'Wallet Transfer', 'description' => 'Transfered to another wallet', 'account_type' => 'debit'],
            ['name' => 'Fund Wallet', 'description' => 'Fund wallet', 'account_type' => 'credit'],
            ['name' => 'Cardless Withdrawal', 'description' => 'Cardless Transaction Code Generated', 'account_type' => 'debit'],
            ['name' => 'Airtime Purchase', 'description' => 'Airtime purchased from wallet balance', 'account_type' => 'debit'],
            ['name' => 'Loan Received', 'description' => 'Received loan', 'account_type' => 'credit'],
            ['name' => 'Loan Paid', 'description' => 'Paid back loan', 'account_type' => 'debit'],
            ['name' => 'Loan Request', 'description' => 'Requested a loan', 'account_type' => ''],
            ['name' => 'Check Withrawal Code Status', 'description' => 'Get the status of a withdrawal code', 'account_type' => ''],
            ['name' => 'Cancel Cardless Withdrawal', 'description' => 'Cancel a cardless withdrawal request', 'account_type' => 'credit']
        ]);
    }
}
