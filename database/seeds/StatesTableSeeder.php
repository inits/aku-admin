<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            ['name' => 'Kano  '], 
            ['name' => 'Lagos  '], 
            ['name' => 'Kaduna  '], 
            ['name' => 'Katsina  '], 
            ['name' => 'Oyo  '], 
            ['name' => 'Rivers  '], 
            ['name' => 'Bauchi  '], 
            ['name' => 'Jigawa  '], 
            ['name' => 'Benue  '], 
            ['name' => 'Anambra  '], 
            ['name' => 'Borno  '], 
            ['name' => 'Delta  '], 
            ['name' => 'Imo  '], 
            ['name' => 'Niger  '], 
            ['name' => 'Akwa Ibom'], 
            ['name' => 'Ogun  '], 
            ['name' => 'Sokoto  '], 
            ['name' => 'Ondo  '], 
            ['name' => 'Osun  '], 
            ['name' => 'Kogi  '], 
            ['name' => 'Zamfara  '], 
            ['name' => 'Enugu  '], 
            ['name' => 'Kebbi  '], 
            ['name' => 'Edo  '], 
            ['name' => 'Plateau  ']
        ]);
    }
}
