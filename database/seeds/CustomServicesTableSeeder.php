<?php

use Illuminate\Database\Seeder;

class CustomServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CustomService::create([
            'name' => 'tradermoni',
            'status' => 'on',
            'description' => 'Disbursement of federal government loans to Market Women and Petti Traders'
        ]);
    }
}
