<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'name' => 'create-role',
                'display_name' => 'Create Role',
                'description' => 'Create New Role'
            ],
            [
                'name' => 'list-roles',
                'display_name' => 'Display Role Listing',
                'description' => 'List All Roles'
            ],
            [
                'name' => 'update-role',
                'display_name' => 'Update Role',
                'description' => 'Update Role Information'
            ],
            [
                'name' => 'delete-role',
                'display_name' => 'Delete Role',
                'description' => 'Delete A Role'
            ],
            [
                'name' => 'create-user',
                'display_name' => 'Create User',
                'description' => 'Create New User'
            ],
            [
                'name' => 'list-users',
                'display_name' => 'Display User Listing',
                'description' => 'List All Users'
            ],
            [
                'name' => 'update-user',
                'display_name' => 'Update User',
                'description' => 'Update User Information'
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Delete User',
                'description' => 'Delete A User'
            ],
            [
                'name' => 'deactivate-user',
                'display_name' => 'Deactivate User',
                'description' => 'Deactivate A User'
            ],
            [
                'name' => 'create-admin',
                'display_name' => 'Create Admin',
                'description' => 'Create New Admin'
            ],
            [
                'name' => 'list-admins',
                'display_name' => 'Display Admin Listing',
                'description' => 'List All Admins'
            ],
            [
                'name' => 'update-admin',
                'display_name' => 'Update Admin',
                'description' => 'Update Admin Information'
            ],
            [
                'name' => 'delete-admin',
                'display_name' => 'Delete Admin',
                'description' => 'Delete An Admin'
            ],
            [
                'name' => 'create-wallet',
                'display_name' => 'Create Admin',
                'description' => 'Create New Admin'
            ],
            [
                'name' => 'list-wallets',
                'display_name' => 'Display Wallet Listing',
                'description' => 'List All Wallets'
            ],
            [
                'name' => 'update-wallet',
                'display_name' => 'Update Wallet',
                'description' => 'Update Wallet Information'
            ],
            [
                'name' => 'delete-wallet',
                'display_name' => 'Delete Wallet',
                'description' => 'Delete A Wallet'
            ],
            [
                'name' => 'list-apilogs',
                'display_name' => 'Display ApiLog Listing',
                'description' => 'List All ApiLogs'
            ]
        ];
        foreach ($permission as $key => $value) {
            $value['guard_name'] = 'admin';
            Permission::create($value);
        }
    }
}
