<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            ['name' => 'Access Bank', 'sort_code' => '044150149', 'bank_code' => '044'],
            ['name' => 'Citibank Nigeria', 'sort_code' => '023150005', 'bank_code' => '023'],
            ['name' => 'Diamond Bank', 'sort_code' => '063150162', 'bank_code' => '063'],
            ['name' => 'Ecobank Nigeria', 'sort_code' => '050150010', 'bank_code' => '050'],
            ['name' => 'Enterprise Bank', 'sort_code' => '084150015', 'bank_code' => '084'],
            ['name' => 'Fidelity Bank', 'sort_code' => '070150003', 'bank_code' => '070'],
            ['name' => 'First Bank of Nigeria', 'sort_code' => '011151003', 'bank_code' => '011'],
            ['name' => 'First City Monument Bank', 'sort_code' => '214150018', 'bank_code' => '214'],
            ['name' => 'Guaranty Trust Bank', 'sort_code' => '058152036', 'bank_code' => '058'],
            ['name' => 'Heritage Bank', 'sort_code' => '030159992', 'bank_code' => '030'],
            ['name' => 'Keystone Bank', 'sort_code' => '082150017', 'bank_code' => '082'],
            ['name' => 'MainStreet Bank', 'sort_code' => '014150331', 'bank_code' => '014'],
            ['name' => 'Skye Bank', 'sort_code' => '076151006', 'bank_code' => '076'],
            ['name' => 'Stanbic IBTC Bank', 'sort_code' => '221159522', 'bank_code' => '221'],
            ['name' => 'Standard Chartered Bank', 'sort_code' => '068150015','bank_code' => '068'],
            ['name' => 'Sterling Bank', 'sort_code' => '232150016', 'bank_code' => '232'],
            ['name' => 'Union Bank of Nigeria', 'sort_code' => '032080474', 'bank_code' => '032'],
            ['name' => 'United Bank For Africa', 'sort_code' => '033153513', 'bank_code' => '033'],
            ['name' => 'Unity Bank', 'sort_code' => '215154097', 'bank_code' => '215'],
            ['name' => 'Wema Bank', 'sort_code' => '035150103', 'bank_code' => '035'],
            ['name' => 'Zenith Bank', 'sort_code' => '057150013', 'bank_code' => '057'],
            ['name' => 'Jaiz Bank', 'sort_code' => '301080020', 'bank_code' => '301'],
            ['name' => 'Suntrust Bank', 'sort_code' => NULL, 'bank_code' => '100'],
            ['name' => 'Providus Bank', 'sort_code' => NULL, 'bank_code' => '101'],
            ['name' => 'Parallex Bank', 'sort_code' => NULL, 'bank_code' => '526'],
        ]);
    }
}
