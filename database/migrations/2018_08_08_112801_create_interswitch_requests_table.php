<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterswitchRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('interswitch_requests')) {

            Schema::create('interswitch_requests', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('url')->nullable();
                $table->string('amount')->nullable();
                $table->string('reference_code')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->string('code_status')->nullable();
                $table->string('interswitch_code_status')->nullable();
                $table->text('request_meta')->nullable();
                $table->text('response_meta')->nullable();
                $table->integer('transaction_type_id')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interswitch_requests');
    }
}
