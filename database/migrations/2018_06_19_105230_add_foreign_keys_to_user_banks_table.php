<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserBanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::table('user_banks', function(Blueprint $table)
		{
			if(!foreignKeyExists('user_banks','bank_id')){
				$table->foreign('bank_id', 'user_banks_ibfk_1')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('NO ACTION');
				$table->foreign('user_id', 'user_banks_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
			}
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_banks', function(Blueprint $table)
		{
			$table->dropForeign('user_banks_ibfk_1');
			$table->dropForeign('user_banks_ibfk_2');
		});
	}

}
