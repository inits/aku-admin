<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('transaction_types')){
			Schema::create('transaction_types', function(Blueprint $table)
			{
				$table->integer('id', true);
				$table->string('name', 100);
				$table->string('description');
				$table->timestamps();
			});
		}		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaction_types');
	}

}
