<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiryDateFieldInCardlessWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('cardless_withdrawals','expiry_date')){
            Schema::table('cardless_withdrawals', function (Blueprint $table) {
                $table->dateTime('expiry_date')->after('paycode')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cardless_withdrawals', function (Blueprint $table) {
            $table->dropColumn('expiry_date');
        });
    }
}
