<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaycodeColumnsToCardlessWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cardless_withdrawals', function (Blueprint $table) {
            $table->string('paycode')->nullable();
            $table->string('url')->nullable();
            $table->string('provider_channel')->comment("paga, interswitch")->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('code_status')->nullable();
            $table->string('provider_code_status')->nullable();
            $table->text('request_meta')->nullable();
            $table->text('response_meta')->nullable();
            $table->dateTime('created_at')->after('response_meta')->useCurrent();
            $table->dateTime('updated_at')->after('created_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cardless_withdrawals', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('provider_channel');
            $table->dropColumn('status');
            $table->dropColumn('code_status');
            $table->dropColumn('provider_code_status');
            $table->dropColumn('request_meta');
            $table->dropColumn('response_meta');
        });
    }
}
