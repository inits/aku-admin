<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sms_providers')){
            Schema::create('sms_providers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->tinyInteger('status')->default(0);
                $table->string('function_name')->nullable();
                $table->datetime('created_at')->useCurrent();
                $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_providers');
    }
}
