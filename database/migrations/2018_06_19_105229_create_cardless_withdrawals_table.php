<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCardlessWithdrawalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('cardless_withdrawals')){
		    Schema::create('cardless_withdrawals', function(Blueprint $table)
            {
                $table->integer('id', true);
                $table->integer('user_id')->nullable()->index('user_id');
                $table->string('reference_code')->nullable()->unique('reference_code');
                $table->integer('amount')->nullable();
                $table->integer('charge')->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cardless_withdrawals');
	}

}
