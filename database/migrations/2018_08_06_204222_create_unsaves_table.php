<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnsavesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('unsaves'))
			Schema::create('unsaves', function(Blueprint $table)
			{
				$table->integer('id', true);
				$table->string('phone', 55)->nullable();
				$table->text('reason', 65535)->nullable();
				$table->timestamps();
			});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unsaves');
	}

}
