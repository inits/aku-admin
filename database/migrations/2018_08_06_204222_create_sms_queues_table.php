<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSmsQueuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sms_queues', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('phone', 50)->nullable();
			$table->text('message', 65535)->nullable();
			$table->boolean('sent')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sms_queues');
	}

}
