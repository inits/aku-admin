<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToWalletTransactionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('wallet_transaction_logs', function(Blueprint $table)
		{
			if(!foreignKeyExists('wallet_transaction_logs', 'transaction_type_id'))
			$table->foreign('transaction_type_id', 'wallet_transaction_logs_ibfk_1')->references('id')->on('transaction_types')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('user_id', 'wallet_transaction_logs_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('wallet_transaction_logs', function(Blueprint $table)
		{
			$table->dropForeign('wallet_transaction_logs_ibfk_1');
			$table->dropForeign('wallet_transaction_logs_ibfk_2');
		});
	}

}
