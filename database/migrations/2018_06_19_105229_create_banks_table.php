<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('banks')){
		    Schema::create('banks', function(Blueprint $table)
            {
                $table->integer('id', true);
                $table->string('name', 100);
                $table->string('sort_code', 100)->nullable();
                $table->string('bank_code', 100)->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banks');
	}

}
