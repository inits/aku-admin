<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('wallets')){
			Schema::create('wallets', function(Blueprint $table)
			{
				$table->integer('id', true);
				$table->integer('user_id')->unique()->index('user_id');
				$table->integer('amount');
				$table->timestamps();
			});
		}		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wallets');
	}

}
