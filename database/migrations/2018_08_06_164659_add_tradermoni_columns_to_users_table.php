<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTradermoniColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users','clean_market_location')) {

            Schema::table('users', function (Blueprint $table) {
                $table->string('clean_market_location')->nullable()->after('current_otp');
                $table->string('state')->nullable()->after('clean_market_location');
                $table->string('data_source')->nullable()->after('state');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('clean_market_location');
            $table->dropColumn('state');
            $table->dropColumn('data_source');
        });
    }
}
