<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('bank_transfers')){
            Schema::create('bank_transfers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('bank_id');
                $table->double('amount');
                $table->string('bank_acct_no');
                $table->integer('status')->description('1-success, 2-failed, 3-pending, 4-processing')->default(3);
                $table->text('meta')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_transfers');
    }
}
