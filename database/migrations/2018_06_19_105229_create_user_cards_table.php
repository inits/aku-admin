<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserCardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('user_cards')){
			Schema::create('user_cards', function(Blueprint $table)
			{
				$table->increments('id');
				$table->integer('user_id')->index('user_id');
				$table->string('authorization_code');
				$table->string('card_type');
				$table->string('last4');
				$table->string('exp_month');
				$table->string('exp_year');
				$table->string('bin');
				$table->string('bank');
				$table->string('channel');
				$table->string('signature');
				$table->boolean('reusable');
				$table->string('country_code');
				$table->smallInteger('status');
				$table->timestamps();
			});
		}		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_cards');
	}

}
