<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ModifySmsQueueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sms_queue', function(Blueprint $table)
		{
			$table->string('phone', 50)->nullable()->change();
			$table->text('message', 65535)->nullable()->change();
			$table->boolean('sent')->nullable()->default(0)->change();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sms_queues');
	}

}
