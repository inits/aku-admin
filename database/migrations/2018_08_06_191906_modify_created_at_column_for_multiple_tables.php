<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCreatedAtColumnForMultipleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table) {
            $table->datetime('created_at')->useCurrent()->change();
            $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'))->change();
        });

        Schema::table('wallets',function(Blueprint $table) {
            $table->datetime('created_at')->useCurrent()->change();
            $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'))->change();
        });

        Schema::table('wallet_transaction_logs',function(Blueprint $table) {
            $table->datetime('created_at')->useCurrent()->change();
            $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'))->change();
        });

        Schema::table('user_cards',function(Blueprint $table) {
            $table->datetime('created_at')->useCurrent()->change();
            $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'))->change();
        });

        Schema::table('transaction_logs',function(Blueprint $table) {
            $table->datetime('created_at')->useCurrent()->change();
            $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'))->change();
        });

        Schema::table('sms_queue',function(Blueprint $table) {
            $table->datetime('created_at')->useCurrent()->change();
            $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'))->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
