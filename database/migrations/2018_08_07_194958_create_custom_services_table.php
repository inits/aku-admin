<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('custom_services')){
            Schema::create('custom_services', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('status')->default('on');
                $table->text('description')->nullable();
                $table->text('metadata')->nullable();
                $table->datetime('created_at')->useCurrent();
                $table->datetime('updated_at')->useCurrent()->onUpdate( DB::raw('now()::timestamp(0)'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_services');
    }
}
