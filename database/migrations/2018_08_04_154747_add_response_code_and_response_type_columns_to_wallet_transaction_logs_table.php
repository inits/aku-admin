<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResponseCodeAndResponseTypeColumnsToWalletTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('wallet_transaction_logs', 'response_code')){
            Schema::table('wallet_transaction_logs', function (Blueprint $table) {
                $table->string('response_code')->nullable();
                $table->boolean('response_type')->nullable()->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wallet_transaction_logs', function (Blueprint $table) {
            //
        });
    }
}
