<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaction_logs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('transaction_type_id')->nullable()->index('campaign_id');
			$table->integer('user_id')->nullable()->index('user_id');
			$table->string('amount', 100)->nullable();
			$table->string('currency', 20)->nullable();
			$table->string('transaction_date', 50)->nullable();
			$table->text('status')->nullable();
			$table->string('reference', 50)->nullable();
			$table->string('domain', 30)->nullable();
			$table->text('metadata')->nullable();
			$table->string('gateway_response', 50)->nullable();
			$table->string('message')->nullable();
			$table->string('channel', 20)->nullable();
			$table->string('ip_address', 200)->nullable();
			$table->string('log', 20)->nullable();
			$table->string('fees', 20)->nullable();
			$table->text('authorization', 65535)->nullable();
			$table->text('customer', 65535)->nullable();
			$table->string('plan', 20)->nullable();
			$table->string('integration')->nullable();
			$table->string('source')->nullable();
			$table->string('reason')->nullable();
			$table->string('recipient')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaction_logs');
	}

}
