<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTransactionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_logs', function(Blueprint $table)
		{
			$table->foreign('transaction_type_id', 'transaction_logs_ibfk_1')->references('id')->on('transaction_types')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('user_id', 'transaction_logs_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_logs', function(Blueprint $table)
		{
			$table->dropForeign('transaction_logs_ibfk_1');
			$table->dropForeign('transaction_logs_ibfk_2');
		});
	}

}
