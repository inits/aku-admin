<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_activities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('heading')->nullable();
			$table->string('tag')->nullable();
			$table->text('message', 65535)->nullable();
			$table->smallInteger('status')->nullable()->default(0);
			$table->integer('user_id')->nullable()->index('user_id');
			$table->boolean('activity_flag')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_activities');
	}

}
