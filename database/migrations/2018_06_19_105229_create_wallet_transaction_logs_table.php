<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletTransactionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('wallet_transaction_logs')){
			Schema::create('wallet_transaction_logs', function(Blueprint $table)
			{
				$table->integer('id', true);
				$table->integer('transaction_type_id')->nullable()->index('campaign_id');
				$table->integer('user_id')->nullable()->index('user_id');
				$table->string('amount', 100)->nullable();
				$table->text('metadata')->nullable();
				$table->string('header')->nullable();
				$table->text('message', 65535)->nullable();
				$table->timestamps();
			});
		}					
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wallet_transaction_logs');
	}

}
