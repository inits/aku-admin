<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancelledPaycodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('cancelled_paycodes')){
            Schema::create('cancelled_paycodes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cardless_withdrawal_id');
                $table->tinyInteger('status')->default(3);
                $table->text('meta');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancelled_paycodes');
    }
}
