<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('loan_providers')){
            Schema::create('loan_providers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('username');
                $table->string('password');
                $table->string('email')->nullable();
                $table->string('address')->nullable();
                $table->string('website')->nullable();
                $table->string('phone')->nullable();
                $table->integer('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_providers');
    }
}
