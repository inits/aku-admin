$(document).ready(function() {
    // alert('oops');
    $(".sidebar-menu.tree li a").each(function (x, link) {
        if ($(this).attr('href') == window.location.href) {
            $(this).parent('li').addClass('active');
            $(this).parents('ul.treeview-menu').show();
            $(this).parents('.treeview').addClass('menu-open');
        }
    })
})

function readURL(input, preview_id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(preview_id).attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function dangerlert(message,confirmButton,cancelButton,callback){
    swal({
        title: "Are you sure?",
        text: message,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: confirmButton,
        cancelButtonText: cancelButton,
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            callback();
        }
    });
}
function filterResult(url, container, page = null) {
    var url = page == null ? url : url+'?'+page;
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function (data) {
        $(container).html(data);
    }).fail(function (error) {
        throw new Error('Unable to return result:', error);
    });
}