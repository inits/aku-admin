<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unsave extends Model
{
    //
    protected $fillable = [
        'phone',
        'reason',
    ];


    //
    public static function saveQueue($phone, $message) {
        Unsave::create([
            'phone' => $phone,
            'reason' => $message,
        ]);
    }

}
