<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TransactionLog
 *
 * @property int $id
 * @property int $transaction_type_id
 * @property int $user_id
 * @property string $amount
 * @property string $currency
 * @property string $transaction_date
 * @property string $status
 * @property string $reference
 * @property string $domain
 * @property string $metadata
 * @property string $gateway_response
 * @property string $message
 * @property string $channel
 * @property string $ip_address
 * @property string $log
 * @property string $fees
 * @property string $authorization
 * @property string $customer
 * @property string $plan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\TransactionType $transaction_type
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class TransactionLog extends Eloquent
{
    protected $casts = [
        'transaction_type_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'transaction_type_id',
        'user_id',
        'amount',
        'currency',
        'transaction_date',
        'status',
        'reference',
        'domain',
        'metadata',
        'gateway_response',
        'message',
        'channel',
        'ip_address',
        'log',
        'fees',
        'authorization',
        'customer',
        'plan',
        'integration',
        'source',
        'reason',
        'recipient',
    ];

    public function transaction_type()
    {
        return $this->belongsTo(\App\Models\TransactionType::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
