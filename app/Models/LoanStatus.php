<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Loan;

class LoanStatus extends Model
{
    protected $table = 'loan_status';
    
    protected $fillable = [
        'name', 'value','description'
    ];

    public function loans()
    {
        return $this->hasMany(Loan::class, 'status', 'value');
    }

    public function value()
    {
        return $this->value;
    }
}
