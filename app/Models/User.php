<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $pin
 * @property string $phone
 * @property int $status
 * @property int $current_otp
 * @property int $is_phone_verified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $transaction_logs
 * @property \Illuminate\Database\Eloquent\Collection $user_activities
 * @property \Illuminate\Database\Eloquent\Collection $banks
 * @property \Illuminate\Database\Eloquent\Collection $user_cards
 * @property \Illuminate\Database\Eloquent\Collection $wallet_transaction_logs
 * @property \Illuminate\Database\Eloquent\Collection $wallets
 *
 * @package App\Models
 */

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $casts = [
        'status' => 'int',
        'current_otp' => 'int',
        'is_phone_verified' => 'int',
        'custom_email' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'email',
        'pin',
        'phone',
        'status',
        'current_otp',
        'is_phone_verified',
        'custom_email',
        'tradermoni_whitelisted'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pin', 'remember_token',
    ];


    public function transaction_logs()
    {
        return $this->hasMany(\App\Models\TransactionLog::class);
    }

    public function user_activities()
    {
        return $this->hasMany(\App\Models\UserActivity::class);
    }

    public function cardless_withdrawals()
    {
        return $this->hasMany(CardlessWithdrawal::class);
    }

    public function banks()
    {
        return $this->belongsToMany(\App\Models\Bank::class, 'user_banks')
            ->withPivot('id', 'account_number', 'account_name', 'recipient_code', 'created', 'modified');
    }

    public function user_cards()
    {
        return $this->hasMany(\App\Models\UserCard::class);
    }

    public function wallet_transaction_logs()
    {
        return $this->hasMany(\App\Models\WalletTransactionLog::class);
    }

    public function wallet()
    {
        return $this->hasOne(\App\Models\Wallet::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function loans(){
        return $this->hasMany(Loan::class);
    }
}
