<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserCard
 * 
 * @property int $id
 * @property int $user_id
 * @property string $authorization_code
 * @property string $card_type
 * @property string $last4
 * @property string $exp_month
 * @property string $exp_year
 * @property string $bin
 * @property string $bank
 * @property string $channel
 * @property string $signature
 * @property bool $reusable
 * @property string $country_code
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserCard extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'reusable' => 'bool',
		'status' => 'int'
	];

	protected $fillable = [
		'user_id',
		'authorization_code',
		'card_type',
		'last4',
		'exp_month',
		'exp_year',
		'bin',
		'bank',
		'channel',
		'signature',
		'reusable',
		'country_code',
		'status'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
