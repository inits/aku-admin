<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsQueue extends Model
{
    protected $fillable = [
        'message',
        'phone',
        'sent'
    ];


    //
    public static function saveQueue($phone, $message) {
        SmsQueue::create([
            'phone' => $phone,
            'message' => $message,
        ]);
    }

}
