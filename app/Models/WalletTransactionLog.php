<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use App\Helper\Meta;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class WalletTransactionLog
 * 
 * @property int $id
 * @property int $transaction_type_id
 * @property int $user_id
 * @property string $amount
 * @property string $metadata
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\TransactionType $transaction_type
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class WalletTransactionLog extends Eloquent
{
	protected $casts = [
		'transaction_type_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'transaction_type_id',
		'user_id',
		'amount',
		'response_code',
		'response_type',
		'metadata',
        'message'
	];

	public function transaction_type()
	{
		return $this->belongsTo(\App\Models\TransactionType::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

    public static function createWalletTransaction($dataSent)
    {
        WalletTransactionLog::create([
            'response_code' => $dataSent['response_code'],
            'response_type' => $dataSent['response_type'],
            'user_id' => $dataSent['user_id'],
            'message' => $dataSent['message'],
            'transaction_type_id' => $dataSent['transaction_type_id'],
            'amount' => $dataSent['amount'],
            'metadata' => $dataSent['metadata'],
        ]);
    }

    public static function bankTransfers()
    {
        return WalletTransactionLog::whereIn('transaction_type_id', [Meta::TRANSFER_MONEY_OUT_ACTION, Meta::TRANSFER_MONEY_OUT_FAILED_ACTION])->get();
    }

    public static function cardlessWithdrawals()
    {
        return WalletTransactionLog::where('transaction_type_id', Meta::WITHDRAWAL_CODE_GENERATED)->get();
    }

    public static function airtimePurchases()
    {
        return WalletTransactionLog::where('transaction_type_id', Meta::BUY_AIRTIME)->get();
    }
}
