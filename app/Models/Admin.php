<?php

namespace App\Models;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class Admin extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use HasRoles;

    protected $guard = 'admin';
    protected $guard_name = 'admin';
    protected $table = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'avatar',
        'password',
    ];

    protected $hidden = [
        'password', 
        'remember_token',
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getRoles()
    {
        return implode(
            ",",
            $this->getRoleNames()->toArray()
        );
    }
}
