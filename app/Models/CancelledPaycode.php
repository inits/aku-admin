<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CancelledPaycode extends Model
{
    protected $fillable = [
        'cardless_withdrawal_id', 'status','meta',
    ];
}
