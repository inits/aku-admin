<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomService extends Model
{
    protected $fillable = [
        "name", "description", "status", "next_live", "metadata"
    ];

    public function switch_off()
    {
        $this->status = 'off';
        $this->save();
        return $this;
    }

    public function switch_on()
    {
        $this->status = 'on';
        $this->save();
        return $this;
    }
}
