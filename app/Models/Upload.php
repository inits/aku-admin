<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    //
    protected $fillable = [
        'upload_name',
        'process',
    ];

    public static function updateUploadQueue($id, $message, $process)
    {
        Upload::where('id', $id)->update([
            'message' => $message,
            'process' => $process,
        ]);
    }
}
