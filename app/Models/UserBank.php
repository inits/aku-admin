<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserBank
 * 
 * @property int $id
 * @property int $user_id
 * @property int $bank_id
 * @property string $account_number
 * @property string $account_name
 * @property string $recipient_code
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * 
 * @property \App\Models\Bank $bank
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserBank extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'bank_id' => 'int'
	];

	protected $dates = [
		'created',
		'modified'
	];

	protected $fillable = [
		'user_id',
		'bank_id',
		'account_number',
		'account_name',
		'recipient_code',
		'created',
		'modified'
	];

	public function bank()
	{
		return $this->belongsTo(\App\Models\Bank::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
