<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 19 Jun 2018 10:49:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CardlessWithdrawal
 * 
 * @property int $id
 * @property int $user_id
 * @property string $reference_code
 * @property int $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CardlessWithdrawal extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'amount' => 'int'
	];

	protected $fillable = [
		'user_id',
		'reference_code',
        'paycode',
        'expiry_date',
		'amount',
		'charge',
		'meta',
        'url',
        'provider_channel',
        'status',
        'code_status',
        'provider_code_status',
        'request_meta',
        'response_meta',
	];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function provider(){
        return $this->provider_channel;
    }

    public static function filterChannel($channel)
    {
        return CardlessWithdrawal::where('provider_channel', $channel);
    }

    public function isUniqueRef($ref){
        $occurrence = CardlessWithdrawal::where('reference_code', $ref);
        if($occurrence->count() > 0)
        {
            return false;
        }
        return true;
    }
}
