<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterswitchRequest extends Model
{
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'user_id',
        'url',
        'amount',
        'reference_code',
        'status',
        'code_status',
        'interswitch_code_status',
        'request_meta',
        'response_meta',
        'transaction_type_id'
	];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
