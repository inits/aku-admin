<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Wallet
 * 
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Wallet extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'amount' => 'int'
	];

	protected $fillable = [
		'user_id',
		'amount'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
