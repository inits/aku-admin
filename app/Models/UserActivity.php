<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserActivity
 * 
 * @property int $id
 * @property string $heading
 * @property string $tag
 * @property string $message
 * @property int $status
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserActivity extends Eloquent
{
	protected $casts = [
		'status' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'heading',
		'tag',
		'message',
		'status',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
