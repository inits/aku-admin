<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    //
    protected $fillable = [
        'user_id',
        'loan_provider_id',
        'amount',
        'balance',
        'reason',
        'meta',
        'status',
        'interest',
        'reference',
        'provider_status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function loan_status()
    {
        return $this->belongsTo(LoanStatus::class, 'status', 'value');
    }


    public function provider()
    {
        return $this->belongsTo(LoanProvider::class);
    }

//    public static function saveQueue($user_id, $loan_provider_id, $amount, $reason, $reference)
//    {
//        Loan::create([
//            'user_id' => $user_id,
//            'loan_provider_id' => $loan_provider_id,
//            'amount' => $amount,
//            'balance' => $amount,
//            'reason' => $reason,
//            'meta' => [],
//            'status' => 0,
//            'interest' => 0,
//            'reference' => $reference,
//            'provider_status' => 0,
//        ]);
//    }

}
