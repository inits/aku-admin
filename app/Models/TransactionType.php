<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TransactionType
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $transaction_logs
 * @property \Illuminate\Database\Eloquent\Collection $wallet_transaction_logs
 *
 * @package App\Models
 */
class TransactionType extends Eloquent
{
	protected $fillable = [
		'name'
	];

	public function transaction_logs()
	{
		return $this->hasMany(\App\Models\TransactionLog::class);
	}

	public function wallet_transaction_logs()
	{
		return $this->hasMany(\App\Models\WalletTransactionLog::class);
	}
}
