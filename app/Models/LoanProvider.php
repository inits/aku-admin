<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanProvider extends Model
{
    //

    protected $fillable = [
        'name',
        'username',
        'password',
        'email',
        'address',
        'website',
        'phone',
        'status',
    ];

    public function loans(){
        return $this->hasMany(Loan::class);
    }

    protected $hidden = [
        'password'
    ];
}
