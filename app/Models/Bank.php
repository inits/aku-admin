<?php

/**
 * Created by gem.
 * Date: Sun, 10 Jun 2018 21:01:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property int $id
 * @property string $name
 * @property string $sort_code
 * @property string $bank_code
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Bank extends Eloquent
{
	public $timestamps = false;

	protected $dates = [
		'created',
		'modified'
	];

	protected $fillable = [
		'name',
		'sort_code',
		'bank_code',
		'created',
		'modified'
	];

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'user_banks')
					->withPivot('id', 'account_number', 'account_name', 'recipient_code', 'created', 'modified');
	}
}
