<?php
/**
 * Created by PhpStorm.
 * User: davinci
 * Date: 7/19/17
 * Time: 1:26 PM
 */

namespace App\Transformers;


use App\Models\CardlessWithdrawal;
use App\Models\Wallet;
use League\Fractal\TransformerAbstract;

class CardlessWithdrawalTransformer extends TransformerAbstract
{

    public function transform(CardlessWithdrawal $cardless_withdrawal)
    {
        if(!is_null($cardless_withdrawal->provider_code_status)){
            $code_status = INTERSWITCH_PAYCODE_STATUSES[$cardless_withdrawal->provider_code_status]['description'];
        }
        return [
            'id' => $cardless_withdrawal->id,
            'amount' => $cardless_withdrawal->amount,
            'status' => ($cardless_withdrawal->status == 0)? 'Failed' : 'Successful',
            'paycode_status' => $code_status ?? null,
            'expiry_date' => $cardless_withdrawal->expiry_date
        ];
    }


    public function collect_pagination($pagination)
    {
        $pagination_double = $pagination;
        $build_res = $pagination_double->toArray();
        $transformer = new CardlessWithdrawalTransformer();
        $data = collect($pagination->getCollection())->map(function ($model) use ($transformer) {
            return $transformer->transform($model);
        });
        $build_res['load'] = $data;

        return select_array_indexes($build_res, ['load', 'total', 'current_page', 'last_page']);


    }
}