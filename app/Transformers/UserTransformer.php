<?php
/**
 * Created by PhpStorm.
 * User: gem
 * Date: 7/13/17
 * Time: 5:35 PM
 */

namespace App\Transformers;


use App\Models\User;
use  \League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        if($user->pin != null){
            $pinisset = true;
        }else{
            $pinisset = false;
        }
        $bank_transfer = \App\Models\CustomService::where('name', 'bank_transfer')->first()->status;
        $cardless_withdrawal = \App\Models\CustomService::where('name', 'cardless_withdrawal')->first()->status;
        return [
            'id' => encrypt_decrypt('encrypt', $user->id),
            'name' => $user->name,
            'phone' => $user->phone,
            'pin_set' => $pinisset,
            'custom_email' => $user->custom_email,
            'wallet_balance' => $user->wallet->amount,
            'custom_services' => [
                'bank_transfer' => $bank_transfer,
                'cardless_withdrawal' => $cardless_withdrawal
            ],
            'email' => $user->email
        ];
    }

    public function collect($collection)
    {
        $transformer = new UserTransformer();
        return collect($collection)->map(function ($model) use ($transformer) {
            return $transformer->transform($model);
        });
    }

}