<?php
/**
 * Created by PhpStorm.
 * User: gem
 * Date: 7/13/17
 * Time: 5:31 PM
 */

namespace App\Transformers;


use  \League\Fractal\TransformerAbstract;
use \App\Models\State;

class StateTransformer extends TransformerAbstract
{
	/**
	 * Turn this item object into a generic array
	 *
	 * @return array
	 */
	public function transform(State $state, $lgas = null)
	{
		if($lgas == null )
			return ['id' => encrypt_decrypt('encrypt', $state->id), 'name' => $state->name];
		else
			return ['id' => encrypt_decrypt('encrypt', $state->id), 'name' => $state->name, 'lgas' => $lgas];
	}


	public function collect($collection)
	{
		$transformer = new StateTransformer();
		return collect($collection)->map(function ($model) use ($transformer) {
			if($model->has('lgas')){
				$lga_transformer = new LgaTransformer();
				return $transformer->transform($model, $lga_transformer->collect($model->lgas));
			}
			return $transformer->transform($model);
		});
	}
}