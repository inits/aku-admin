<?php
/**
 * Created by PhpStorm.
 * User: davinci
 * Date: 7/19/17
 * Time: 1:26 PM
 */

namespace App\Transformers;


use App\Models\Wallet;
use App\Models\WalletTransactionLog;
use League\Fractal\TransformerAbstract;

class WalletTransactionTransformer extends TransformerAbstract
{

    public function transform(WalletTransactionLog $wallet_tran_log)
    {
        return [
            'id' => $wallet_tran_log->id,
            'transaction_type' => $wallet_tran_log->transaction_type->name,
//            'user_id' => $wallet_tran_log->user_id,
            'amount' => $wallet_tran_log->amount,
            'message' => $wallet_tran_log->message,
            'header' => $wallet_tran_log->header,
            'date' => $wallet_tran_log->created_at->format('Y-m-d H:i:s')
        ];
    }


    public function collect_pagination($pagination)
    {
        $pagination_double = $pagination;
        $build_res = $pagination_double->toArray();
        $transformer = new WalletTransactionTransformer();
        $data = collect($pagination->getCollection())->map(function ($model) use ($transformer) {
            return $transformer->transform($model);
        });
        $build_res['load'] = $data;

        return select_array_indexes($build_res, ['load', 'total', 'current_page', 'last_page']);


    }
}