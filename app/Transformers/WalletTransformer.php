<?php
/**
 * Created by PhpStorm.
 * User: davinci
 * Date: 7/19/17
 * Time: 1:26 PM
 */

namespace App\Transformers;


use App\Models\Wallet;
use League\Fractal\TransformerAbstract;

class WalletTransformer extends TransformerAbstract
{

    public function transform(Wallet $wallet)
    {
        return [
//			'id' => encrypt_decrypt('encrypt', $wallet->id),
            'amount' => $wallet->amount
        ];
    }
}