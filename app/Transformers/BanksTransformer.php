<?php
/**
 * Created by PhpStorm.
 * User: gem
 * Date: 7/13/17
 * Time: 5:35 PM
 */

namespace App\Transformers;


use App\Models\Bank;
use  \League\Fractal\TransformerAbstract;

class BanksTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Bank $bank)
    {
        return [
            'id' => $bank->id,
            'name' => $bank->name,
            'sort_code' => $bank->sort_code,
            'bank_code' => $bank->bank_code,
            ];
    }

    public function collect($collection)
    {
        $transformer = new BanksTransformer();
        return collect($collection)->map(function ($model) use ($transformer) {
            return $transformer->transform($model);
        });
    }

}