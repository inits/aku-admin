<?php
/**
 * Created by PhpStorm.
 * User: gem
 * Date: 7/13/17
 * Time: 5:35 PM
 */

namespace App\Transformers;


use App\Helper\Meta;
use App\Models\UserCard;
use  \League\Fractal\TransformerAbstract;

class UserCardTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(UserCard $user_card)
    {
        return [
            'id' => $user_card->id,
            'card_type' => $user_card->card_type,
            'last4' => $user_card->last4,
            'exp_month' => $user_card->exp_month,
            'exp_year' => $user_card->exp_year,
            'status' => ($user_card->status == Meta::ACTIVE_CARD) ? 'active' : 'inactive',
        ];
    }

    public function collect($collection)
    {
        $transformer = new UserCardTransformer();
        return collect($collection)->map(function ($model) use ($transformer) {
            return $transformer->transform($model);
        });
    }

}