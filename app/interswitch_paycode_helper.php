<?php

date_default_timezone_set('Africa/Lagos');

function pr($data) {
    echo "\n";
    print_r($data);
    echo "\n";
}

define('CFG_INTERSWITCH_CLIENT_ID', "IKIAAA8589ECA63248BA5C87BE138C591ACEEDF8E1AC");
define('CFG_INTERSWITCH_CLIENT_SECRET', "rUS/KWnt9rSnPrrfGTwSP1hVczfulgJd2xVDK3DyL7E=");
define("CFG_INTERSWITCH_ENV", "SANDBOX");
define("CFG_INTERSWITCH_BASE_URL", ("CFG_INTERSWITCH_ENV" == 'PRODUCTION') ? "https://saturn.interswitchng.com" : "https://sandbox.interswitchng.com");
define("INTERSWITCH_PAYCODE_STATUSES", [
        "0" => [
            "name" => "Open",
            "description" => "Withdrawal code is still active"
            ],
        "1" => [
            "name" => "In-Use",
            "description" => "Withdrawal code is currently in use"
            ],
        "2" => [
            "name" => "Cancelled",
            "description" => "Withdrawal code has been cancelled"
            ],
        "3" => [
            "name" => "Reversed",
            "description" => "Transaction has been reversed"
            ],
        "4" => [
            "name" => "Closed",
            "description" => "Withdrawal code has been used"
            ],
        "5" => [
            "name" => "Locked No of PIN retries exceeded.",
            "description" => "Withdrawal code locked: maximim number of pin retries exceeded"
            ],
        "6" => [
            "name" => "Expired",
            "description" => "Withdrawal Code has expired"
            ]
        ]);

function generateNonce() {
    return sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function generateTimestamp() {
    $date = new \DateTime(null, new \DateTimeZone('Africa/Lagos'));
    return $date->getTimestamp();
}

function generateSignature($clientId, $clientSecretKey, $resourceUrl, $httpMethod, $timestamp, $nonce, $transactionParams) {

    $resourceUrl = str_replace('http://', 'https://', strtolower($resourceUrl));
    $encodedUrl = urlencode($resourceUrl);

    $signatureCipher = $httpMethod . '&' . $encodedUrl . '&' . $timestamp . '&' . $nonce . '&' . $clientId . '&' . $clientSecretKey;

    if (!empty($transactionParams) && is_array($transactionParams)) {
        $parameters = implode("&", $transactionParams);
        $signatureCipher = $signatureCipher . $parameters;
    }

    $signature = base64_encode(sha1($signatureCipher, true));
    return $signature;
}

function getAccessToken(){
    $passportUrl="/passport/oauth/token";
    $accessTokenDataResponse = generateAccessToken(CFG_INTERSWITCH_CLIENT_ID, CFG_INTERSWITCH_CLIENT_SECRET, $passportUrl);
    $accessTokenData = json_decode($accessTokenDataResponse['HTTP_RESPONSE']);
    return $accessTokenData->access_token;
}

function generateAccessToken($clientId, $clientSecret, $passportUrl) {

    $finalPassportUrl = CFG_INTERSWITCH_BASE_URL.$passportUrl;
    $content_type = 'application/x-www-form-urlencoded';
    $basicCipher = base64_encode($clientId . ':' . $clientSecret);
    $authorization = 'Basic ' . $basicCipher;

    $headers = [
        'Content-Type: ' . $content_type,
        'Authorization: ' . $authorization
    ];

    $apilog =  external_request_log($finalPassportUrl."?grant_type=client_credentials&scope=profile", 'POST_OUTGOING', [], $headers);

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $finalPassportUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&scope=profile");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $curl_response = curl_exec($curl);

    external_request_log(null, null, null,null, $curl_response, $apilog->id);

    $info = curl_getinfo($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($curl_response === false) {
        throw new Exception(json_encode([
            "message" => 'error occured during curl exec. Additional info: ',
            "response_data" => var_export($info)]));
        curl_close($curl);
    }

    $response['HTTP_CODE'] = $info['http_code'];
    $response['HTTP_RESPONSE'] = $curl_response;

    curl_close($curl);

    return $response;
}

function getSecurityHeaders($httpMethod, $finalUrl, $realmType = 'Bearer') {

    $nonce = generateNonce();
    $timestamp = generateTimestamp();
    $signatureMethod = 'SHA1';
    $signature = generateSignature(CFG_INTERSWITCH_CLIENT_ID, CFG_INTERSWITCH_CLIENT_SECRET, $finalUrl, $httpMethod, $timestamp, $nonce, null);

    $authorizationString = "InterswitchAuth " . base64_encode(CFG_INTERSWITCH_CLIENT_ID);
    if ($realmType == 'Bearer') {

        $authorizationString="Bearer ".getAccessToken();
    }

    $securityHeaders = [
        "Authorization: $authorizationString",
        "SignatureMethod: $signatureMethod",
        'Signature: ' . $signature,
        'Timestamp: ' . $timestamp,
        'Nonce: ' . $nonce,
        'Content-Type: ' . 'application/json'
    ];
    return $securityHeaders;
}

function getStatusSecurityHeaders($httpMethod, $finalUrl, $realmType = 'Bearer', $paycode) {

    $nonce = generateNonce();
    $timestamp = generateTimestamp();
    $signatureMethod = 'SHA1';
    $signature = generateSignature(CFG_INTERSWITCH_CLIENT_ID, CFG_INTERSWITCH_CLIENT_SECRET, $finalUrl, $httpMethod, $timestamp, $nonce, null);

    $authorizationString = "InterswitchAuth " . base64_encode(CFG_INTERSWITCH_CLIENT_ID);
    if ($realmType == 'Bearer') {

        $authorizationString="Bearer ".getAccessToken();
    }

    $securityHeaders = [
        "Authorization: $authorizationString",
        "SignatureMethod: $signatureMethod",
        'Signature: ' . $signature,
        'Timestamp: ' . $timestamp,
        'Nonce: ' . $nonce,
        'Content-Type: ' . 'application/json',
        'paycode: '. $paycode
    ];
    return $securityHeaders;
}

function postToInterswitch($url, $requestParams) {

    $finalUrl = CFG_INTERSWITCH_BASE_URL . $url;

    $headers = getSecurityHeaders($finalUrl, 'Bearer');

    return sendPost($finalUrl, $requestParams, $headers);

}

function sendPost($finalUrl, $requestParams, $headers) {

    $content = json_encode($requestParams);
    $apilog =  external_request_log($finalUrl, 'POST_OUTGOING', $requestParams, $headers);
    $curl = curl_init($finalUrl);

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

    $jsonResponse = curl_exec($curl);
    external_request_log(null,null,null,$jsonResponse,$apilog->id);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $response = [
        'status_code' => $status,
        'response' => json_decode($jsonResponse, true)
    ];
    return $response;
}

function sendCustom($finalUrl, $headers, $httpVerb, $reqData = null)
{
    $apilog =  external_request_log($finalUrl, "{$httpVerb}_OUTGOING", [], $headers);
    $curl = curl_init($finalUrl);

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    if($httpVerb == 'GET') {
        curl_setopt($curl, CURLOPT_HTTPGET, true);
    }else {
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $httpVerb);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $reqData);
    }
    $jsonResponse = curl_exec($curl);
    external_request_log(null,null,null, null, $jsonResponse, $apilog->id);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $response = [
        'status_code' => $status,
        'response' => json_decode($jsonResponse, true)
    ];
    return $response;
}

