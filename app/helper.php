<?php
/**
 * Created by PhpStorm.
 * User: gem
 * Date: 7/7/17
 * Time: 2:50 PM
 */

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

define('REJECTED_TRANSACTION', '2');
define('SUCCESS_TRANSACTION', '1');
define('FAILED_TRANSACTION', '0');
define('NGN', '<span style="font-size: 13px;">₦</span>');


function convert_multi_array($array)
{
    foreach ($array as $k => $v) {
        if (is_array($v)) {
            $array[$k] = json_encode($v);
        }
    }
    return $array;
}

function encrypt_decrypt($action, $string)
{
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'LaoN';
    $secret_iv = 'simELp';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}

/**
 * @param $query
 */
function die_dump($query, $dont_exit = true)
{
    echo '<pre>';
    if (is_array($query)) {
        print_r($query);
    } else {
        var_dump($query);
    }
    if ($dont_exit) {
        exit();
    }
}


function search_query_constructor($searchString, $col)
{
    $dataArray = (array_filter(explode(" ", trim($searchString))));
    $constructor_sql = "(";
    if (count($dataArray) < 1) {
        return " 1 ";
    }
    if (is_array($col)) {
        foreach ($col as $col_name) {
            if ($col_name !== $col[0]) {
                $constructor_sql .= " OR ";
            }
            for ($i = 0; $i < count($dataArray); $i++) {
                if (count($dataArray) - 1 === $i) {
                    $constructor_sql .= "$col_name LIKE '%{$dataArray[$i]}%' ";
                } else {
                    $constructor_sql .= "$col_name LIKE '%{$dataArray[$i]}%' OR ";
                }
            }
        }
    } else {
        for ($i = 0; $i < count($dataArray); $i++) {
            if (count($dataArray) - 1 === $i) {
                $constructor_sql .= "$col LIKE '%{$dataArray[$i]}%' ";
            } else {
                $constructor_sql .= "$col LIKE '%{$dataArray[$i]}%' OR ";
            }
        }
    }
    $constructor_sql .= ")";
    return $constructor_sql;
}

function multi_unset($array, $keys)
{
    if (is_array($array)) {
        foreach ($keys as $key) {
            unset($array[$key]);
        }

        return $array;

    } else {
        return null;
    }
}

function select_array_indexes($array, $keys)
{
    $val = [];
    if (is_array($array)) {
        foreach ($keys as $key) {
            if (isset($array[$key])) {
                $val[$key] = $array[$key];
            }
            //  else {
            //      $val[$key] = $array[$key];
            //  }

        }

        return $val;

    } else {
        return null;
    }
}

function sendEmail($message_data, $subject, $from, $to, $type = null)
{
    if (!$type) {
        $type = "default";
    }
    $info['message'] = $message_data;
    $info['from'] = $from;
    $info['email'] = $to;
    $info['subject'] = $subject;

    \Illuminate\Support\Facades\Mail::send('emails.' . $type, compact('message_data', 'info'), function ($message) use ($info) {
        $message->from("noreply@" . env('APP_NAME', 'Aku') . ".com");
        $message->to($info['email'])->subject($info['subject']);
    });

}

function problemResponse($message = null, $status_code = null, $request = null, $trace = null)
{
    $code = ($status_code != null) ? $status_code : "404";
    $body = [
        'message' => "$message",
        'code' => $code,
        'status_code' => $code,
        'status' => false
    ];


    if (!is_null($request)) {
        $logBody = $body;
        $logBody['trace'] = $trace;
        save_log($request, $logBody);
        if ($code == "500" && !is_null($trace)) {

            $message = 'URL : ' . $request->fullUrl() .
                '<br /> METHOD: ' . $request->method() .
                '<br /> DATA_PARAM: ' . json_encode($request->all()) .
                '<br /> RESPONSE: ' . json_encode($body) .
                '<br /> Trace Message: ' . $trace->getMessage() .
                '<br /> <b> Trace: ' . json_encode($trace->getTrace()) . "</b>";
            sendEmail($message, 'API ERROR ALERT', env('APP_NAME', 'Aku'), 'victoria@initsng.com');
        }
    }


    return response()->json($body)->setStatusCode("$code");
}
function schedule_logger($task, $status, $trace =null)
{
    if(!is_null($trace))
    {
        \Illuminate\Support\Facades\Log::error(json_encode($trace));
        $message = "URL : {$task}".
            '<br /> METHOD: Call'.
            '<br /> Trace Message: ' . $trace->getMessage() .
            '<br /> <b> Trace: ' . json_encode($trace->getTrace()) . "</b>";
        sendEmail($message, 'API Scehedule Error ALERT', env('APP_NAME', 'Aku'), 'victoria@initsng.com');
    }else{
        \Illuminate\Support\Facades\Log::info(json_encode(['task'=> $task, 'status' => $status]));
    }
}
function validResponse($message = null, $data = [], $request = null)
{
    $body = [
        'message' => "$message",
        'data' => $data,
        'status' => true,
        'status_code' => 200,
    ];

    if (!is_null($request)) {
        save_log($request, $body);
    }

    return response()->json($body);
}

function save_log($request, $response)
{
    return \App\Models\ApiLog::create([
        'url' => $request->fullUrl(),
        'method' => $request->method(),
        'data_param' => json_encode($request->all()),
        'response' => json_encode($response),
    ]);
}

function generic_logger($fullUrl = null, $method = null, $param, $response)
{
    \App\Models\ApiLog::create([
        'url' => $fullUrl,
        'method' => $method,
        'data_param' => json_encode($param),
        'response' => json_encode($response),
    ]);
}

function external_request_log($fullUrl, $method, $param, $req_header = null, $response = null, $log_id = null)
{
    if (is_null($log_id)) {
        return \App\Models\ApiLog::create([
            'url' => $fullUrl,
            'header' => ($req_header != null)? json_encode($req_header): null,
            'method' => $method,
            'data_param' => json_encode($param),
            // 'request_type' => 'external'
        ]);
    } else {
        $log = \App\Models\ApiLog::find($log_id);
        return $log->update([
            'response' => json_encode($response)
        ]);
    }
}

function queueSMS($recipient, $theMessage){
    return \App\Models\SmsQueue::saveQueue($recipient, $theMessage);
}

function processSmsQueue()
{
    DB::table('sms_queue')->where('sent', 0)->orderBy('created_at')->chunk(100, function ($smses) {
        foreach ($smses as $sms) {
            try {
                sendSMS($sms->phone, $sms->message);
                App\Models\SmsQueue::find($sms->id)->update(['sent' => 1]);
                schedule_logger('sendSMS','successful');
            } catch (\Exception $e){
                schedule_logger('sendSMS','failed',$e);
            }
        }
    });
}

function sendSMS($recipient, $theMessage)
{
    return _sendSMSRouteSms($recipient, $theMessage);
}

function _sendSMS2($recepientList, $message)
{

    if (is_array($recepientList)) {
        $recepients = implode(',', $recepientList);
    } else {
        $recepients = $recepientList;
    }

    try {
        $url = 'https://flow-sim.initsconduit.com/Services/sendSms';
        $params = [
            'api_key' => '8-govote-7834eiuwnj20234',
            'sim_card_id' => 37,
            'sms_message' => $message,
            'sms_recipients' => $recepients
        ];

        $client = new Client();
        $response = $client->request('POST', $url, [
            'form_params' => $params
        ]);
        // echo "Successfull";
        $response_data = $response->getBody()->getContents();
        generic_logger($url, "POST-OUTGOING", $params, $response_data);
    } catch (ClientException $e) {
        // echo "Failed [Client]";
        // var_dump($e->getMessage());
        generic_logger($url, "POST-OUTGOING", $params, $e->getResponse());
    } catch (RequestException $e) {
        // echo "Failed [Request]";
        if ($e->hasResponse()) {
            return generic_logger($url, "POST-OUTGOING", $params, $e->getResponse());

        }
        return generic_logger($url, "POST-OUTGOING", $params, $e->getRequest());
    }
}

function _sendSMS($recipient, $theMessage)
{
    $recipient = (strlen(trim(" " . $recipient)) >= 10) ? "234" . substr(trim($recipient), -10) : null;

    $username = 'dftaiwo@gmail.com'; #env('MULTITEXTER_USERNAME', 'dftaiwo@gmail.com');
    $password = 'jX9tXK8G8'; #env('MULTITEXTER_PASSWORD', 'jX9tXK8G8');
    $sender = env('APP_NAME', 'Fanta');
    $flash = 1;
    $message = urlencode($theMessage);
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://www.multitexter.com/tools/geturl/Sms.php?' .
            "username={$username}&password={$password}&sender={$sender}&" .
            "flash={$flash}&recipients={$recipient}&message={$message}",
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return $err;
    } else {
        return $response;
    }
}

function _sendSMSRouteSms($recipient, $theMessage)
{

    $message = $theMessage . "\n\n Powered by Aku.ng";

    try {
        $recipient = (strlen(trim(" " . $recipient)) >= 10) ? "234" . substr(trim($recipient), -10) : null;

        $requestParams = [
            'username' => 'ebng-boing',
            'password' => 'industry',
            'source' => 'TraderMoni',
            'type' => 0,
            'dlr' => 1,
            'message' => $message,
            'destination' => $recipient,
        ];
        $log = external_request_log('http://ngnr.connectbind.com:8080/bulksms/bulksms?' . http_build_query($requestParams), 'POST-OUTGOING', $requestParams);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://ngnr.connectbind.com:8080/bulksms/bulksms?' . http_build_query($requestParams)
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);


        curl_close($curl);

        external_request_log("http://ngnr.connectbind.com:8080/bulksms/bulksms", 'POST-OUTGOING', ['phone' => $recipient, 'message' => $theMessage], null, $response, $log->id);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    } catch (Exception $e) {
        return false;
    }
}

function isValidEmail($email)
{
    return (boolean)filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email);
}

function bvnVerifyName($name, $bvn_name)
{
    $specials = [",", "-", "'"];
    $nameArray = explode(" ", strtolower(str_replace($specials, '', $name)));
    $bvnNameArray = explode(" ", strtolower(str_replace($specials, '', $bvn_name)));

    if (!is_array($nameArray) || !is_array($bvnNameArray)) {
        return false;
    }

    $validity = 0;
    foreach ($nameArray as $name_) {
        $foundAt = array_search($name_, $bvnNameArray);
        if (!is_bool($foundAt)) {
            $validity++;
        }
    }

    if ($validity >= 2) {
        $isValid = true;
    } else {
        $isValid = false;
    }
    generic_logger('api-verify-bank-acct-details', 'INTERNAL', ['escaped_characters' => $specials, 'name' => strtolower($name), 'bvn_name' => strtolower($bvn_name)], ['isValid' => $isValid]);
    return $isValid;
}

function generatePaystackReferenceCode($length = 6, $char = null)
{
    if ($char == null) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else {
        $characters = '123456789abcdefghijklmnopqrstuvwxyz';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    //check if the string is selected
    $string_rand = \App\Models\WalletTransactionLog::pluck('response_code')->toArray();

    if (in_array('aku-'.$randomString, $string_rand)) {
        $randomString = generatePaystackReferenceCode();
    }

    return $randomString;
}

function generateRandomReferenceCode($length = 6, $char = null)
{
    if ($char == null) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else {
        $characters = '123456789';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    //check if the string is selected
    $string_rand = \App\Models\CardlessWithdrawal::pluck('reference_code')->toArray();

    if (in_array($randomString, $string_rand)) {
        $randomString = generateRandomReferenceCode();
    }

    return $randomString;
}

function ddd($var)
{
    $dump = $var;
    print_r($dump);
    exit;

}

function createTransferRecipient($recipient_name, $account_number, $bank_code = "")
{

    $fields = array(
        "type" => "nuban",
        "name" => "$recipient_name",
        "description" => "Influencer transfer recipient",
        "account_number" => "$account_number",
        "bank_code" => "$bank_code",
        "currency" => "NGN"
    );

    $fields = json_encode($fields);

    $log = external_request_log("https://api.paystack.co/transferrecipient", "POST-OUTGOING", $fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.paystack.co/transferrecipient");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
        'Authorization: Bearer ' . env('PAYSTACK_SECRET_KEY')));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    external_request_log(null, null, null, null, $response, $log->id);

    $response = json_decode($response, true);
    if (isset($response['data']) && isset($response['data']['recipient_code']) && (strlen($response['data']['recipient_code']) > 0)) {
        return $response['data']['recipient_code'];
    }
    return false;

}

function payRecipient($amount, $recipient_code, $user_id, $reason, $refCode)
{

    $fields = array(
        "source" => "balance",
        "reason" => $reason,
        "amount" => $amount * 100,
        "recipient" => "$recipient_code",
        "reference" => $refCode
    );

    $fields = json_encode($fields);
    $date = new DateTime();
    $reqheader = array('Content-Type: application/json; charset=utf-8',
        'Authorization: Bearer ' . config("app.PAYSTACK_SECRET_KEY"));
    $log = external_request_log("https://api.paystack.co/transfer", "POST-OUTGOING", $fields, $reqheader);

    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.paystack.co/transfer");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Bearer ' . config("app.PAYSTACK_SECRET_KEY")));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        $resData = $response;
        $resDataMeta = (array)json_decode($resData, true);

        external_request_log(null, null, null, null, $resDataMeta, $log->id);

//        if (isset($resDataMeta['status']) && $resDataMeta['status'] === 1) {
//            $resData = $resDataMeta['data'];
//
//            $resData['paystack_id'] = $resData['id'];
//            $resData['message'] = $resDataMeta['message'];
//            $resData['transaction_type_id'] = \App\Helper\Meta::TRANSFER_MONEY_OUT_ACTION;
//            $resData['transaction_date'] = $date->format('Y-m-d H:i:s');
//            // $resData = multi_unset($resData, ['id', 'createdAt', 'updatedAt']);
//            $resData['user_id'] = $user_id;
//
//           // \App\Models\TransactionLog::create($resData);
//        } else {
//            $resData = $resDataMeta['data'];
//            $resData['message'] = $resDataMeta['message'];
//            $resData['transaction_type_id'] = \App\Helper\Meta::TRANSFER_MONEY_OUT_FAILED_ACTION;
//            $resData['transaction_date'] = $date->format('Y-m-d H:i:s');
//            $resData['paystack_id'] = $resData['id'];
//
//           // \App\Models\TransactionLog::create($resData);
//        }

        $resDataMeta['reference'] = $refCode;
        return $resDataMeta;

    } catch (\Exception $e) {

        $rData['status'] = "payment_broken";
        $rData['message'] = $e->getMessage();
        $rData['trace'] = $e->getTraceAsString();
        $rData['user_id'] = $user_id;
        $rData['reference'] = $refCode;
        $rData['transaction_date'] = $date->format('Y-m-d H:i:s');

        external_request_log(null, null, null, null, $rData, $log->id);

        return $rData;
    }


}

function saveWalletLog($data)
{
    \App\Models\WalletTransactionLog::create($data);
}

function saveWalletLogDev($data)
{
    \App\Models\WalletTransactionLog::createWalletTransaction($data);
}

function saveUserActivity($data)
{
    \App\Models\UserActivity::create($data);
}

function resolveAcctName($bank_acct, $bank_code)
{
    $url = "https://api.paystack.co/bank/resolve?account_number={$bank_acct}&bank_code={$bank_code}";

    try {

        $ch = curl_init();

//      Set query data here with the URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Bearer ' . config("app.PAYSTACK_SECRET_KEY")));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);

        $response = curl_exec($ch);
        curl_close($ch);
        $resData = $response;
        $array_response = (array)json_decode($resData, true);

        if (isset($array_response['status']) && ($array_response['status'] == true)) {
            if (isset($array_response['data']) && ($array_response['data']['account_name'])) {
                $res = ['name' => $array_response['data']['account_name'], 'status' => 'success'];
                generic_logger($url, "POST-OUTGOING", compact('bank_acct', 'bank_code'), $res);
                return ($res);
            }
        }
        $res = ['message' => $array_response, 'status' => 'fail'];
        generic_logger($url, "POST-OUTGOING", compact('bank_acct', 'bank_code'), $res);
        return ($res);
    } catch (\Exception $e) {

        $res = ['status' => 'fail', 'message' => $e->getMessage()];

        generic_logger($url, "POST-OUTGOING", compact('bank_acct', 'bank_code'), $res);
        return ($res);

    }
}

function cleanUpPhone($phone)
{
    return (strlen(trim(" " . $phone)) >= 10) ? "+234" . substr(trim($phone), -10) : null;

}

function rechargeAPhone($phone, $amount)
{
    //Specify your credentials
    $username = env('AFRICA_IS_TALKING_USERNAME');
    $apiKey = env('AFRICA_IS_TALKING_PASSWORD');

    //Specify the phone number/s and amount in the format shown
    $recipients = array(
        array("phoneNumber" => $phone, "amount" => "NGN $amount"),
    );

    $recipientStringFormat = json_encode($recipients);
    $gateway = new AfricasTalkingGateway($username, $apiKey);

    try {
        $results = $gateway->sendAirtime($recipientStringFormat);
        $res = [];
        foreach ($results as $result) {
            $res = [
                'status' => $result->status,
                'amount' => $result->amount,
                'phone_number' => $result->phoneNumber,
                'discount' => $result->discount,
                'request_id' => $result->requestId,
                //Error message is important when the status is not Success
                'error' => $result->errorMessage,
                'failed' => false,
            ];
            generic_logger("http://api.africastalking.com/airtime/sending", 'POST-OUTGOING', ['phone' => $phone, 'amount' => $amount], $res);

        }

        //generic_logger("http://api.africastalking.com/airtime/sending", 'POST-OUTGOING', ['phone' => $phone, 'amount' => $amount], $res);
        if (strtolower(trim($res['status'])) != "sent") {
            return [
                'data' => $res,
                'success' => null
            ];
        }

        return [
            'data' => $res,
            'success' => true
        ];
    } catch (AfricasTalkingGatewayException $e) {
        $res = [
            'status' => "unsuccessful",
            'amount' => $amount,
            'error_message' => $e->getMessage(),
            'failed' => true,
        ];
        generic_logger("http://api.africastalking.com/airtime/sending", 'POST-OUTGOING', ['phone' => $phone, 'amount' => $amount], $res);
        return [
            'data' => $res,
            'success' => false
        ];
    }

}

function is_verified($user)
{
    if ($user->status == 0) {
        return false;
    }
    return true;
}

function completed_signup($user)
{
    if (is_null($user->email)) {
        return false;
    }
    return true;
}

function generatePagaHash(array $params)
{
    $hash = hash('sha512', implode($params) . env('WITHDRAWAL_PROVIDER_HASH'));
    return $hash;
}

////////////////////////////////
function isDummyEmail($user)
{
    if ($user->email == substr(trim($user->phone), -10) . '@inits.xyz') {
        return true;
    }
    return false;
}

function flash_message($type, $message)
{
    if (!in_array(['info', 'success', 'warning', 'danger'], $type)) {
        throw new Exception("first argument 'type' must be any of these ['info','successs','warning','danger'],'" . $type . "' given");
    }
    return [
        'flash_message' => [
            'type' => $type,
            'message' => $message,
        ]
    ];
}



if (!function_exists('array_group_by')) {
    /**
     * Groups an array by a given key.
     *
     * Groups an array into arrays by a given key, or set of keys, shared between all array members.
     *
     * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
     * This variant allows $key to be closures.
     *
     * @param array $array   The array to have grouping performed on.
     * @param mixed $key,... The key to group or split by. Can be a _string_,
     *                       an _integer_, a _float_, or a _callable_.
     *
     *                       If the key is a callback, it must return
     *                       a valid key from the array.
     *
     *                       If the key is _NULL_, the iterated element is skipped.
     *
     *                       ```
     *                       string|int callback ( mixed $item )
     *                       ```
     *
     * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
     */
    function array_group_by(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }
        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;
            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && isset($value->{$_key})) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }
            if ($key === null) {
                continue;
            }
            $grouped[$key][] = $value;
        }
        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();
            foreach ($grouped as $key => $value) {
                $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }
        return $grouped;
    }
}

function foreignKeyExists($table, $key_name){
    $sm = Schema::getConnection()->getDoctrineSchemaManager();
    $indexesFound = $sm->listTableIndexes($table);
    if(array_key_exists($key_name, $indexesFound)){
        return true;
    }
    return false;
}


function activeServiceType($service_type)
{
    $service = \App\Models\CustomService::where('name', $service_type)->first();
    if($service->status == 'on'){
        return true;
    }else{
        return false;
    }
}

require_once('interswitch_paycode_helper.php');

?>