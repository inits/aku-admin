<?php
/**
 * Created by PhpStorm.
 * User: gem
 * Date: 6/11/18
 * Time: 7:27 PM
 */

namespace App\Helper;


class Meta
{

    // Account status
    const ACCOUNT_ACTIVE = 1;
    const ACCOUNT_INACTIVE = 0;

    // Card status
    const  ACTIVE_CARD = 1;
    const  INACTIVE_CARD = 0;

    // Transaction Types
    const TRANSFER_MONEY_OUT_ACTION = 1;
    const TRANSFER_MONEY_OUT_FAILED_ACTION = 2;
    const WALLET_TRANSFER_ACTION = 3;
    const FUND_WALLET_ACTION = 4;
    const WITHDRAWAL_CODE_GENERATED = 5;
    const BUY_AIRTIME = 6;
    const LOAN_RECEIVED = 7;
    const LOAN_PAYMENT = 8;
    const LOAN_REQUESTED = 9;
    const CHECK_WITHDRAWAL_CODE_STATUS = 10;
    const CANCEL_CARDLESS_WITHDRAWAL = 11;

    //activity tags
    const WALLET_FUNDED = "wallet_funded";
    const TRANSFER_MONEY_OUT = "transferred_money_out";
    const TRANSFER_MONEY_OUT_FAILED = "transferred_money_out_failed";
    const WALLET_TRANSFER = "wallet_transfer";
    const GENERATED_WITHDRAWAL_CODE = "generated_withdrawal_code";
    const AIRTIME_PURCHASE = "airtime_purchase";

    // Service providers for sms trasnfer
    const MTN = '1';
    const AIRTEL = '2';
    const ETISALAT = '3';
    const GLO = '4';

    // charges
    const TRANSFER_OUT_TO_ACCT_CHARGE = 0;//100
    const TRANSFER_OUT_TO_WALLET_CHARGE = 0;//50
    const CARDLESS_WITDRAWAL_CHARGE = 0;//70
    const BUY_AIRTIME_CHARGE = 0;//2

    const MULTITEXTER_ERROR = [
        -1 =>  "Incorrect / badly formed XML data",
        -2  => "Incorrect username and/or password",
        -3  => "Not enough credit units in user account",
        -4  => "Invalid sender name",
        -5  => "No valid recipient",
        -6  => "Invalid message length/No message content",
        -10 => "Unknown/Unspecified error"
    ];

    const LOAN_UNPAID = 0;
    const LOAN_PAID = 1;
    const LOAN_PROCESSING = 2;
    const LOAN_FAILED = 3;

    const PENDING = 1;
    const UPLOADING = 2;
    const DONE_UPLOADING = 3;
    const ERROR = 4;


    const BOI = 1;

}