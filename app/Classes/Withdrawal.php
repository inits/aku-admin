<?php

namespace App\Classes;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

use App\Models\CardlessWithdrawal;

class Withdrawal 
{
    protected $client = null;
    public function __construct() {
        //
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => env('WITHDRAWAL_PROVIDER_BASE_URL', ''),

            'headers' => [
                'principal' => env('WITHDRAWAL_PROVIDER_PRINCIPAL', ''),
                'credentials' => env('WITHDRAWAL_PROVIDER_CREDENTIALS', ''),
            ]
        ]);
    }

    public function getWithdrawalCode(array $request_params)
    {
        $hash_params = [
            $request_params['referenceNumber'],
            $request_params['amount'],
            $request_params['destinationAccount']
        ];

        $hash_params = collect($hash_params)->filter(function($option){
            return !!$option;
        })->toArray();
        try {
            $response = $this->client->request('POST', 'moneyTransfer', [
                'headers' => [
                    'hash' => generatePagaHash($hash_params)
                ],
                'json' => $request_params
            ]);
            $response = json_decode($response->getBody()->getContents(), true);

            generic_logger(env('WITHDRAWAL_PROVIDER_BASE_URL', ''), 'POST-OUTGOING', [
                'phone' => $request_params['destinationAccount'],
                'amount' => $request_params['amount']
            ], $response);

            return $response;
        }catch (ClientException $e) {
            // echo Psr7\str($e->getRequest());
            // echo Psr7\str($e->getResponse());
            // ddd($e->getRequest());
            // ddd('client error');
            generic_logger(env('WITHDRAWAL_PROVIDER_BASE_URL', ''), 'POST-OUTGOING', [
                    'phone' => $request_params['destinationAccount'],
                    'amount' => $request_params['amount']
                ], $e->getResponse());

            // ddd($e->getResponse());
            throw new \Error($e->getResponse());
        }
         catch (RequestException $e) {
            // echo Psr7\str($e->getRequest());
            // ddd($e->getRequest());
            // ddd('request error');
            if ($e->hasResponse()) {
                // echo Psr7\str($e->getResponse());
                generic_logger(env('WITHDRAWAL_PROVIDER_BASE_URL', ''), 'POST-OUTGOING', [
                    'phone' => $request_params['destinationAccount'],
                    'amount' => $request_params['amount']
                ], $e->getResponse());

                throw new \Error($e->getResponse());
            }

            throw new \Error($e->getRequest());
        }
        
    }
}
