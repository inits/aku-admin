<?php

namespace App\Classes;

use App\Helper\Meta;
use App\Models\CancelledPaycode;
use App\Models\CardlessWithdrawal;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class InterswitchTransactions
{
    protected $subscriber;
    protected $amount;
    protected $clean_phone;
    protected $user_pin;
    protected $paycode_transaction;

    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 2;
    const STATUS_PENDING= 3;
    const STATUS_PROCESSING = 4;

    public function __construct(User $user, $amount = null, $transaction = null)
    {
        $this->subscriber = $user;
        $this->amount = $amount;
        $this->user_pin = $user->pin;
        $this->clean_phone = str_replace('+','',cleanUpPhone($user->phone));
        if($transaction == null) {
            $this->paycode_transaction = CardlessWithdrawal::create([
                'user_id' => $user->id,
                'amount' => $amount,
                'provider_channel' => 'interswitch'
            ]);
        }else{
            $this->paycode_transaction = CardlessWithdrawal::find($transaction);
        }
    }

    public function getTTID()
    {
        return str_pad($this->paycode_transaction->id, 9, 0, STR_PAD_LEFT);
    }

    public function paycodeRequest()
    {
        $oneTimePin = $this->user_pin;
        $ttid = $this->getTTID();
        return [
            'subscriberId' => $this->clean_phone,
            "ttid" => $ttid,
            "paymentMethodTypeCode" => "MMO",
            'paymentMethodCode' => (env('APP_ENV') != 'production') ? 'WEMA': 'AKU',
            "payWithMobileChannel" => "ATM",
            "tokenLifeTimeInMinutes" => "2880",
            "amount" => $this->amount * 100,
            "oneTimePin" => $oneTimePin,
            'codeGenerationChannel' => 'Web',
            "codeGenerationChannelProvider" => "DBP",
            "transactionRef" => $ttid,
            "frontEndPartnerId" => (env('APP_ENV') != 'production') ? 'WEMA': 'AKU',
            'autoEnroll' => 'false',
            'accountNo' => $this->clean_phone,
        ];

    }

    public function getPaycode() {

        $url = "/api/v1/pwm/subscribers/{$this->clean_phone}/tokens";

        $finalUrl = CFG_INTERSWITCH_BASE_URL . $url;
        $request = $this->paycodeRequest();
        try {

            $headers = getSecurityHeaders('POST',$finalUrl, 'Bearer');

            $response_data = sendPost($finalUrl, $request, $headers);
            $response = $response_data['response'];
            if($response_data['status_code'] != 201){
                $this->paycode_transaction->update([
                    'status' => 0,
                    'reference_code' => $request['ttid'],
                    'request_meta' => json_encode($request),
                    'response_meta' => json_encode($response_data['response'])
                ]);
                return [
                    'status' => false,
                    'error' => $response_data['response']
                ];
            }else {
                $expiry = $this->paycode_transaction->created_at->addMinutes($response['tokenLifeTimeInMinutes']);
                $code_status = INTERSWITCH_PAYCODE_STATUSES['0'];
                $this->paycode_transaction->update([
                    'status' => 1,
                    'reference_code' => $request['ttid'],
                    'paycode' => $response['payWithMobileToken'],
                    'code_status' => $code_status['name'],
                    'provider_code_status' => '0',
                    'expiry_date' => $expiry->toDateTimeString(),
                    'request_meta' => json_encode($request),
                    'response_meta' => json_encode($response_data['response'])
                ]);
		        $response_data['response']['reference'] = $request['ttid'];
                return [
                    'status' => true,
                    'data' => $response_data['response']
                ];
            }

        } catch (\Exception $e){
            $this->paycode_transaction->update([
                'status' => 0,
                'reference_code' => $request['ttid'],
                'request_meta' => json_encode([
                    'url' => $finalUrl,
                    'post_data' => json_encode($request),
                ]),
                'response_meta' => json_encode($e->getMessage())
            ]);
            throw new Exception($e);
        }
    }

    public function paycodeStatusHeader($finalUrl)
    {
        $header = getSecurityHeaders('GET',$finalUrl, "Bearer");
        $header[] = "paycode: {$this->paycode_transaction->paycode}";
        return $header;
    }

    public function getPaycodeStatus()
    {
        $url = "/api/v1/pwm/info/{$this->clean_phone}/tokens";
        $finalUrl = CFG_INTERSWITCH_BASE_URL . $url;

        try{
            $headers = $this->paycodeStatusHeader($finalUrl);
            $response_data = sendCustom($finalUrl, $headers, 'GET');
            if($response_data['status_code'] == 200)
            {
                $response = $response_data['response'];
                $expiry = $this->paycode_transaction->created_at->addMinutes($response['tokenLifeTimeInMinutes']);
                if($response['status'] != 6) {
                    $code_status = INTERSWITCH_PAYCODE_STATUSES[$response['status']];
                    $this->paycode_transaction->update([
                        'code_status' => $code_status['name'],
                        'provider_code_status' => $response['status'],
                        'expiry_date' => $expiry->toDateTimeString()
                    ]);
                }else{
                    $this->paycode_transaction->update([
                        'expiry_date' => $expiry->toDateTimeString()
                    ]);
                }
                return [
                    'reference' => $this->paycode_transaction->reference_code,
                    'status'  => true,
                    'data' => $response_data['response']
                ];
            }else{
                return [
                    'reference' => $this->paycode_transaction->reference_code,
                    'status' => false,
                    'error' => $response_data['response']
                ];
            }
        } catch (\Exception $e){

            throw $e;
        }
    }

    public function cancelPaycode()
    {
        $url = "/api/v1/pwm/info/{$this->clean_phone}/tokens";
        $statusUrl = CFG_INTERSWITCH_BASE_URL . $url;
        $cancelUrl = CFG_INTERSWITCH_BASE_URL . "/api/v1/pwm/tokens";
        $headers = getSecurityHeaders('DELETE', $cancelUrl);
        try {
            //Check Transaction Status
            $status_headers = $this->paycodeStatusHeader($statusUrl);
            $status_response_data = sendCustom($statusUrl, $status_headers, 'GET');

            if($status_response_data['status_code'] != 200 ){
                return [
                    'status' => false,
                    'message' => 'Service temporarily unavailable',
                    'error' => $status_response_data['response']
                ];
            }
            $transaction_status = $status_response_data['response']['status'];
            if($transaction_status > 1)
            {
                $code_status = INTERSWITCH_PAYCODE_STATUSES["{$transaction_status}"];
                $expiry = $this->paycode_transaction->created_at->addMinutes($status_response_data['response']['tokenLifeTimeInMinutes']);
                $this->paycode_transaction->update([
                    'code_status' => $code_status['name'],
                    'provider_code_status' => $transaction_status,
                    'expiry_date' => $expiry->toDateTimeString()

                ]);
                return [
                    'status' => false,
                    'message' =>  $code_status['description'],
                    'code_status' => $transaction_status,
                ];
            };

            /**
             * If paycode is 0: unused, 1: in-use
             *
             * Proceed to Cancel the Token
             */
            $reqData = json_encode([
                "transactionRef" => $this->paycode_transaction->reference_code,
                "frontEndPartner" => (env('APP_ENV') != 'production') ? 'SBP': 'AKU',
                ]);
            $response_data = sendCustom($cancelUrl, $headers, 'DELETE',$reqData);

            if($response_data['status_code'] != 200){
                return [
                    'status' => false,
                    'message'=> 'Something went wrong while cancelling the paycode',
                    'error' => $response_data['response']
                ];
            }
            $re_status_headers = $this->paycodeStatusHeader($statusUrl);

            /*
             * If cancellation process returned a 200 response
             *
             * Check status of transaction
             */
            $status_recheck_response = sendCustom($statusUrl, $re_status_headers, 'GET');
            if($status_recheck_response['status_code'] != 200 ){
                return [
                    'status' => false,
                    'message' => 'Service temporarily unavailable try again',
                    'error' => $status_response_data['response']
                ];
            }
            $updated_status = $status_recheck_response['response']['status'];
            $message = "Paycode cancellation is being processed";
            $status = 'queued';
            if($updated_status == 2) {
                $status = true;
                $message = "Paycode cancelled Successfully";
            }elseif($updated_status > 2) {
                $status = false;
                $message = "Failed to cancel paycode: ".INTERSWITCH_PAYCODE_STATUSES["{$updated_status}"]['description'];
            }else{
                $status = 'queued';
                queueCancelledPaycode($this->paycode_transaction->id);
            }
            $this->paycode_transaction->update([
                'code_status' => INTERSWITCH_PAYCODE_STATUSES["{$updated_status}"]['name'],
                'provider_code_status' => $updated_status
            ]);
            return [
                'status' => $status,
                'message' => $message,
                'data' => $status_recheck_response['response']
            ];

        } catch (\Exception $e){
            throw $e;
        }


    }

    public static function checkExpiredPaycodes()
    {
        CardlessWithdrawal::where('provider_channel', 'interswitch')->whereIn('provider_code_status', [0,1])->whereDate('expiry_date', '<=', Carbon::today())->oldest()->chunk(200, function ($cardlessWithdrawals) {
            foreach ($cardlessWithdrawals as $cw)
            {
                try{
                    $user = User::find($cw->user_id);
                    $transaction = new InterswitchTransactions($user, null, $cw->id);
                    $response_data = $transaction->getPaycodeStatus();
                    if($response_data['status'] == true)
                    {
                        $response = $response_data['data'];
                        $status = $response['status'];
                        if($status == 6){
                            $code_status = INTERSWITCH_PAYCODE_STATUSES[$status];
                            $expiry = $cw->created_at->addMinutes($response['tokenLifeTimeInMinutes']);
                            $cw->update([
                                'code_status' => $code_status['name'],
                                'provider_code_status' => $status,
                                'expiry_date' => $expiry->toDateTimeString()
                            ]);
                            $wallet = Wallet::where('user_id', $user->id)->first();
                            $wallet->update([
                                'amount' => $wallet->amount + $cw->amount
                            ]);
                            $smsMessage = "Your withdrawal code has expired \n, You can now access you money via the aku wallet.";
                            $smsMessage .= " Your wallet balance is NGN{$wallet->amount}";
                            queueSMS($user->phone,$smsMessage);
                        }
                        schedule_logger('paycodeExpiryCheck', 'Successful');
                    }else{
                        $trace = new \Exception(json_encode($response_data['error']));
                        schedule_logger('paycodeExpiryCheck', 'Failed', $trace);
                    }

                } catch (\Exception $e){
                    schedule_logger('paycodeExpiryCheck', 'Failed', $e);
                }
            }
        });

    }

    public static function checkCancelledPaycodes()
    {
        CancelledPaycode::where('status', self::STATUS_PENDING)->oldest()->chunk(200, function ($cancelled_paycodes){
            foreach ($cancelled_paycodes as $cp){
                $cardless_withdrawal = CardlessWithdrawal::find($cp->cardless_withdrawal_id);
                $user = $cardless_withdrawal->user;
                $transaction = new InterswitchTransactions($user, null, $cardless_withdrawal->id);

                $clean_phone = str_replace('+','',cleanUpPhone($user->phone));

                $url = "/api/v1/pwm/info/{$clean_phone}/tokens";

                $statusUrl = CFG_INTERSWITCH_BASE_URL . $url;
                $cancelUrl = CFG_INTERSWITCH_BASE_URL . "/api/v1/pwm/tokens";

                $status_headers = $transaction::paycodeStatusHeader($statusUrl);
                $cancel_headers = getSecurityHeaders('DELETE', $cancelUrl);

                try {
                    $status_response_data = sendCustom($statusUrl, $status_headers, 'GET');

                    if($status_response_data['status_code'] != 200 ){
                        throw new Exception(json_encode($status_response_data['response']));
                    }
                    $transaction_status = $status_response_data['response']['status'];
                    $code_status = INTERSWITCH_PAYCODE_STATUSES["{$transaction_status}"];
                    if($transaction_status == 2){
                        $cardless_withdrawal->update([
                            'code_status' => $code_status['name'],
                            'provider_code_status' => $transaction_status,
                        ]);
                        $current_amount = $user->wallet->amount;
                        $user->wallet->update([
                            'amount' => $current_amount + $cardless_withdrawal->amount
                        ]);
                        DB::commit();
                        $smsMSG  = "Your Paycode {$cardless_withdrawal->paycode} has been cancelled successfully\n ";
                        $smsMSG .= "Your wallet balance is {$cardless_withdrawal->amount}";
                        queueSMS($user->phone, $smsMSG);
                        $cp->update([
                            'status' => self::STATUS_SUCCESS
                        ]);
                    }elseif($transaction_status > 2) {
                        $transaction_status->update([
                            'code_status' => $code_status['name'],
                            'provider_code_status' => $transaction_status,
                        ]);
                        $smsMSG = "Request to cancel paycode {$cardless_withdrawal->paycode} failed: {$code_status['description']}";
                        queueSMS($user->phone, $smsMSG);
                        $cp->update([
                            'status' => self::STATUS_FAILED
                        ]);
                    }
                    schedule_logger('checkCancelledPaycodes', 'Successful');

                } catch (\Exception $e){
                    schedule_logger('checkCancelledPaycodes', 'Failed', $e);
                }
            }
        });
    }

}
function queueCancelledPaycode($id)
{
    CancelledPaycode::create([
        'cardless_withdrawal_id' => $id
    ]);
}