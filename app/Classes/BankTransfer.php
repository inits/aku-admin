<?php
// use Illuminate\Support\Facades\DB;

namespace App\Classes;

use App\Models\BankTransfer as Transfer;
use App\Models\User;
use App\Models\UserBank;

use App\Helper\Meta;

class BankTransfer 
{

    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 2;
    const STATUS_PENDING= 3;
    const STATUS_PROCESSING =4;

    // public function __construct() {

    // }

    static function sendErrorSMS($phone, $message) {
        queueSMS($phone, $message);
    }
    
    static function queueTransfer(array $transfer) {
        Transfer::create($transfer);
    }

    static function processTransfers() {

        Transfer::where('status' , self::STATUS_PENDING)->oldest()->chunk(250, function ($bankTransfers) {
            foreach ($bankTransfers as $transfer) {
                //
                if($transfer->stattus == self::STATUS_PROCESSING) continue;
                try{
                    // start processing transfer
                    $transfer->update(['status' => self::STATUS_PROCESSING]);

                    $user = User::where('id',$transfer->user_id)->first();

                    $overall_amount = ($transfer->amount + Meta::TRANSFER_OUT_TO_ACCT_CHARGE);
                    if ($user->wallet->amount < $overall_amount) {
                        $transfer->update([
                            'status' => self::STATUS_FAILED,
                            'meta' => json_encode([
                                'reason' => 'Insufficient funds.'
                            ])
                        ]);
                        $message = "Your bank transfer of ".number_format($transfer->amount, 2). " failed due to insufficient funds. Please fund your account and try again.";
                        self::sendErrorSMS($user->phone, $message);
                        continue;
                    }

                    // create account name if user_id, account number is not available
                    $userBank = UserBank::where('user_id', $user->id)->where('account_number', $transfer->bank_acct_no)
                        ->where('bank_id', $transfer->bank_id)
                        ->first();

                    // recipient not created before
                    if (is_null($userBank) || empty($userBank)) {
                        // get bank
                        $bank = Bank::where('id', $transfer->bank_id)->first();

                        if (is_null($bank)){
                            $transfer->update([
                                'status' => self::STATUS_FAILED,
                                'meta' => json_encode([
                                    'reason' => 'Invalid bank id'
                                ])
                            ]);
                            $message = "Your bank transfer of ".number_format($transfer->amount, 2). " failed because an invalid bank was selected. Please confirm bank account and try again.";
                            self::sendErrorSMS($user->phone, $message);
                            continue;
                        }

                        // this means that the recipient has not being created
                        // first:  get account name
                        // return [$transfer->bank_acct_no, $bank->bank_code];
                        $account_resolver = resolveAcctName($transfer->bank_acct_no, $bank->bank_code);

                        // return $account_resolver;
                        
                        if (!isset($account_resolver['name'])) {
                            $transfer->update([
                                'status' => self::STATUS_FAILED,
                                'meta' => json_encode([
                                    'reason' => 'Error occurred while resolving the account name.'
                                ])
                            ]);
                            $message = "Your bank transfer of ".number_format($transfer->amount, 2). " failed due to an error resolving bank account. Please confirm bank account and try again.";
                            self::sendErrorSMS($user->phone, $message);
                            continue;
                        }

                        $account_name = $account_resolver['name'];
                        // get user recipient code
                        $reciepient_code = createTransferRecipient($account_name, $transfer->bank_acct_no, $bank->bank_code);
                        if ($reciepient_code === false){
                            $transfer->update([
                                'status' => self::STATUS_FAILED,
                                'meta' => json_encode([
                                    'reason' => 'Error occurred while generating a transfer code for the recipient'
                                ])
                            ]);
                            $message = "Your bank transfer of ".number_format($transfer->amount, 2). " failed . Please try again.";
                            self::sendErrorSMS($user->phone, $message);
                            continue;
                        }

                        $userBank = UserBank::create([
                            'user_id' => $user->id,
                            'bank_id' => $bank->id,
                            'account_number' => $transfer->bank_acct_no,
                            'account_name' => $account_name,
                            'recipient_code' => $reciepient_code,
                        ]);

                    }


                    $reason = "Transfer_by_" . $user->phone . "_via_TraderMoni-AKU";
                    $refCode = 'aku-' . generatePaystackReferenceCode(7, 'ssss');
                    $resp = collect(payRecipient($transfer->amount, $userBank->recipient_code, $user->id, $reason, $refCode))->toArray();

                    // ddd($resp);

                    if ( isset($resp["status"]) && $resp["status"] == true ) {
                        $message = "You transferred NGN " . number_format($transfer->amount) . " to " . $userBank->account_name . " successfully.";
                        $user_wallet_transaction_log = [
                            'user_id' => $user->id,
                            'message' => $message,
                            'response_code' => $resp['reference'],
                            'response_type' => SUCCESS_TRANSACTION,
                            'transaction_type_id' => Meta::TRANSFER_MONEY_OUT_ACTION,
                            'amount' => $transfer->amount,
                            'metadata' => json_encode(['charge' => Meta::TRANSFER_OUT_TO_ACCT_CHARGE, 'payload' => $resp]),
                        ];

                        $user_activity_log = [
                            'heading' => "Fund transfer from wallet",
                            'tag' => Meta::TRANSFER_MONEY_OUT,
                            'message' => $message,
                            'status' => 0,
                            'user_id' => $user->id,
                        ];

                        $transfer->update([
                            'status' => self::STATUS_SUCCESS,
                            'meta' => json_encode([
                                'reason' => $reason,
                                'payload' => $resp
                            ])
                        ]);

                        $user->wallet->update(['amount' => ($user->wallet->amount - $overall_amount)]);
                        // $wallet = Wallet::where('user_id', $user->id)->first();
                        // Wallet::where('user_id', $user->id)->update(['amount' => ($user->wallet->amount - $overall_amount)]);

                        echo 'Bank Transfer Successful! [id]: '.$transfer->id." [ref]: ".$resp['reference']."\n";
                        
                        $smsMessage = "Transfer of NGN".number_format($transfer->amount, 2).
                                    " to " . $userBank->account_name." ".$userBank->account_number . " was successful. ";

                        $smsMessage .= "Your new balance is NGN".number_format($user->wallet->amount, 2).".";
                        queueSMS($user->phone, $smsMessage);

                    } else {
                        $message = "You transfer of NGN " . number_format($transfer->amount) . " to " . $userBank->account_name . " failed.";
                        $user_wallet_transaction_log = [
                            'user_id' => $user->id,
                            'message' => $message,
                            'response_code' => $resp['reference'],
                            'response_type' => FAILED_TRANSACTION,
                            'transaction_type_id' => Meta::TRANSFER_MONEY_OUT_ACTION,
                            'amount' => $transfer->amount,
                            'metadata' => json_encode(['payload' => $resp]),
                        ];// save it in the user wallet log.


                        $user_activity_log = [
                            'heading' => "Failed fund transfer from wallet",
                            'tag' => Meta::TRANSFER_MONEY_OUT_FAILED,
                            'message' => $message,
                            'status' => 0,
                            'user_id' => $user->id,
                        ];

                        $transfer->update([
                            'status' => self::STATUS_FAILED,
                            'meta' => json_encode([
                                'reason' => 'Error occurred trying to process transfer',
                                'payload' => $resp
                            ])
                        ]);
                        
                        $smsMessage = "Transfer of NGN".number_format($transfer->amount, 2).
                                    " to " . $userBank->account_name." ".$userBank->account_number . " failed. Please try again";
                        self::sendErrorSMS($user->phone, $smsMessage);
                    }
                    saveUserActivity($user_activity_log);
                    saveWalletLog($user_wallet_transaction_log);

                } catch(\Exception $e){
                    // ddd($e->getMessage());
                    $transfer->update([
                        'status' => self::STATUS_FAILED,
                        'meta' => json_encode([
                            'reason' => 'Error occurred trying to process transfer',
                            'error' => $e->getMessage()
                        ])
                    ]);
                    continue;
                }
            }
        });
    }
}
