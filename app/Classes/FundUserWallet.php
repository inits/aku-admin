<?php

namespace App\Helper;

use App\Models\TransactionLog;
use App\Models\User;
use App\Models\UserCard;
use App\Models\Wallet;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class FundUserWallet
{

    protected $member, $member_card, $amount, $member_wallet;

    /**
     * Create a new job instance.
     *
     * @param $member
     * @param $member_card
     *
     * @return void
     */
    public function __construct(User $member, UserCard $member_card, $amount,Wallet $member_wallet)
    {
        //
        $this->member = $member;
        $this->member_card = $member_card;
        $this->amount = $amount;
        $this->member_wallet = $member_wallet;
    }

    /**
     * Execute the job.
     *
     * @return boolean
     */
    public function handle()
    {

        //


        $client = new Client();
        $paystackSecretKey = env('PAYSTACK_SECRET_KEY', 'some_account_secret_key_1234335453');
        $url = "https://api.paystack.co/charge";
        $form_param = [
            'headers' => [
                'Authorization' => "Bearer {$paystackSecretKey}"
            ],
            'form_params' => [
                'authorization_code' => $this->member_card->authorization_code,
                'amount' => ((double)$this->amount) * 100,
                'email' => $this->member->email
            ]
        ];
        try {

            $response = $client->post($url, $form_param);

            $payload = json_decode($response->getBody()->getContents(), true);
            generic_logger($url, "POST-INTERNAL", $form_param, $payload);

            if (isset($payload['status']) && $payload['status'] == true) {
                
                $user_wallet_transaction_log = [
                    'user_id' => $this->member->id,
                    'message' => "You funded your wallet with NGN " . $this->amount . " successfully.",
                    'transaction_type_id' => Meta::FUND_WALLET_ACTION,
                    'amount' => $this->amount,
                    'metadata' => json_encode([]),
                ];
                // save it in the user wallet log.
                saveWalletLog($user_wallet_transaction_log);

                $user_activity_log= [
                    'heading'=> "Wallet funded",
                    'tag' => Meta::WALLET_FUNDED,
                    'message' => "You funded your wallet with NGN " . $this->amount . " successfully.",
                    'status' => 0,
                    'user_id' => $this->member->id,
                ];
                saveUserActivity($user_activity_log);
                

                $this->member_wallet->amount = $this->member_wallet->amount + $this->amount;
                $this->member_wallet->save();
                $pl = $payload['data'];
                $pl['transaction_type_id'] = Meta::FUND_WALLET_ACTION;
                $pl['user_id'] = $this->member->id;
                $pl['source'] = url()->current();
                $pl['reason'] = 'Fund User Wallet';
                $pl = convert_multi_array($pl);

                TransactionLog::create($pl);

                if(env('APP_ENV') == 'testing' || $this->member->email  !== substr(trim($this->member->phone), -10).'@inits.xyz')
                {
                    sendEmail(
                        "Your wallet has been funded with  NGN " . $this->amount . " successfully.",
                        'Wallet Funding',
                        env('A', 'Aku'), $this->member->email);
                }else{
                    sendSMS($this->member->phone,"Your wallet on Aku has been funded with  NGN " . $this->amount . " successfully.");
                }
                return true;
                

            } else {
                Log::debug(['App Env'=> env('APP_ENV'), 'payload' => json_encode($payload)]);
                if(env('APP_ENV') == 'testing' || $this->member->email  !== substr(trim($this->member->phone), -10).'@inits.xyz')
                {   
                    $err_msg = !$payload['status'] ? $payload['message']: json_encode($payload);
                    sendEmail(
                        "Failed to fund wallet:". $err_msg,
                        'Wallet Funding',
                        env('APP_NAME', 'Aku'), $this->member->email);
                }else{
                    sendSMS($this->member->phone,"Your wallet on Aku has been funded with  NGN " . $this->amount . " successfully.");
                }
                return true;
                $user_transaction_log = [
                    'member_id' => $this->member->id,
                    'notification_type' => 0,
                    'status' => 0,
                    'message' => "Your wallet could not be funded please check your card details and try again.",
                ];
                // \App\Models\MemberNotification::create($user_transaction_log;]

                sendEmail(
                    "Your wallet could not be funded please check your card details and try again. ",
                    'Wallet Funding Failed',
                    env('APP_NAME', 'Aku'), $this->member->email);

                return false;
            }
        } catch (\Exception $e) {
            Log::error(array_merge(['message' => $e->getMessage()]));
            generic_logger($url, 'POST-INTERNAL', $form_param, array_merge(['message' => $e->getMessage()], $e->getTrace()));
            return false;
        }
    }
}
