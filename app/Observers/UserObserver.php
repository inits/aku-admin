<?php

namespace App\Observers;

use App\Models\User;
use App\Models\Wallet;

class UserObserver
{

    //creating, created, updating, updated, deleting, deleted, saving, saved, restoring, restored.

    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function created(User $user)
    {
        Wallet::create(['user_id' => $user->id, 'amount' => 0]);
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function deleting(User $user)
    {
        Wallet::where('user_id', $user->id)->delete();
    }
}
