<?php

namespace App\Http\Middleware;

use Closure;
use Hash;

use App\Models\LoanProvider;

class ValidateLoanProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error_payload = [
            "error" => "Invalid Authorization Header",
            "message" => "Please specify a valid Authorization Header"
        ];
        $fullUrl = $request->fullUrl();
        $requestMethod = $request->method();
        $requestBody = $request->except('provider');
        $statusCode = 403;

        $auth_type = 'Basic';
        $authorization = $request->header('Authorization');

        if(!$authorization) {
            generic_logger($fullUrl,$requestMethod,$requestBody,$error_payload);
            return response()->json($error_payload,$statusCode);
        }

        if(substr( $auth_type, 0, strlen($auth_type) ) !== "Basic"){
            generic_logger($fullUrl,$requestMethod,$requestBody,$error_payload);
            return response()->json($error_payload,$statusCode);
        }

        $encoded_credentials = trim(str_replace($auth_type, '', $authorization));

        if(!$encoded_credentials) {
            generic_logger($fullUrl,$requestMethod,$requestBody,$error_payload);
            return response()->json($error_payload,$statusCode);
        }

        $decoded_credentials = base64_decode($encoded_credentials, true);

        if(!$decoded_credentials) {
            generic_logger($fullUrl,$requestMethod,$requestBody,$error_payload);
            return response()->json($error_payload,$statusCode);
        }

        $credentials = collect(explode(':', $decoded_credentials))->map(function($credential){
            return trim($credential);
        });

        $provider = LoanProvider::where('username', '=', $credentials[0])->first();

        if(!$provider) {
            generic_logger($fullUrl,$requestMethod,$requestBody,$error_payload);
            return response()->json($error_payload,$statusCode);
        }

        if (!Hash::check($credentials[1], $provider->password)) {
            // The password doesn't match
            generic_logger($fullUrl,$requestMethod,$requestBody,$error_payload);
            return response()->json($error_payload,$statusCode);
        }

        $request['provider'] = $provider;

        return $next($request);
    }
}
