<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        $AuthGuard = app('auth')->guard('admin');
        if ($AuthGuard->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            if ($AuthGuard->user()->can($permission)) {
                
                return $next($request);
            }
        }

        throw UnauthorizedException::forPermissions($permissions);
    }
}
