<?php

namespace App\Http\Controllers;

use App\Helper\Meta;
use App\Models\SmsQueue;
use App\Models\Unsave;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Loan;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = DB::table('users')
            ->join('wallets', 'user_id', '=', 'users.id')
            ->paginate(20)->setPath('');
        return view('users.list', compact('users'));
    }

    public function filter_users(Request $request, $q = null)
    {
        if ($q != "") {
            $users = DB::table('users')->join('wallets', 'user_id', '=', 'users.id')
                ->where('phone', 'LIKE', '%' . $q . '%')
                ->orWhere('name', 'LIKE', '%' . $q . '%')
                ->orWhere('email', 'LIKE', '%' . $q . '%')
                ->orWhere('amount', 'LIKE', '%' . $q . '%')->paginate(15)->setPath('');
        } else {
            $users = DB::table('users')->join('wallets', 'user_id', '=', 'users.id')
                ->paginate(15)->setPath('');
        }
        // if ($loans->count() > 0){
        //     return view ( 'users.list' )->withDetails( $users )->withQuery ( $q );
        // }
        // foreach($users as $u):
        //     var_dump($u->amount);
        // endforeach;
        // die;

        return view('users.ajax_table', compact('users'));

    }

    public function user_edit($user_id, Request $request)
    {
        if ($request->method() == "POST") {
            $requestData = $request->all();
            $id = $requestData['user_id'];
            unset($requestData['user_id']);
            unset($requestData['_token']);

            if (User::where('id', $id)->update($requestData)) {
                $request->session()->flash('success', 'User updated successfully');
                return redirect()->back();
            } else {
                $request->session()->flash('error', 'Error updating permission data');
            }
        }

        $userDetail = User::where('id', $user_id)->first()->ToArray();
        return view('users.edit', compact('userDetail'));
    }

    public function update_pin(Request $request)
    {
        if ($request->method() == "POST") {
            $requestData = $request->all();
            $id = $requestData['user_id'];
            unset($requestData['user_id']);
            unset($requestData['_token']);

            if (User::where('id', $id)->update($requestData)) {
                $request->session()->flash('success', 'User updated successfully');
                return redirect()->back();
            } else {
                $request->session()->flash('error', 'Error updating user data');
            }
        } else {
            $request->session()->flash('error', 'Error invalid data');
            return redirect()->back();
        }
    }

    public function uploaded_users()
    {
        $users = User::where('trademoni_whitelisted', 1)->with('wallet')
//            ->join('wallets', 'user_id', '=', 'users.id')
            ->paginate(20)->setPath('');

        return view('users.uploaded_users', compact('users'));
    }

    public function filter_users_uploaded(Request $request, $q = null)
    {
        if ($q != "") {
            $users = DB::table('users')
                ->join('wallets', 'user_id', '=', 'users.id')
                ->where('phone', 'LIKE', '%' . $q . '%')
                ->orWhere('name', 'LIKE', '%' . $q . '%')
                ->orWhere('email', 'LIKE', '%' . $q . '%')
                ->orWhere('amount', 'LIKE', '%' . $q . '%')->paginate(15)->setPath('');
        } else {
            $users = DB::table('users')->join('wallets', 'user_id', '=', 'users.id')
                ->paginate(15)->setPath('');
        }

        return view('users.ajax_table_uploaded', compact('users'));

    }

    public function uniqueName($filename)
    {
        date_default_timezone_set('Africa/Lagos');
        @$f = explode('.', $filename);
        @$ext = $f[count($f) - 1];
        @$uniqNewName = 'aku_file_' . uniqid() . time() . '.' . $ext;
        return $uniqNewName;
    }

    public function uploaded_files(Request $request) {
        return view('users.uploaded_files', compact('userDetail'));
    }

    public function upload_excel(Request $request)
    {
        set_time_limit(0);
        if ($request->method() == "POST") {
            $path = $request->file->path();

            if (empty($path)) {
                return response()->json(['error' => 'Empty File path']);
            }

            //file
            $file = $request->file('file');

            $fileName = $file->getClientOriginalName();
            $uploadName = $this->uniqueName($fileName);

            $rel_url = storage_path() . "/files";
            $folder_url =  $rel_url;
            $url = $rel_url . '/' . $uploadName;

            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }

            if ($file->move($folder_url, $url)) {
                $upload = DB::table('uploads')->insertGetId([
                    'upload_name' => $uploadName,
                    'process' => Meta::PENDING,
                ]);
                $request->session()->flash('success', 'File uploaded successfully');
                return redirect()->back();
            } else {
                $request->session()->flash('error', 'File could not uploaded');
                return redirect()->back();
            }
        }
        return view('users.upload', compact('userDetail'));
    }

    public function upload_user_from_excel_(Request $request)
    {
        set_time_limit(0);
        if ($request->method() == "POST") {

            $path = $request->file->path();

            if (empty($path)) {
                return response()->json(['error' => 'Empty File path']);
            }

            Excel::filter('chunk')->load($path)->chunk(50, function ($results) {

                //batch of 50
                $collections = [];
                foreach ($results as $row) {

                    $requestDataSet = (Array)$row;
                    $requestDataInsert = (Array)$requestDataSet;
                    $requestData = end($requestDataInsert);

                    $phone = $requestData['phone_number'];

                    if (isset($phone) && !empty($phone)) {
                        if ((strlen(trim(" " . $phone)) == 11)) {
                            $phone = cleanUpPhone($phone);

                            $otp = str_shuffle("" . substr(time(), -6));
                            $userData = [
                                'email' => substr($phone, -10) . '@inits.xyz',
                                'phone' => $phone,
                                'current_otp' => $otp,
                                'status' => Meta::ACCOUNT_ACTIVE,
                                'is_phone_verified' => Meta::ACCOUNT_ACTIVE,
                                'name' => $requestData['firstname'] . ' ' . $requestData['lastname'],
                                'clean_market_location' => $requestData['clean_market_location'],
                                'state' => $requestData['state'],
                                'data_source' => $requestData['data_source'],
                                'tradermoni_whitelisted' => 0,
                            ];

                            $user = DB::table('users')->where('phone', '=', $phone)->first();
                            if (empty($user) || !isset($user) || is_null($user)) {
                                $userRes = DB::table('users')->insertGetId($userData);
                                if (isset($userRes) && !empty($userRes)) {
                                    Wallet::create(['user_id' => $userRes, 'amount' => '10000']);
                                    SmsQueue::saveQueue($phone, 'You just received N10,000 from TraderMoni, to access your cash on AKU, please dial *347*111# or call 07001000258');
                                }
                            } else {
                                Unsave::saveQueue($phone, 'Phone number duplicated');
                            }

                        } else {
                            // error number is more then 11 digits
                            Unsave::saveQueue('', 'Phone number is more than 11 digits ');
                        }
                    } else {
                        // error number missing
                        Unsave::saveQueue($phone, 'Phone number is missing');
                    }
                }

                //DB::table('users')->insert($collections);
                //save here

            });

            // right after loop return response
            return response()->json(['success' => 'File done']);
        }
        return view('users.upload_user_from_excel', compact('userDetail'));
    }

}
