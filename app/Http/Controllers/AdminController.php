<?php

namespace App\Http\Controllers;

use App\Models\CardlessWithdrawal;
use App\Models\TransactionType;
use App\Models\Wallet;
use App\Models\WalletTransactionLog;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Permission;
use App\Models\Role;
use Auth;
use Illuminate\Support\Facades\DB;
use Storage;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index()
    {
        $admins = Admin::all();

        $currentYear = date('Y');
        $rawLgas = WalletTransactionLog::select(DB::raw('COUNT(id) AS count, MONTH(created_at) AS month_name, SUM(amount) AS amount_paid'))
            ->where('response_type', SUCCESS_TRANSACTION)
            ->whereYear('created_at', $currentYear)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get()
            ->toArray();

        $failedRawLgas = WalletTransactionLog::select(DB::raw('COUNT(id) AS count, MONTH(created_at) AS month_name, SUM(amount) AS amount_paid'))
            ->where('response_type', FAILED_TRANSACTION)
            ->whereYear('created_at', $currentYear)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get()
            ->toArray();

        $failedRawLgas = array_group_by($failedRawLgas, 'month_name');
        $rawLgas = array_group_by($rawLgas, 'month_name');
        $gReport = [];
        foreach ($rawLgas as $rp) {
            $gReport[$rp[0]['month_name']] = $rp[0]['count'];
        }

        $gReportRaw = [];
        foreach ($failedRawLgas as $rp) {
            $gReportRaw[$rp[0]['month_name']] = $rp[0]['count'];
        }
        $failedCount = array_sum($gReportRaw);

        $walletTransactionData = $this->treatChartData($rawLgas);
        $failedRawLgas = $this->treatChartData($failedRawLgas);
        $amountPaidTotalGet = [];
        foreach ($rawLgas as $rawPaymentga) {
            $amountPaidTotalGet[] = $rawPaymentga[0]['amount_paid'];
        }

        $amountPaidTotal = array_sum($amountPaidTotalGet);
        $users = DB::table('users')->count();

        $walletsLgas = Wallet::select(DB::raw('COUNT(id) AS count ,SUM(amount) AS amount_to_paid'))
            ->whereYear('created_at', $currentYear)
            ->get()
            ->toArray();
        $amountToPay = end($walletsLgas);

        $transactionDetails = TransactionType::pluck('name','id');
        $wallletlogsByType = WalletTransactionLog::groupBy('transaction_type_id')
            ->selectRaw('COUNT(transaction_type_id) AS transaction_type, transaction_type_id')
            ->pluck('transaction_type', 'transaction_type_id');

        return view('admin.dashboard', compact(['admins', 'walletTransactionData', 'currentYear', 'failedCount',
            'gReport', 'amountPaidTotal', 'failedRawLgas', 'users',
            'amountToPay', 'transactionDetails', 'wallletlogsByType']));
    }

    public function create()
    {
        // $permissions = Permission::pluck('display_name','id');
        $roles = Role::pluck('display_name', 'id');
        return view('admin.create', compact('roles'));
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:admins,email',
            'password' => 'required|confirmed|min:6',
            'avatar' => 'nullable|mimes:jpg,jpeg,bmp,png',
            'role' => 'required|integer|exists:roles,id',
        ]);
        $path = '';
        if ($request->has('avatar')) {
            $path = $request->file('avatar')->store('avatars', 'public');
        }
        $admin = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'avatar' => $path,
            'role' => $request->role
        ]);
        return redirect()->back()->with('success', 'Admin Created Successfully');
    }

    public function all_admins(Request $request)
    {
        $admins = Admin::paginate(15);
        return view('admin.list', compact('admins'));
    }

    public function edit(Request $request, Admin $admin)
    {
        $roles = Role::pluck('display_name', 'id');
        return view('admin.edit', compact('admin', 'roles'));
    }

    public function update(Request $request, Admin $admin)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:admins,email,' . $admin->id,
            'password' => 'nullable|confirmed|min:6',
            'avatar' => 'nullable|mimes:jpg,jpeg,bmp,png',
            'role' => 'required|integer|exists:roles,id',
        ]);
        if ($request->has('avatar')) {
            $path = $request->file('avatar')->storeAs('admin_avatars', 'admin_' . $admin->id, 'public');
        }
        $updated = $admin->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        if ($request->has('avatar')) {
            $admin->avatar = $path;
            $admin->save();
        }

        $selected_role = Role::find($request->role);
        if (!$admin->hasRole($selected_role)) {
            $admin->assignRole(Role::find($request->role));
        }
        return redirect()->back()->with('success', 'Admin Updated Successfully');
    }

    public function destroy(Request $request, Admin $admin)
    {
        $admin->delete();
        redirect()->back()->with(flash_message('success', 'Admin deleted successfully'));
    }

    public function deactivate(Request $request, Admin $admin)
    {
        $admin->deactivate();
        return redirect()->back()->with(flash_message('success', 'Admin deacticated successfully'));
    }
}
