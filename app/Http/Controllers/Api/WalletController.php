<?php

namespace App\Http\Controllers\Api;

use App\Helper\FundUserWallet;
use App\Helper\Meta;
use App\Models\UserCard;
use App\Models\Wallet;
use App\Models\Bank;
use App\Transformers\UserCardTransformer;
use App\Transformers\WalletTransformer;
use App\Transformers\BanksTransformer;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;

class WalletController extends BaseController
{

    /**
     * @SWG\Get(
     *   path="/api/get-user-wallet",
     *   summary="Get the user's wallet balance",
     *      tags={"Wallet"},
     *   @SWG\Response(
     *     response=200,
     *     description="user's wallet data"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function get_user_wallet(Request $request)
    {

        try {

            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }

            $wallet = Wallet::where('user_id', $this->user()->id)->first();
            $transformer = new WalletTransformer();

            if ($wallet->count() < 1) {
                $message = 'Something went wrong while processing your request. Wallet not found.';
                return problemResponse($message, '404', $request);
            }
            $data = $transformer->transform($wallet);
            return validResponse("User Wallet Balance", $data, $request);

        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @SWG\Get(
     *   path="/api/get-user-cards",
     *   summary="Get all of the user's cards",
     *           tags={"Wallet"},
     *   @SWG\Response(
     *     response=200,
     *     description="{
    'message': 'User Cards',
    'data': [
    {
    'id': '1',
    'card_type': 'visa DEBIT',
    'last4': '4081',
    'exp_month': '12',
    'exp_year': '2017',
    'status': 'active'
    }
    ],
    'status': true,
    'status_code': 200
    }"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function get_user_cards(Request $request)
    {

        try {
            $user_card = UserCard::where('user_id', $this->user()->id)->get();
            if (count($user_card) < 1) {
                $data = [];
                $message = 'No Card available';
                return validResponse($message, $data, $request);
            }
            $transformer = new UserCardTransformer();
            $data = $transformer->collect($user_card);
            return validResponse("User Cards", $data, $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request, $e);
        }
    }

    /**
     * @SWG\Post(
     *     path="/api/add-user-card",
     *     summary="Add a user's card",
     *     tags={"Wallet"},
     *     @SWG\Response(
     *     response="200",
     *     description="{
    'message': 'Card added successfully',
    'data': {
    'cards': [
    {
    'card_type': 'visa DEBIT',
    'last4': '4081',
    'exp_month': '12',
    'exp_year': '2017',
    'status': 'inactive'
    },
    {
    'card_type': 'visa DEBIT',
    'last4': '4081',
    'exp_month': '07',
    'exp_year': '2018',
    'status': 'active'
    }
    ]
    },
    'status': true,
    'status_code': 200
    }
    "
     * ),
     *
     *
     * @SWG\Parameter(
     *     name="token",
     *     description="token for authentication",
     *     required=true,
     *     in="query",
     *     type="string"
     * ),
     *     @SWG\Parameter(
     *     name="number",
     *     description="user's card number",
     *     required=true,
     *     in="query",
     *     type="string"
     * ),
     *       @SWG\Parameter(
     *     name="cvv",
     *     description="user's card cvv",
     *     required=true,
     *     in="query",
     *     type="string"
     * ),
     *       @SWG\Parameter(
     *     name="expiry_month",
     *     description="user's card expiry month",
     *     required=true,
     *     in="query",
     *     type="string"
     * ),
     *       @SWG\Parameter(
     *     name="expiry_year",
     *     description="user's card expiry year",
     *     required=true,
     *     in="query",
     *     type="string"
     * )
     * )
     */
    public function add_user_card(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'number' => 'required',
                'cvv' => 'required',
                'expiry_month' => 'required',
                'expiry_year' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }

            $client = new Client();

            $data = [
                'email' => $this->user()->email,
                'card' => $request->only('number', 'cvv', 'expiry_month', 'expiry_year')
            ];

            $response = $client->post("https://api.paystack.co/charge/tokenize", [
                'json' => $data,
                'headers' => [
                    'Authorization' => 'Bearer ' . env('PAYSTACK_SECRET_KEY'),
                    'Content-Type' => 'application/json'
                ]
            ]);
            DB::beginTransaction();
            $payload = json_decode($response->getBody()->getContents(), true);

            UserCard::where('user_id', $this->user()->id)->count();
            UserCard::where('user_id', $this->user()->id)->update(['status' => Meta::INACTIVE_CARD]);

            $card_transformer = new UserCardTransformer();
            $saver = $payload['data'];
            $saver['status'] = Meta::ACTIVE_CARD;
            $this->user()->user_cards()->save(new UserCard($saver));
            $cards = UserCard::where('user_id', $this->user()->id)->get();

            DB::commit();
            $data_res = [
                'cards' => $card_transformer->collect($cards)
            ];
            $message = "Card added successfully";
            return validResponse($message, $data_res, $request);

        } catch (ValidationException $validator) {
            DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            DB::rollback();
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }
    }

    /**
     * @SWG\Post(
     *   path="/api/fund-user-wallet",
     *   summary="Fund a user's wallet with a specified card",
     *     tags={"Wallet"},
     *   @SWG\Response(
     *     response=200,
     *     description="
    {
    'message': 'Wallet funded with 40,000',
    'data': {
    'amount': 80000
    },
    'status': true,
    'status_code': 200
    }"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *     name="card_id",
     *     description="the user card id",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *     name="amount",
     *     description="amount to be deposited",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *     name="pin",
     *     description="Authentication pin of the user",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function fund_wallet(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'card_id' => 'required|exists:user_cards,id',
                'amount' => 'required|numeric',
                'pin' => 'required|numeric'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            if($this->user()->pin == $request->pin)
            {
                $data = [
                    'card_id' => $request->input('card_id'),
                    'amount' => $request->input('amount')
                ];

                $user_card = UserCard::where('id', $data['card_id'])->where('user_id', $this->user()->id)->first();
                $member_wallet = Wallet::firstOrCreate(['user_id' => $this->user()->id],['amount' => 0]);
                // ddd($member_wallet);
                if ($user_card->count() < 1) {
                    $message = "Wallet funding failed. Invalid user card";
                    return problemResponse($message, '404', $request);
                }
                DB::beginTransaction();
                $initFunder = new FundUserWallet($this->user(), $user_card, $data['amount'],$member_wallet);
                $result = $initFunder->handle();
                if ($result) 
                {
                    DB::commit();
                    $transformer = new WalletTransformer();
                    $member_wallet = Wallet::where('user_id', $this->user()->id)->first();

                    $message = "Wallet funded with " . number_format($data['amount']);
                    return validResponse($message, $transformer->transform($member_wallet), $request);
                }

                $message = "Wallet funding failed.";
                return problemResponse($message, '404', $request);
            }
            else{
                $message = "Incorrect Pin";
                return problemResponse($message, '400', $request);
            }

        } catch (ValidationException $validator) {
            DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            DB::rollback();
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }
    }

    /**
     * @SWG\Post(
     *   path="/api/delete-user-card",
     *   summary="Delete the specified member's card",
     *     tags={"Wallet"},
     *   @SWG\Response(
     *     response=200,
     *     description="{
    'message': 'Card deleted successfully',
    'data': {
    'cards': [
    {
    'id': 19,
    'card_type': 'visa DEBIT',
    'last4': '4081',
    'exp_month': '12',
    'exp_year': '2017',
    'status': 'active'
    }
    ]
    },
        'status': true,
        'status_code': 200
    }"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *     name="card_id",
     *     description="the card id",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function delete_card(Request $request)
    {
        try {
            DB::beginTransaction();
            $validator = Validator::make($request->all(), [
                'card_id' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationHttpException($validator);
            }


            // first deactivate all cards
            UserCard::where('user_id', $this->user()->id)->update(['status' => Meta::INACTIVE_CARD]);

            $count_check = UserCard::where('user_id', $this->user()->id)->count();


            if (UserCard::where('user_id', $this->user()->id)->where('id', $request->card_id)->count() < 1) {
                $message = "Invalid card";
                return problemResponse($message, [], $request);

            }

            if ($count_check == 1) {

                UserCard::where('user_id', $this->user()->id)->update(['status' => Meta::ACTIVE_CARD]);
                $message = "Can't delete the only card available";
                return problemResponse($message, [], $request);

            }


            UserCard::where('user_id', $this->user()->id)->where('id', $request->card_id)->delete();
            UserCard::where('user_id', $this->user()->id)->first()->update(['status' => Meta::ACTIVE_CARD]);


            $card_transformer = new UserCardTransformer();

            $cards = UserCard::where('user_id', $this->user()->id)->get();

            DB::commit();
            $data_res = [
                'cards' => $card_transformer->collect($cards)
            ];
            $message = "Card deleted successfully";
            return validResponse($message, $data_res, $request);

        } catch (ValidationException $validator) {
            DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            
            return problemResponse($message, '400', $request);
        }  catch (\Exception $e) {
            DB::rollback();
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/activate-card",
     *   summary="Activate the specified member's card",
     *     tags={"Wallet"},
     *   @SWG\Response(
     *     response=200,
     *     description="{
    'message': 'Card Activated successfully',
    'data': {
    'cards': [
    {
    'id': 19,
    'card_type': 'visa DEBIT',
    'last4': '4081',
    'exp_month': '12',
    'exp_year': '2017',
    'status': 'active'
    }
    ]
    },
    'status': true,
    'status_code': 200
    }"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *     name="card_id",
     *     description="the card id",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function activate_card(Request $request)
    {
        try {
            DB::beginTransaction();
            $validator = Validator::make($request->all(), [
                'card_id' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationHttpException($validator);
            }


            // first deactivate all cards
            UserCard::where('user_id', $this->user()->id)->update(['status' => Meta::INACTIVE_CARD]);


            if (UserCard::where('user_id', $this->user()->id)->where('id', $request->card_id)->count() < 1) {
                $message = "Invalid card";
                return problemResponse($message, [], $request);

            }

            UserCard::where('user_id', $this->user()->id)->where('id', $request->card_id)->update(['status' => Meta::ACTIVE_CARD]);


            $card_transformer = new UserCardTransformer();

            $cards = UserCard::where('user_id', $this->user()->id)->get();

            DB::commit();
            $data_res = [
                'cards' => $card_transformer->collect($cards)
            ];

            $message = "Card activated successfully";
            return validResponse($message, $data_res, $request);

        } catch (ValidationException $validator) {
            DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            DB::rollback();
           $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }


    }
    /**
     * @SWG\Get(
     *   path="/api/get-bank-list",
     *   summary="Get list of banks",
     *      tags={"Wallet"},
     *   @SWG\Response(
     *     response=200,
     *     description="user's wallet data"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function get_bank_list(Request $request)
    {
        try {
            $allbanks = Bank::all();
            if($allbanks->count() < 1){
                $data = [];
                return validResponse("No Banks Available", $data, $request);
            }
            $transformer = new BanksTransformer();
            $data = $transformer->collect($allbanks);
            return validResponse("Bank List", $data, $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request, $e);
        }
    }

}
