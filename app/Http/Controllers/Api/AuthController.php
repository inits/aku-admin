<?php

namespace App\Http\Controllers\Api;

use App\Helper\Meta;
use App\Models\User;
use App\Models\UserActivity;
use App\Models\WalletTransactionLog;
use App\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTFactory;
use URL;


class AuthController extends BaseController
{
    use Helpers;

    //

    /**
     * @SWG\Post(
     *   path="/api/auth/login",
     *   summary="Logs the user in and sends JWT token to be sent with every request with the user's detail",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="authorization token with user's data"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="User's phone number  ",
     *     required=true,
     *     in= "formData",
     *     type="string"
     * ),
     *     @SWG\Parameter(
     *     name="pin",
     *     description="User's pin",
     *     required=true,
     *     in="formData",
     *     type="string"
     * )
     * )
     */

    public function login(Request $request)
    {
        try {
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }

            $validator = Validator::make($request->all(), [
                'phone' => 'required|max:255',
                'pin' => 'required'
            ]);

            if ($validator->fails())
                throw new ValidationException($validator);

            // Attempt to verify the credentials and create a token for the user
            $user = User::where('phone', $request->phone)->where('pin', $request->pin)->get();
            
            if (count($user) > 1) {
                return problemResponse('Corrupt User Data', '404', $request);
            }

            if (count($user) < 1) {

                return $this->onUnauthorized($request);
            }

            $user = $user->first();
            
            if(is_null($user->custom_email) && $user->email  == substr(trim($user->phone), -10).'@inits.xyz')
            {
                DB::beginTransaction();
                $user->custom_email = false;
                $user->save();
                DB::commit();
            }else{
                DB::beginTransaction();
                $user->custom_email = true;
                $user->save();
                DB::commit();
            }

            if(!is_verified($user))
            {
                $otp = str_shuffle("" . substr(time(), -6));
                $message = "Please use the OTP code: {$otp} to complete sign up";
                queueSMS($user->phone, $message);
                return problemResponse('User not verified','400',$request);
            }

            $token = JWTAuth::fromUser($user);
            // All good so return the token
            return $this->onAuthorized($token, $request);
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token

            return $this->onJwtGenerationError($e);

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/auth/sign_up",
     *   summary="sign up ogbeni ! shaprapra! ji! ma sun!",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="authorization token with user's data"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     description="User's name  ",
     *     required=true,
     *     in= "formData",
     *     type="string"
     *  ),
     *   @SWG\Parameter(
     *     name="email",
     *     description="User's name  ",
     *     required=true,
     *     in= "formData",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="User's phone",
     *     required=true,
     *     in= "formData",
     *     type="string"
     * ),
     *     @SWG\Parameter(
     *     name="pin",
     *     description="User's pin",
     *     required=true,
     *     in="formData",
     *     type="string"
     * )
     * )
     */

    public function sign_up(Request $request)
    {
        try {
            DB::beginTransaction();
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }

            $validator = Validator::make($request->all(), [
                'phone' => 'required|unique:users',
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'pin' => 'required'
            ]);

            if ($validator->fails()){
                throw new ValidationException($validator);
            }

            // Attempt to verify the credentials and create a token for the user
            $otp = str_shuffle("" . substr(time(), -6));
            $user_data = [
                'phone' => $request->phone,
                'name' => $request->name,
                'email' => $request->email,
                'pin' => $request->pin,
                'current_otp' => $otp,
                'status' => Meta::ACCOUNT_INACTIVE,
            ];

            $user = User::create($user_data);

            $token = JWTAuth::fromUser($user);


            $message = "Please use the OTP code: {$otp} to complete sign up";
            queueSMS($user->phone, $message);


            DB::commit();
            // All good so return the token
            return $this->onAuthorized($token, $request);
        } catch (ValidationException $e) {
            DB::rollback();
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);

        } catch (\Exception $e) {
            DB::rollback();
            // $message = $e->getMessage();
             $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/auth/ussd/sign_up",
     *   summary="Add new user with phone number",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="authorization token with user's data"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="User's phone",
     *     required=true,
     *     in= "formData",
     *     type="string"
     *   )
     * )
     */

    public function ussd_sign_up(Request $request)
    {
        try {
            DB::beginTransaction();
            
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
            ]);

            if ($validator->fails())
                throw new ValidationException($validator);

            // Attempt to verify the credentials and create a token for the user
            $otp = str_shuffle("" . substr(time(), -6));
            $user_data = [
                'email' => substr($request->phone,-10) .'@inits.xyz',
                'current_otp' => $otp,
                'status' => Meta::ACCOUNT_ACTIVE,
                'is_phone_verified' => Meta::ACCOUNT_ACTIVE
            ];

            $user = User::firstOrCreate(['phone' => $request->phone],$user_data)->makeVisible('pin');
            if($user->wasRecentlyCreated){
                $token = JWTAuth::fromUser($user);

                DB::commit();
                
                return $this->onAuthorized($token, $request);
            }else{
                if(!is_verified($user)){
                    $user->update([
                        "status" => Meta::ACCOUNT_ACTIVE,
                        "is_phone_verified" => Meta::ACCOUNT_ACTIVE
                    ]);
                }
                $token = JWTAuth::fromUser($user);

                return $this->onAuthorized($token, $request);
            }

        } catch (ValidationException $e) {
            DB::rollback();
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            DB::rollback();
            
            // $message = $e->getMessage(); 
              $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }

    }

    public function tradermoni_whitelisted(Request $request)
    {
        try {
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
            ]);

            if ($validator->fails())
                throw new ValidationException($validator);

            // Attempt to verify the credentials and create a token for the user
            $otp = str_shuffle("" . substr(time(), -6));

            $user = User::where('phone',$request->phone)->first();
            if(!empty($user)){
                if($user->tradermoni_whitelisted == 1) {
                    if (!is_verified($user)) {
                        $user->update([
                            "status" => Meta::ACCOUNT_ACTIVE,
                            "is_phone_verified" => Meta::ACCOUNT_ACTIVE
                        ]);
                    }
                    $user = $user->makeVisible('pin');
                    $token = JWTAuth::fromUser($user);

                    return $this->onAuthorized($token, $request);
                }
            }
            $msg = "User Not Found";
            return problemResponse($msg, 404, $request);

        } catch (ValidationException $e) {
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) { 
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }
    }

    public function generateNewUserPin(Request $request)
    {
        try{
            $user = User::find($this->user()->id);
            if(!empty($user)){
                $new_pin = rand(1000, 9999);
                $user = $user->makeVisible('pin');
                $updated = $user->update(['pin' => $new_pin]);
                $text = "Congratulations, Your TraderMoni loan of N10,000 is ready for Collection.\n";
                $text .= "Your PIN is {$new_pin}. To get your money, please dial " . env('USSD_CODE', '*347*111#') . " or " . env('IVR_CODE','07001000258') . " and select \"Paga Agent Withdrawal\" or \"Transfer to bank account\". Thank you";
                queueSMS($user->phone, $text);

                $transformer = new UserTransformer();
                if($user->pin != null){
                    $pinisset = true;
                }else{
                    $pinisset = false;
                }
                $data = ['user' =>  [
                        'id' => encrypt_decrypt('encrypt', $user->id),
                        'name' => $user->name,
                        'phone' => $user->phone,
                        'pin' => $user->pin,
                        'pin_set' => $pinisset,
                        'custom_email' => $user->custom_email,
                        'email' => $user->email
                    ]];
                return validResponse('Pin generated successfully',$data,$request);
            }else{
                return problemResponse("User Not Found", 404, $request);
            }

        }catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token

            return $this->onJwtGenerationError($e);

        }  catch(\Exception $e){
            return problemResponse('Something went wrong', 500, $request, $e);
        }
    }
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }



    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\Get(
     *   path="/api/auth/user",
     *   summary="Get logged in user details using token gotten from login",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="user's data"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function getUser(Request $request)
    {
        try {

            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }
            $transformer = new UserTransformer();
            $data = ['user' => $transformer->transform($this->user())];
            
            return validResponse("User", $data, $request);

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return problemResponse("Expired Session", '401', $request);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return problemResponse('TOKEN INVALID', '401', $request);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return problemResponse('Something went wrong while generating your token', "500", $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }

    }

    public function verifyBVN(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'bvn' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $bvn = $request->input('bvn');

            $verified_member_count = User::where('is_bvn_verified', false)->count();

            if ($verified_member_count > 3) {
                return validResponse('BVN_QUEUED', [], $request);
            }


            $client = new Client();
            $paystackSecretKey = env('PAYSTACK_SECRET_KEY', 'some_account_secret_key_1234335453');

            $response = $client->get("https://api.paystack.co/bank/resolve_bvn/{$bvn}", [
                'headers' => [
                    'Authorization' => "Bearer {$paystackSecretKey}"
                ]
            ]);
            $payload = json_decode($response->getBody()->getContents(), true);

            return validResponse('BVN_VERIFIED', $payload, $request);


        }catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            return problemResponse('EXCEPTION', '500', $request,$e);
        }
    }


    /**
     * @SWG\Post(
     *   path="/api/auth/send/otp",
     *   summary="Send OTP",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="Sends an otp  message"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     description="user's token",
     *     required=true,
     *     in= "header",
     *     type="string"
     * )
     * )
     */
    public function sendOTP(Request $request)
    {
        try {

            $otp = str_shuffle("" . substr(time(), -6));
            $message = "Please use the OTP code: {$otp} to complete your setup";
            $this->user()->update(['current_otp' => $otp]);
            $response = queueSMS($this->user()->phone, $message); //'2348141543654'
            if ($response == 100) {
                return validResponse('OTP SENT', ['sms_response' => $response, 'otp' => $otp, 'phone' => $this->user()->phone], $request);
            }
            
            $message = !is_null($response) && $response == -5 ? Meta::MULTITEXTER_ERROR[$response]: 'OTP Sending failed' ;

            $error_code = !is_null($response) && $response == -5 ? null : 500;
                
            $trace = !is_null($response) && $error_code == 500 ? Meta::MULTITEXTER_ERROR[$response]: null;
            
            return problemResponse($message, $error_code, $request, $trace);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }
    }

    /**
     * @SWG\Post(
     *   path="/api/auth/send-otp-without-token",
     *   summary="Send OTP",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="Sends an otp message"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="user's phone",
     *     required=true,
     *     in= "query",
     *     type="string"
     *  )
     * )
     */
    public function send_otp_without_token(Request $request)
    {
        try {
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $requestData = $request->all();
            if (!isset($requestData['phone'])) {
                return problemResponse('OTP_NOT_SENT', null, $request);
            }

            $otp = str_shuffle("" . substr(time(), -6));
            $message = "Please use the OTP code: {$otp} to complete your setup";
            User::where('phone', $request->phone)->update(['current_otp' => $otp]);

            $user = User::where('phone', $request->phone)->get();
            if (count($user) > 1) {
                return problemResponse('Bad User Data', '404', $request);
            }

            if (count($user) < 1) {
                return $this->onUnauthorized($request);
            }

            if ($user->count() > 0) {
                $user = $user->first();

                $response = queueSMS($user->phone, $message);
                if ($response == 100) {
                    return validResponse('OTP Sent', ['sms_response' => $response, 'otp' => $otp, 'phone' => $user->phone], $request);
                } elseif(!is_null($response)){

                    $message = $response == -5 ? Meta::MULTITEXTER_ERROR[$response]: 'OTP Sending failed' ;

                    $error_code = $response == -5 ? null : 500;
                        
                    $trace = $error_code == 500 ? Meta::MULTITEXTER_ERROR[$response]: null;
                    
                    return problemResponse($message, $error_code, $request, $trace);
                }
            }

            return problemResponse('OTP sending unsuccessful', null, $request);

        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            //   $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }
    }

    /**
     * @SWG\Post(
     *   path="/api/auth/verify/otp",
     *   summary="Verify OTP",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="returns status: true if valid otherwise false and message"
     *   ),
     *   @SWG\Parameter(
     *     name="otp",
     *     description="user's otp",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="user's phone",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function verifyOTP(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'otp' => 'required',
                'phone' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $user = User::where('phone', $request->input('phone'))->get();

            if (count($user) > 1) {
                return problemResponse('Bad User Data', '404', $request);
            }

            if (count($user) < 1) {
                return $this->onUnauthorized($request);
            }

            $user = $user->first();
            if ($request->otp == $user->current_otp) {
                //activate acct
                $user->update([
                    'status' => Meta::ACCOUNT_ACTIVE,
                    'is_phone_verified' => Meta::ACCOUNT_ACTIVE
                ]);

                $token = JWTAuth::fromUser($user);
                // All good so return the token
                return $this->onAuthorized($token, $request);

            } else {
                return problemResponse('Invalid OTP', null, $request);
            }

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }


    }

    /**
     * @SWG\Post(
     *   path="/api/auth/verify/pin",
     *   summary="Verify user's current pin",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="Pin provide is valid for this"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="pin",
     *     description="User's pin",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   )
     * )
     */

    public function verifyPIN(Request $request)
    {
        try {
            
            $validator = Validator::make($request->all(), [
                'pin'   => 'required'
            ]);

            if ($validator->fails())
                throw new ValidationException($validator);

            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }

            if(!$this->_pinIsValid($request->pin)){
                return problemResponse("Invalid Pin",403,$request);
            } else {
                return validResponse("Pin is valid",[],$request);
            }

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/auth/update_pin",
     *   summary="Change User's current pin",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="Pin Updated successfully"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="new_pin",
     *     description="New pin",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   )
     * )
     */
    public function updatePin(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'new_pin' => 'required'
            ]);

            if ($validator->fails()){
                throw new ValidationException($validator);
            }

            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }
            $user = User::find($this->user()->id);
            $user->pin = $request->new_pin;
            $user->save();

            $transformer = new UserTransformer();
            
            $data = ['user' => $transformer->transform($user)];
            return validResponse("Pin Updated Successfully", $data, $request);

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }
    }

     /**
     * @SWG\Post(
     *   path="/api/auth/mobile/update_pin",
     *   summary="Change User's current pin",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="Pin Updated successfully"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="old_pin",
     *     description="Old pin",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="new_pin",
     *     description="New pin",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   )
     * )
     */
    public function updatePinMobile(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'old_pin' => 'required',
                'new_pin' => 'required'
            ]);

            if ($validator->fails()){
                throw new ValidationException($validator);
            }

            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }

            // validate old pin
            if(!$this->_pinIsValid($request->old_pin)){
                return problemResponse("Old Pin Invalid",403,$request);
            }

            $user = User::find($this->user()->id);
            $user->pin = $request->new_pin;
            $user->save();

            $transformer = new UserTransformer();
            
            $data = ['user' => $transformer->transform($user)];
            return validResponse("Pin Updated Successfully", $data, $request);

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }
    }

    private function _pinIsValid($pin) {
        try {

            $user = User::find($this->user()->id);
            if($pin != $user->pin){
                return false;
            }else{
                return true;
            }

        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', null, $e);
        }
    }
    
    /**
     * @SWG\Post(
     *   path="/api/auth/update_profile",
     *   summary="Change User's current pin",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description="Pin Updated successfully"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     description="Name",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     description="Email Address",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="Phone Number",
     *     required=true,
     *     in="formData",
     *     type="string"
     *   )
     * )
     */
    public function updateProfile(Request $request)
    {
        try {
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$this->user()->id,
                'phone' => 'required|unique:users,phone,'.$this->user()->id,
            ]);

            if ($validator->fails()){
                throw new ValidationException($validator);
            }

            DB::beginTransaction();
            if ($this->user()->count() < 1) {
                return problemResponse("User not found", 404, $request);
            }

            $email = $this->user()->custom_email  == true ? $this->user()->email : $request->email;
            $user = User::find($this->user()->id);
            $user->update([
                'name' => $request->name,
                'email' => $email,
                'phone' => $request->phone ,
                'custom_email'=> true              
            ]);
            DB::commit();
            $transformer = new UserTransformer();
            
            $data = ['user' => $transformer->transform($user)];
            return validResponse("Profile Updated Successfully", $data, $request); 

        }  catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request, $e);
        }
    }
    
    /**
     * @SWG\Post(
     *   path="/api/auth/complete-ussd-signup",
     *   summary="Add name and email to users having only phone number",
     *      tags={"Authentication"},
     *   @SWG\Response(
     *     response=200,
     *     description=" User's data"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     description="User's name  ",
     *     required=true,
     *     in= "formData",
     *     type="string"
     *  ),
     *   @SWG\Parameter(
     *     name="email",
     *     description="User's email  ",
     *     required=true,
     *     in= "formData",
     *     type="string"
     *  )
     * )
     */

    public function complete_ussd_signup(Request $request)
    {
        try {
            DB::beginTransaction();
            if(!completed_signup($this->user())){
            
                $validator = Validator::make($request->all(), [
                    'token' => 'required',
                    'name' => 'required',
                    'email' => 'required|email|unique:users'
                ]);

                if ($validator->fails())
                    throw new ValidationException($validator);

                if (count($this->user()) < 1) {
                    return problemResponse("User not found", 404, $request);
                }
                if(!is_verified($this->user))
                    return problemResponse('User not verified','400',$request);
                
                $user_data = [
                    'name' => $request->name,
                    'email' => $request->email
                ];

                $user = User::find($this->user()->id);
                $user->update($user_data);

                // $message = "Please use the OTP code: {$otp} to complete sign up";
                // queueSMS($user->phone, $message);

                DB::commit();
                
                $transformer = new UserTransformer();
                $data = ['user' => $transformer->transform($user())];

                return validResponse('User signup completed successfully',$data, $request);
            }else{
                
                $transformer = new UserTransformer();
                $user = User::find($this->user()->id);
                $data = ['user' => $transformer->transform($user)];

                return validResponse('User already completed signup',$data, $request);
            }

        } catch (ValidationException $e) {
            DB::rollback();
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            DB::rollback();
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request,$e);
        }

    }

    public function forgotPin(Request $request)
    {
        try{
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'phone' => 'required|exists:users,phone',
                'email' => 'required_if_not:phone|email|exists:users'
            ]);
            if($validator->fails()){
                throw new ValidationException($validator);
            }
            if($request->has('phone'))
            {
                $user = User::where('phone',$request->phone)->first();
            }elseif ($request->has('email')){
                $user = User::where('email',$request->email)->first();
            }
            if(!empty($user)){
                $new_pin = rand(1000, 9999);
                $user = $user->makeVisible('pin');
                $updated = $user->update(['pin' => $new_pin]);
                $text = "Hello, Your new PIN is {$new_pin}.\n";
                $text .= " Please dial " . env('USSD_CODE', '*347*111#') . " or " . env('IVR_CODE','07001000258') . " and perform any transaction using this pin. Thank you";
                queueSMS($user->phone, $text);

                if($user->pin != null){
                    $pinisset = true;
                }else{
                    $pinisset = false;
                }
                $data = ['user' =>  [
                    'id' => encrypt_decrypt('encrypt', $user->id),
                    'name' => $user->name,
                    'phone' => $user->phone,
                    'pin' => $user->pin,
                    'pin_set' => $pinisset,
                    'custom_email' => $user->custom_email,
                    'email' => $user->email
                ]];
                return validResponse('Pin generated successfully',$data,$request);
            }else{
                return problemResponse("User Not Found", 404, $request);
            }

        }catch (ValidationException $v) {
            $errors = $v->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch(\Exception $e) {
            $message = "Something went wrong";
            return problemResponse($message, 500, $request, $e);
        }
    }
    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError($e)
    {
        return problemResponse($e->getMessage(), $e->getStatusCode(), null);

    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token, $request)
    {

        $transformer = new UserTransformer();
        $user = JWTAuth::setToken($token)->toUser();
        $t = JWTAuth::getToken();
        $payload = JWTAuth::decode($t);
        
        $data = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
            'expiry_time' => date('Y-M-d H:i:s', $payload->get('exp')),
            'user' => $transformer->transform($user)
        ];

        return validResponse("Authorisation successful", $data, $request);


    }

    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    protected function onUnauthorized($request)
    {
        return problemResponse('Invalid credentials', '401', $request);

    }

    public function deleteUserCompletely(Request $request)
    {
        try{
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'phone' => 'required'
            ]);
            if($validator->fails())
            {
                throw new ValidationException($validator);
            }
            $user = User::where('phone', $request->phone)->first();
            if(!empty($user))
            {
                if (!empty($user->wallet)){$user->wallet->delete();}
                $wtls= WalletTransactionLog::where('user_id', $user->id)->get();
                if (!empty($wtls)){
                    foreach($wtls as $wtl)
                    {
                        $wtl->delete();
                    }
                }
                $user_activities = $user->user_activities;
                if (!empty($user_activities)){
                    foreach($user_activities as $ua)
                    {
                        $ua->delete();
                    }
                }
                if (!empty($user->user_banks)){$user->user_banks->delete();}
                if (!empty($user->user_cards)){
                    foreach ($user->user_cards as $card) {
                        $card->delete();
                    }
                }
                $user->delete();
                return validResponse('User Deleted Successfully',[$user],$request);
            }
        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            return problemResponse($message, '400', $request);
        } catch(\Exception $e){
            return problemResponse('Something went wrong',500, $request, $e );
        }
    }


    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('phone', 'pin');
    }

    public function admin_reset_user_pin(Request $request)
    {
        try{
            $user = User::where('phone', $request->phone)->first();
            if(!empty($user)){
                $new_pin = rand(1000, 9999);
                $updated = $user->update(['pin' => $new_pin]);
                $text = "Hello {$user->name},";
                $text .= "Your new Tradermoni Pin is {$new_pin}.";
                queueSMS($user->phone, $text);

                $data = ['user' =>  [
                    'id' => encrypt_decrypt('encrypt', $user->id),
                    'name' => $user->name,
                    'phone' => $user->phone,
                    'pin_set' => !!$user->pin,
                    'custom_email' => $user->custom_email,
                    'email' => $user->email
                ]];
                return validResponse('Pin reset successful',$data,$request);
            }else{
                return problemResponse("User Not Found", 404, $request);
            }

        } catch(\Exception $e){
            return problemResponse('Something went wrong', 500, $request, $e);
        }
    }


    public function resetPin(Request $request) {
        try{
            $user = User::find($this->user()->id);
            if(!empty($user)){
                $new_pin = rand(1000, 9999);
                $updated = $user->update(['pin' => $new_pin]);
                $text = "Congratulations, Your pin reset was successful.\n";
                $text .= "Your new PIN is {$new_pin}.";
                queueSMS($user->phone, $text);

                $data = ['user' =>  [
                        'id' => encrypt_decrypt('encrypt', $user->id),
                        'name' => $user->name,
                        'phone' => $user->phone,
                        'pin_set' => !!$user->pin,
                        'custom_email' => $user->custom_email,
                        'email' => $user->email
                    ]];
                return validResponse('Pin reset successful',$data,$request);
            }else{
                return problemResponse("User Not Found", 404, $request);
            }

        }catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token

            return $this->onJwtGenerationError($e);

        }  catch(\Exception $e){
            return problemResponse('Something went wrong', 500, $request, $e);
        }
    }

}
