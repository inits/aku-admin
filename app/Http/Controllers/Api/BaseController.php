<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class BaseController extends Controller
{

    use Helpers;


    public function index()
    {
        return response()->json( file_get_contents(url('/apidocs.json')));
    }
}
