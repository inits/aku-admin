<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use \DB;

use App\Models\User;

use App\Helper\Meta;

class LoanController extends Controller
{
    //

    protected $request = null;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function disburseLoan() {
        // return $this->request;
        DB::beginTransaction();
        $response = null;
        $statusCode = 200;
        $fullUrl = $this->request->fullUrl();
        $requestMethod = $this->request->method();
        $requestBody = $this->request->except('provider');
        $rules = [
            "AccountNumberToCredit" => "required",
            "ReferenceNumber" => "required",
            "AmountInNaira" => "required",
            "Interest" => "required",
            "Narration" => "required",
        ];
        try{

            $validator = Validator::make($requestBody, $rules);

            if ($validator->fails()) {
                $response = [
                    "error" => "Invalid Request Data",
                    "ResponseMessage" => "Please provide the ".implode(array_keys($rules), ", ")
                ];
                $statusCode = 403;
                generic_logger($fullUrl,$requestMethod,$requestBody,$response);
                return response()->json($response, $statusCode);
            }

            $loan_provider = $this->request->provider;

            $phone = $this->request->input('AccountNumberToCredit');
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {
                $phone = $phoneUtil->parse($phone, "NG");
                // var_dump($phone);
            } catch (\libphonenumber\NumberParseException $e) {
                // var_dump($e);
                $response =[
                    "Status" => 1,
                    "ResponseMessage" => "Invalid AccountNumberToCredit or Phone number"
                ];
                generic_logger($fullUrl,$requestMethod,$requestBody,$response);
                return response()->json($response, $statusCode);
            }
            

            $phone = $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::E164);

            $user = User::firstOrCreate(['phone' => $phone]);
            $user->update(['tradermoni_whitelisted' => 1]);

            if($user->wallet->amount == 10000){

            }
            $last_loan_from_provider = $user->loans()->where('loan_provider_id', $loan_provider->id)->latest()->first();

            if($last_loan_from_provider && $last_loan_from_provider->status === Meta::LOAN_UNPAID) {
                $response =[
                    "Status" => 1,
                    "ResponseMessage" => "AccountNumberToCredit or Phone number already credited"
                ];
                generic_logger($fullUrl,$requestMethod,$requestBody,$response);
                return response()->json($response, $statusCode);
            }

            $amount = floatval($this->request->input('AmountInNaira'));
            $reason = $this->request->input('Narration');
            $interest = $this->request->input('Interest');
            $reference = $this->request->input('ReferenceNumber');

            $loan = $user->loans()->create([
                'loan_provider_id' => $loan_provider->id,
                'amount' => $amount,
                'balance' => $amount,
                'reason' => $reason,
                'meta' => json_encode($requestBody),
                'status' => Meta::LOAN_UNPAID,
                'reference' => $reference,
                'interest' => $interest,
                'provider_status' => 0
            ]);

            $user_wallet = $user->wallet()->first();

            $user_wallet->update(['amount' => $user_wallet->amount + $amount]);

            saveWalletLog([
                'transaction_type_id' => Meta::LOAN_RECEIVED,
                'user_id' => $user->id,
                'amount' => $amount,
                'metadata' => json_encode($requestBody),
                'message' => $reason
            ]);

            $response =[
                "Status" => 0,
                "ResponseMessage" => "{$phone} credited with {$amount} successfully"
            ];
            generic_logger($fullUrl,$requestMethod,$requestBody,$response);
            
            DB::commit();

            $smsMessage = "Congratulations. Your TraderMoni loan of =N={$amount} has been credited to your ".env('APP_NAME', "")." wallet. Dial ".env('USSD_CODE', "")." or 07001000258 to withdraw or use your money.";
            // $smsMessage = "You have been credited with NGN{$amount} from {$loan_provider->name}. Dial ".env('USSD_CODE', "")." to access your money.";
            queueSMS($phone, $smsMessage);

            return response()->json($response, $statusCode);

        }
        catch(\Exception $e) {
            DB::rollback();
            // ddd($e->getMessage());
            $response =[
                "Status" => 1,
                "ResponseMessage" => "An Error Occurred Trying to credit account"
            ];
            generic_logger($fullUrl,$requestMethod,$requestBody,compact('response', 'e'));
            return response()->json($response, $statusCode);
        }
    }
}
