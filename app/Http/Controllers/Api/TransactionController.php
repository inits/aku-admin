<?php

namespace App\Http\Controllers\Api;

use App\Helper\Meta;
use App\Models\Bank;
use App\Models\CardlessWithdrawal;
use App\Models\User;
use App\Models\UserBank;
use App\Models\Wallet;
use App\Models\WalletTransactionLog;
use App\Classes\InterswitchTransactions;
use App\Transformers\WalletTransactionTransformer;
use App\Transformers\WalletTransformer;
use App\Transformers\CardlessWithdrawalTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\Classes\Withdrawal;
use App\Classes\BankTransfer;
use Mockery\Exception;

class TransactionController extends BaseController
{

    /**
     * @SWG\Get(
     *   path="/api/get-recent-transactions",
     *   summary="Get the user's wallet balance",
     *      tags={"Transaction"},
     *   @SWG\Response(
     *     response=200,
     *     description=" [
    'message': 'Recent transaction',
    'data': {
    'load': [
    {
    'transaction_type': 'Sam',
    'amount': '30',
    'message': 'ekwe ekwe ekwe ',
    'header': 'Sample'
    }
    ],
    'total': 38,
    'current_page': 1,
    'last_page': 38
    },
    'status': true,
    'status_code': 200
    ]"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     description="The page to pick. Defaults to 1",
     *     required=false,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),,
     *   @SWG\Parameter(
     *     name="records_per_page",
     *     description="Number of transactions that shouldbe displayed on each page",
     *     required=false,
     *     in="query",
     *     type="string"
     * )
     * )
     */
    public function get_recent_transactions(Request $request)
    {

        try {
            $paginate_page = $request->page;
            $wallet_tran_transformer = new WalletTransactionTransformer();
            $paginate = $request->records_per_page ?? 10;
            $wal_tran_logs = WalletTransactionLog::where('user_id', $this->user()->id)->orderBy('created_at', 'desc')->paginate($paginate);
            $data = $wallet_tran_transformer->collect_pagination($wal_tran_logs);

            return validResponse("Recent transaction", $data, $request);

        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @param Request $request
     * optional parameters are the {provider or channel}, {page} and {records_per_page}
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function get_recent_cardless_withdrawals(Request $request)
    {
        try {
            $paginate_page = $request->page;
            $wallet_tran_transformer = new CardlessWithdrawalTransformer();
            $paginate = $request->records_per_page ?? 10;
            $user = $this->user();
            if($request->has('channel')) {
                $withdrawals = CardlessWithdrawal::where([
                    'user_id' =>  $user->id,
                    'provider_channel' => $request->channel
                ])->orderBy('created_at', 'desc')->paginate($paginate);
            }else{
                $withdrawals = CardlessWithdrawal::where('user_id',$user->id)->orderBy('created_at', 'desc')->paginate($paginate);
            }
            $data = $wallet_tran_transformer->collect_pagination($withdrawals);

            return validResponse("Recent transaction", $data, $request);

        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            return problemResponse($message, '500', $request, $e);
        }

    }
    /**
     * @SWG\Post(
     *   path="/api/transfer-to-account",
     *   summary="Transfer to an account",
     *      tags={"Transaction"},
     *   @SWG\Response(
     *     response=200,
     *     description="
    {
    'message': 'Fund Transfer to ONWUASOANYA, GEORGE NDUBUISI was successful',
    'data': [],
    'status': true,
    'status_code': 200
    }
    "
     *   ),
     *   @SWG\Parameter(
     *     name="bank_id",
     *     description="bank id",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="bank_acct_no",
     *     description="Account number",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="amount",
     *     description="amount to transfer",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="pin",
     *     description="pin to confirm the request",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function transfer_to_account(Request $request)
    {
         if(checkEnabled() == false) {
             return response()->json([
                'data' => [ 'message' => 'This service currently under maintainance until July 31st 2018. Thank you.']
             ]);
        }
        try {
            $validator = Validator::make($request->all(), [
                'pin' => 'required',
                'amount' => 'required',
                'bank_acct_no' => 'required',
                'bank_id' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $user = $this->user();
            if ($user->pin != $request->pin) {
                $message = "Invalid pin";
                return problemResponse($message, [], $request);
            }

            if (is_null($user->wallet)) {
                Wallet::create(['user_id' => $user->id, 'amount' => 0]);
            }

            $overall_amount = ($request->amount + Meta::TRANSFER_OUT_TO_ACCT_CHARGE);
            if ($user->wallet->amount < $overall_amount) {
                return problemResponse("Insufficient funds.", '403', $request);
            }
            $refCode = 'aku-' . generatePaystackReferenceCode(7, 'ssss');

            // Add transfer to queue

            BankTransfer::queueTransfer([
                'user_id' => $user->id,
                'amount' => $request->amount,
                'bank_acct_no' => $request->bank_acct_no,
                'bank_id' => $request->bank_id
            ]);


            $message = "Your bank transfer request has been received successfully and is being processed. Thank you";

            $wallet = Wallet::where('user_id', $user->id)->first();

            $transformer = new WalletTransformer();
            $data_res = ['wallet' => $transformer->transform($wallet)];
            return validResponse($message, $data_res, $request);

        } catch (ValidationException $validator) {
            // DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request.';
            $user_wallet_transaction_log = [
                'user_id' => $user->id,
                'message' => $message,
                'response_code' => $refCode,
                'response_type' => FAILED_TRANSACTION,
                'transaction_type_id' => Meta::TRANSFER_MONEY_OUT_ACTION,
                'amount' => $request->amount,
                'metadata' => json_encode($e->getTrace()),
            ];

            saveWalletLog($user_wallet_transaction_log);
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/transfer-to-individual",
     *   summary="transfer to a user's wallet ",
     *      tags={"Transaction"},
     *   @SWG\Response(
     *     response=200,
     *     description="{
    'message': 'Your wallet transfer of  NGN 4,500 to George Baba was successfully.',
    'data': {
    'wallet': {
    'amount': 35450
    }
    },
    'status': true,
    'status_code': 200
    }"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="recipient phone number",
     *     required=false,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="amount",
     *     description="amount to transfer",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="pin",
     *     description="pin to confirm the request",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function transfer_to_individual(Request $request)
    {
        if(!activeServiceType('bank_transfer')){
            $user = $this->user();
            $message = "You cannot transfer money at this time Try again tomorrow.";
            $user_wallet_transaction_log = [
                'user_id' => $user->id,
                'message' => "Bank Transfer Service Off",
                'response_code' => null,
                'response_type' => FAILED_TRANSACTION,
                'transaction_type_id' => Meta::TRANSFER_MONEY_OUT_ACTION,
                'amount' => ($request->has('amount')) ? $request->amount : null
            ];

            $user_activity_log = [
                'heading' => "Fund transfer from wallet",
                'tag' => Meta::TRANSFER_MONEY_OUT,
                'message' => $message,
                'status' => 0,
                'user_id' => $user->id,
            ];
            saveUserActivity($user_activity_log);
            saveWalletLog($user_wallet_transaction_log);
            return problemResponse($message,400, $request);
        }
        try {
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'pin' => 'required',
                'amount' => 'required',
                'phone' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $user = $this->user();

            if ($user->pin != $request->pin) {
                $message = "Invalid pin";
                return problemResponse($message, [], $request);
            }

            $user_checker = User::firstOrCreate(['phone'=> $request->phone], [
                            'email' => substr($request->phone, -10).'@inits.xyz'
                        ]);
            
            DB::beginTransaction();
            if ($request->phone == $user->phone) {

                $message = "Recipient and sender should not be the same";
                return problemResponse($message, '404', $request);
            }

            $recipient = $user_checker;

            if (is_null($user->wallet)) {
                Wallet::create(['user_id' => $user->id, 'amount' => 0]);
            }
            
            if (is_null($recipient->wallet)) {
                Wallet::create(['user_id' => $recipient->id, 'amount' => 0]);
            }

            $overall_amount = ($request->amount + Meta::TRANSFER_OUT_TO_WALLET_CHARGE);

            if ($user->wallet->amount < $overall_amount) {
                return problemResponse("Insufficient funds.", '403', $request);
            }

            // Wallet::where('user_id', $this->user()->id)->update(['amount' => ($user->wallet->amount - $overall_amount)]);
            $user->wallet->update(['amount' => ($user->wallet->amount - $overall_amount)]);
            // Wallet::where('user_id', $recipient->id)->update(['amount' => ($recipient->wallet->amount + $overall_amount)]);
            $recipient->wallet->update(['amount' => ($recipient->wallet->amount + $overall_amount)]);

            if($user_checker->wasRecentlyCreated){
                $phone = substr($user->phone,-10);
                $otp = str_shuffle("" . substr(time(), -6));
                $message = "You have received N{$request->amount} from a 0{$phone}. Dial *347*111# to use it";
                queueSMS($user_checker->phone, $message);
                DB::commit();
            }
            $recipient_info = $recipient->name ?? $recipient->phone;
            $message = "Your wallet transfer of  NGN " . number_format($request->amount, 2) . " to " . $recipient_info . " was successfully.";
            $message_recipient = "NGN " . number_format($request->amount) . " has been transferred to your wallet by " . $user->name ?? $user->phone . ".";

            $user_wallet_transaction_log = [
                'user_id' => $user->id,
                'message' => $message,
                'transaction_type_id' => Meta::WALLET_TRANSFER_ACTION,
                'amount' => $request->amount,
                'metadata' => json_encode([]),
            ];

            $recipient_wallet_transaction_log = [
                'user_id' => $recipient->id,
                'message' => $message_recipient,
                'transaction_type_id' => Meta::FUND_WALLET_ACTION,
                'amount' => $request->amount,
                'metadata' => json_encode([]),
            ];

            $user_activity_log = [
                'heading' => "Wallet fund transfer",
                'tag' => Meta::WALLET_TRANSFER,
                'message' => $message,
                'status' => 0,
                'user_id' => $user->id,
            ];

            $recipient_activity_log = [
                'heading' => "Wallet funded",
                'tag' => Meta::WALLET_FUNDED,
                'message' => $message_recipient,
                'status' => 0,
                'user_id' => $user->id,
            ];


            saveUserActivity($user_activity_log);
            saveUserActivity($recipient_activity_log);

            saveWalletLog($user_wallet_transaction_log);
            saveWalletLog($recipient_wallet_transaction_log);


            DB::commit();

            $smsMessage = "Your wallet transfer of  NGN " . number_format($request->amount, 2) . " to " . $recipient_info . " was successful. ";
            $smsMessage = $smsMessage. "Your new balance is NGN".number_format($user->wallet->amount, 2).".";
            $sender = $user->name ?? $user->phone;
            $recipientMessage = "You have received NGN".number_format($request->amount, 2)." from ".$sender;
            $recipientMessage .= ". Your new balance is NGN".number_format($recipient->wallet->amount, 2).".";
            queueSMS($user->phone, $smsMessage);
            queueSMS($recipient->phone, $recipientMessage);

            $wallet = Wallet::where('user_id', $this->user()->id)->first();
            $transformer = new WalletTransformer();
            $data_res = ['wallet' => $transformer->transform($wallet)];


            return validResponse($message, $data_res, $request);
        } catch (ValidationException $validator) {
            DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            DB::rollback();
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/generate-cardless-withdrawal",
     *   summary="generate cardless withdrawal from wallet ",
     *      tags={"Transaction"},
     *   @SWG\Response(
     *     response=200,
     *     description="{
    'message': 'Withdrawal code generated successfully',
    'data': {
    'wallet': {
    'amount': 57780
    },
    'withdrawal_code': 'WvG7MC'
    },
    'status': true,
    'status_code': 200
    }
    "
     *   ),
     *   @SWG\Parameter(
     *     name="amount",
     *     description="amount to generate code for",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="pin",
     *     description="User's pin to confirm the request",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */
    public function generate_cardless_withdrawal(Request $request)
    {

        if(!activeServiceType('cardless_withdrawal')){
            $user = $this->user();
            $message = "You cannot transfer money at this time. Try again tomorrow.";
            $user_wallet_transaction_log = [
                'user_id' => $user->id,
                'message' => "Cardless Withdrawal Service Off",
                'response_code' => null,
                'response_type' => FAILED_TRANSACTION,
                'transaction_type_id' => Meta::TRANSFER_MONEY_OUT_ACTION,
                'amount' => ($request->has('amount')) ? $request->amount : null
            ];

            $user_activity_log = [
                'heading' => "Fund transfer from wallet",
                'tag' => Meta::TRANSFER_MONEY_OUT,
                'message' => $message,
                'status' => 0,
                'user_id' => $user->id,
            ];
            saveUserActivity($user_activity_log);
            saveWalletLog($user_wallet_transaction_log);
            return problemResponse($message,400, $request);

        }
        try {
            
            $validator = Validator::make($request->all(), [
                'pin' => 'required',
                'amount' => 'required'
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $user = $this->user();
            
            if ($user->pin != $request->pin) {
                $message = "Invalid pin";
                return problemResponse($message, [], $request);
            }

            if (is_null($user->wallet)) {
                Wallet::create(['user_id' => $user->id, 'amount' => 0]);
            }

            $overall_amount = ($request->amount + Meta::CARDLESS_WITDRAWAL_CHARGE);

            if ($user->wallet->amount < $overall_amount) {
                return problemResponse("Insufficient funds.", '403', $request);
            }


            $transactionReference = generateRandomReferenceCode();


            $withdrawal = new Withdrawal();

            $withdrawal_response = $withdrawal->getWithdrawalCode([
                'referenceNumber' => $transactionReference,
                'amount' => $request->amount,
                'destinationAccount' => $user->phone,
                'sendWithdrawalCode' => true
            ]);

            // ddd($withdrawal_response);
            
            if(!$withdrawal_response || $withdrawal_response['responseCode'] != 0) {
                $message = 'Unable to get withdrawal code';
                $user_wallet_transaction_log = [
                    'user_id' => $user->id,
                    'message' => $message,
                    'response_code' => $transactionReference,
                    'response_type' => FAILED_TRANSACTION,
                    'transaction_type_id' => Meta::TRANSFER_MONEY_OUT_ACTION,
                    'amount' => $request->amount,
                    'metadata' => json_encode($withdrawal_response)
                ];
                saveWalletLog($user_wallet_transaction_log);
                throw new \Error($message);
            }
            
            // do deduction from wallet
            // Wallet::where('user_id', $this->user()->id)->update(['amount' => ($user->wallet->amount - $overall_amount)]);
            $user->wallet->update(['amount' => ($user->wallet->amount - $overall_amount)]);

            $log = [
                'user_id' => $user->id,
                'reference_code' => $transactionReference,
                'provider_channel' => 'paga',
                'status' => SUCCESS_TRANSACTION,
                'amount' => $request->amount,
                'charge' => Meta::CARDLESS_WITDRAWAL_CHARGE,
                'meta' => json_encode($withdrawal_response)
            ];
            $withrawal_request = CardlessWithdrawal::create($log);

            // save log
            $message = "Withdrawal code for NGN " . number_format($request->amount) . " was generated successfully. Code is " . $transactionReference;
            $user_wallet_transaction_log = [
                'user_id' => $user->id,
                'message' => $message,
                'response_code' => $transactionReference,
                'response_type' => SUCCESS_TRANSACTION,
                'transaction_type_id' => Meta::WITHDRAWAL_CODE_GENERATED,
                'amount' => $request->amount,
                'metadata' => json_encode([
                    'charge' => Meta::CARDLESS_WITDRAWAL_CHARGE,
                    'provider_response' => json_encode($withdrawal_response)
                ]),
            ];

            $user_activity_log = [
                'heading' => "Generated withdrawal code for NGN " . number_format($request->amount),
                'tag' => Meta::GENERATED_WITHDRAWAL_CODE,
                'message' => $message,
                'status' => 0,
                'user_id' => $user->id,
            ];
            $email_message = "Cardless Withdrawal Request".
                "<br /> Amount: {$request->amount}".
                "<br /> Phone: {$user->phone}".
                "<br /> Reference Id: {$withrawal_request->id}".
                "<pre>      {$user->phone}      {$withrawal_request->id}    {$request->amount}</pre>";
            $email_subject = "Cardless withdrawal request |Ref: {$withrawal_request->id} ,  Amount: {$request->amount}, Phone: {$user->phone}";
            sendEmail($email_message, $email_subject, "hello@aku.ng", "aku@initsng.com", null);

            if(!is_null($withdrawal_response['withdrawalCode'])){
                $smsMessage = "Your withdrawal of NGN".number_format($request->amount, 2)." was successful. Your code is {$withdrawal_response['withdrawalCode']}. Visit mypaga.com or any Paga agent to collect.";
            }else{
                $smsMessage = "Your withdrawal of NGN".number_format($request->amount, 2)." was successful and placed into your Paga account.";
            }
            $smsMessage .= " Your new balance is NGN".number_format($user->wallet->amount, 2).".";
            queueSMS($user->phone, $smsMessage);

            saveUserActivity($user_activity_log);
            saveWalletLog($user_wallet_transaction_log);

            $message = "Withdrawal code generated successfully";

            $wallet = Wallet::where('user_id', $this->user()->id)->first();
            $transformer = new WalletTransformer();
            $data_res = ['wallet' => $transformer->transform($wallet), 'withdrawal_code' => $withdrawal_response['withdrawalCode']];


            return validResponse($message, $data_res, $request);

        } catch (ValidationException $validator) {
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            $message = 'Something went wrong while processing your request. Please Try again.';
            return problemResponse($message, '500', $request, $e);
        }

    }

    /**
     * @SWG\Post(
     *   path="/api/buy-airtime",
     *   summary="buy airtime for a phone number ",
     *      tags={"Transaction"},
     *   @SWG\Response(
     *     response=200,
     *     description="{
    'message': 'NGN 50 transferred to +2348163229099 successful',
    'data': {
    'wallet': {
    'amount': 57504
    }
    },
    'status': true,
    'status_code': 200
    }"
     *   ),
     *   @SWG\Parameter(
     *     name="amount",
     *     description="amount to generate code for",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="phone",
     *     description="phone number tp credit",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="pin",
     *     description="pin to confirm the request",
     *     required=true,
     *     in= "query",
     *     type="string"
     * ),
     *   @SWG\Parameter(
     *     name="token",
     *     description="authorization token",
     *     required=true,
     *     in= "query",
     *     type="string"
     * )
     * )
     */

    public function buy_airtime(Request $request)
    {
        //    DB::beginTransaction();
        try {
            if($request->has('phone')){
                $request->phone = cleanUpPhone($request->phone);
            }
            $validator = Validator::make($request->all(), [
                'pin' => 'required',
                'amount' => 'required',
                'phone' => 'required',
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator->errors());
            }

            $user = $this->user();

            if ($user->pin != $request->pin) {
                $message = "Invalid pin";
                return problemResponse($message, [], $request);
            }

            if (is_null($user->wallet)) {
                Wallet::create(['user_id' => $user->id, 'amount' => 0]);
            }

            $overall_amount = ($request->amount + Meta::BUY_AIRTIME_CHARGE);

            if ($user->wallet->amount < $overall_amount) {
                return problemResponse("Insufficient funds.", '403', $request);
            }

            $phone = cleanUpPhone($request->phone);
            if (is_null($phone)) {
                return problemResponse("Invalid phone number.", '404', $request);
            }

            $response = rechargeAPhone($phone, $request->amount);
            if (is_null($response['success'])) {
                $message = "This appears to be a duplicate transaction. Please wait at least five minutes before trying to recharge again or change the amount";

                // this is an error
                $logMessage = "Transfer of NGN " . number_format($request->amount, 2) . " to " . $phone . " failed";
                $user_wallet_transaction_log = [
                    'response_code' => '#####',
                    'response_type' => FAILED_TRANSACTION,
                    'user_id' => $user->id,
                    'message' => $message,
                    'transaction_type_id' => Meta::BUY_AIRTIME,
                    'amount' => $request->amount,
                    'metadata' => json_encode($response['data']),
                ];
                saveWalletLog($user_wallet_transaction_log);

                return problemResponse($message, '404', $request);
            }

            // this will happen if there is a problem
            if ($response['success'] === false) {
                $message = "Error occurred while trying to top up " . $phone;
                if(!is_null($response['data']['message'])){
                    if(strpos($response['data']['message'], 'duplicate')){
                        $message = "This appears to be a duplicate transaction. Please wait at least five minutes before trying to recharge again or change the amount";      
                    }
                }

                // this is an error
                $logMessage = "Transfer of NGN " . number_format($request->amount, 2) . " to " . $phone . " failed";
                $user_wallet_transaction_log = [
                    'response_code' => '#####',
                    'response_type' => FAILED_TRANSACTION,
                    'user_id' => $user->id,
                    'message' => $logMessage,
                    'transaction_type_id' => Meta::BUY_AIRTIME,
                    'amount' => $request->amount,
                    'metadata' => json_encode($response['data']),
                ];
                saveWalletLog($user_wallet_transaction_log);

                return problemResponse($message, '404', $request);
            }

            // do deduction from wallet
            // Wallet::where('user_id', $this->user()->id)->update(['amount' => ($user->wallet->amount - $overall_amount)]);
            $user->wallet->update(['amount' => ($user->wallet->amount - $overall_amount)]);

        //    DB::commit();

            $smsMessage = "Your airtime transfer of NGN " . number_format($request->amount, 2) . " to " . $phone . " was successful. ";
            $smsMessage .= " Your new balance is NGN".number_format($user->wallet->amount, 2).".";
            queueSMS($user->phone, $smsMessage);


            // save log
            $message = "NGN " . number_format($request->amount, 2) . " transferred to " . $phone . " successful";
            $user_activity_log = [
                'heading' => "Airtime purchase",
                'tag' => Meta::AIRTIME_PURCHASE,
                'message' => $message,
                'status' => 0,
                'user_id' => $user->id,
            ];
            $user_wallet_transaction_log = [
                'response_code' => $response['data']['request_id'],
                'response_type' => SUCCESS_TRANSACTION,
                'user_id' => $user->id,
                'message' => $message,
                'transaction_type_id' => Meta::BUY_AIRTIME,
                'amount' => $request->amount,
                'metadata' => json_encode($response['data']),
            ];
            saveUserActivity($user_activity_log);
            saveWalletLogDev($user_wallet_transaction_log);

            // get user current updated wallet details
            $wallet = Wallet::where('user_id', $this->user()->id)->first();
            $transformer = new WalletTransformer();
            $data_res = ['wallet' => $transformer->transform($wallet)];
            return validResponse($message, $data_res, $request);

        } catch (ValidationException $validator) {
        //    DB::rollback();
            $errors = $validator->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);
            
            return problemResponse($message, '400', $request);
        } catch (\Exception $e) {
            // DB::rollback();
            $message = 'Something went wrong while processing your request.';
            // $message = $e->getMessage();
            return problemResponse($message, '500', $request, $e);
        }

    }

    public function generate_paycode(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'amount' => 'required'
            ]);
            if($validator->fails())
            {
                throw new ValidationException($validator);
            }
            $user = $this->user();
            if(($request->amount % 1000) > 0){
                $message = "Invalid Amount, Amount must be multilples of 1000";
                saveUserActivity([
                    'user_id' => $user->id,
                    'heading' => "Failed Withdrawal Code Generation",
                    'tag' => Meta::GENERATED_WITHDRAWAL_CODE,
                    'message' => $message,
                    'status' => 0,
                ]);
                saveWalletLog([
                    'user_id' => $user->id,
                    'message' => $message,
                    'response_code' => '#####',
                    'response_type' => FAILED_TRANSACTION,
                    'transaction_type_id' => Meta::WITHDRAWAL_CODE_GENERATED,
                    'amount' => $request->amount,
                    'metadata' => json_encode([])
                ]);
                return problemResponse($message, 403, $request);
            }
            $wallet = Wallet::firstOrCreate(['user_id' => $user->id],['amount'=> 0]);
            if($wallet->amount < $request->amount)
            {
                $message = "Insufficient Funds";
                saveUserActivity([
                    'user_id' => $user->id,
                    'heading' => "Failed Withdrawal Code Generation",
                    'tag' => Meta::GENERATED_WITHDRAWAL_CODE,
                    'message' => $message,
                    'status' => 0,
                ]);
                saveWalletLog([
                    'user_id' => $user->id,
                    'message' => $message,
                    'response_code' => '#####',
                    'response_type' => FAILED_TRANSACTION,
                    'transaction_type_id' => Meta::WITHDRAWAL_CODE_GENERATED,
                    'amount' => $request->amount,
                    'metadata' => json_encode([])
                ]);
                return problemResponse($message, 400, $request);
            }

            $t = new InterswitchTransactions($user, $request->amount);
            $res = $t->getPaycode();
            if($res['status'] == true){
                $wallet = Wallet::where('user_id', $this->user()->id)->first();
                $wallet->update([
                    'amount' => $wallet->amount - $request->amount
                ]);
                $response_data = $res['data'];
                $data = [
                    'token' => $response_data['payWithMobileToken'],
		            'reference'=>$response_data['reference'],				 
                    'token_lifetime' => $response_data['tokenLifeTimeInMinutes']
                    ];
                $smsMessage = "Your withdrawal of NGN".number_format($request->amount, 2)." was successful. Your code is {$data['token']}. Visit any ATM and collect your cash using the paycode option and your PIN";
                $smsMessage .= " Your new balance is NGN".number_format($user->wallet->amount, 2).".";
                queueSMS($user->phone, $smsMessage);

                //LOG THIS TRANSAACTION
                $message = "Cardless Withdrawal token generated successfully";
                saveUserActivity([
                    'user_id' => $user->id,
                    'heading' => "WithdrwalCode Genereted Successfully",
                    'tag' => Meta::GENERATED_WITHDRAWAL_CODE,
                    'message' => $message,
                    'status' => 0,
                ]);
                saveWalletLog([
                    'user_id' => $user->id,
                    'message' => $message,
                    'response_code' => '#####',
                    'response_type' => SUCCESS_TRANSACTION,
                    'transaction_type_id' => Meta::WITHDRAWAL_CODE_GENERATED,
                    'amount' => $request->amount,
                    'metadata' => json_encode([])
                ]);
                return validResponse($message, $data, $request);
            } else{
                $message = 'Withdrawal code generation failed';
                $wallet_transaction_logs = [
                    'user_id' => $user->id,
                    'response_code' => '#####',
                    'response_type' => FAILED_TRANSACTION,
                    'message' => $message,
                    'transaction_type_id' => Meta::WITHDRAWAL_CODE_GENERATED,
                    'amount' => $request->amount,
                    'metadata' => json_encode($res['error'])
                ];
                saveUserActivity($wallet_transaction_logs);
                saveWalletLog([
                    'user_id' => $user->id,
                    'message' => $message,
                    'response_code' => '#####',
                    'response_type' => FAILED_TRANSACTION,
                    'transaction_type_id' => Meta::WITHDRAWAL_CODE_GENERATED,
                    'amount' => $request->amount,
                    'metadata' => json_encode([])
                ]);
                return problemResponse("Unable to generate paycode, please try later", 401, $request);
            }

        } catch (ValidationException $e) {
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e){
            $msg = 'Service is currently unavailable. Please Try again later.';
            return problemResponse($msg, '500', $request, $e);
        }


    }

    public function get_paycode_status(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'transaction_id' => [
                    'required',
                    function($attribute, $value, $fail) {
                        if(empty($this->user()->cardless_withdrawals()->find($value))){
                            return $fail("$attribute not found in user's transactions");
                        }
                    }]
            ]);

            if($validator->fails())
            {
                throw new ValidationException($validator);
            }
            $user = $this->user();

            if(empty($user->cardless_withdrawals()->find($request->transaction_id))){
                return problemResponse("User transaction not found", 404, $request);
            }
            $trans = $user->cardless_withdrawals()->find($request->transaction_id);
            if($trans->status == 0 || !isset($trans->paycode)){
                $msg = "No paycode was generated during this transaction";
                return problemResponse($msg, 400, $request);
            }
            $transaction = new InterswitchTransactions($user, null, $request->transaction_id);
            $response = $transaction->getPaycodeStatus();
            $reference_transaction = CardlessWithdrawal::find($request->transaction_id);
            if($response['status'] == true) {
                $transformer = new CardlessWithdrawalTransformer();
                $cw = $transformer->transform($reference_transaction);
                $data = ['cardless_withdrawal' => $cw];
                return validResponse("Paycode Status", $data, $request);
            }else{
                return problemResponse("Unable to check status, please try later", 401, $request);
            }

        } catch (ValidationException $e){
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e){
            $msg = 'Service Currently Unavailable try later';
            return problemResponse($msg, 500, $request, $e);
        }
    }

    public function cancel_paycode(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'transaction_id' => [
                    'required',
                    function($attribute, $value, $fail) {
                        if(empty($this->user()->cardless_withdrawals()->find($value))){
                            return $fail("$attribute not found in user's transactions");
                        }
                    }]
            ]);

            if($validator->fails())
            {
                throw new ValidationException($validator);
            }
            $user = $this->user();
            $reference_transaction = CardlessWithdrawal::find($request->transaction_id);
            if(empty($reference_transaction)){
                return problemResponse('Transaction does not exists', 404, $request);
            }
            if($reference_transaction->status == 0 || !isset($reference_transaction->paycode)){
                $msg = "No paycode was generated during this transaction";
                return problemResponse($msg, 400, $request);
            }
            $transaction = new InterswitchTransactions($user, null, $request->transaction_id);
            //Check transaction status
            $response = $transaction->cancelPaycode();

            if($request['status'] = 'queued') {
                $message = "Paycode cancellation added to queue";
                $transformer = new CardlessWithdrawalTransformer();
                $cw = $transformer->transform($reference_transaction);
                $data = [
                    'cardless_withdrawal' => $cw
                ];
                return validResponse("Paycode Cancellation is being processed", $data, $request);
            }
            elseif ($response['status'] == true) {
                $wallet = Wallet::where('user_id', $user->id)->first();
                $wallet->update([
                    "amount" => $wallet->amount + $reference_transaction->amount
                ]);
                $message = "Paycode canceled successfully";
                $wallet_transaction_logs = [
                    'user_id' => $user->id,
                    'response_code' => $reference_transaction->reference_code,
                    'response_type' => SUCCESS_TRANSACTION,
                    'message' => $message,
                    'transaction_type_id' => Meta::CANCEL_CARDLESS_WITHDRAWAL,
                    'amount' => $reference_transaction->amount,
                    'metadata' => json_encode($response['data'])
                ];
                saveWalletLog($wallet_transaction_logs);
                $transformer = new CardlessWithdrawalTransformer();
                $cw = $transformer->transform($reference_transaction);
                $data = [
                    'cardless_withdrawal' => $cw
                ];
                return validResponse("Paycode Canceled", $data, $request);
            }else{
                $message = 'Paycode cancelling failed';
                if(isset($response['message'])){
                    $message = 'Paycode cancelling failed: '.$response['message'];
                }
                $wallet_transaction_logs = [
                    'user_id' => $user->id,
                    'response_code' => '#####',
                    'response_type' => SUCCESS_TRANSACTION,
                    'message' => $message,
                    'transaction_type_id' => Meta::CHECK_WITHDRAWAL_CODE_STATUS,
                    'amount' => 0,
                    'metadata' => (!empty($response['errors'])) ? json_encode($response['errors']) : json_encode($response)
                ];
                saveWalletLog($wallet_transaction_logs);
                if(isset($response['message'])){
                    $message = $response['message'];
                    return problemResponse($message, 401, $request);
                }else{
                    $message = 'Service Currently Unavailable, please try again later';
                    return problemResponse($message, 500, $request);
                }
            }

        } catch (ValidationException $e){
            $errors = $e->errors();
            $errors = array_flatten(array_values((array)$errors));
            $message = "Data sent failed to pass validation. " . implode(' ', $errors);

            return problemResponse($message, '400', $request);
        } catch (\Exception $e){
            $msg = 'Service Currently Unavailable try later';
            return problemResponse($msg, 500, $request, $e);
        }
    }


}
