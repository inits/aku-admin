<?php

namespace App\Http\Controllers;

use App\Models\CustomService;
use Illuminate\Http\Request;

class LoanProviderController extends Controller
{
    public function tradermoni(Request $request)
    {
        $tradermoni = CustomService::where('name', 'tradermoni')->first();
        return view('settings.tradermoni_settings', compact('tradermoni'));
    }

    public function save_tradermoni_status(Request $request)
    {
        $this->validate($request, [
            'tradermoni_service' => 'required'
        ]);

        if($request->tradermoni_service != 'on' && $request->tradermoni_service != 'off')
        {
            return redirect()->back()->with(error, 'Expecting tradermoni_service to either be "on" or "off", '.$request->tradermoni_service .' given');
        }
        if($request->tradermoni_service == 'on'){
            CustomService::tradermoni_service_on();
        }else{
            CustomService::tradermoni_service_off();
        }
        return redirect()->back()->with('success', 'Tradermoni Service Status Updated Successfully');
    }

    public function whitelisted_users(Request $request, CustomService $custom_service)
    {

    }

}
