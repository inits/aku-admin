<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     consumes={"application/x-www-form-urlencoded"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Fanta API",
 *         description="This is the API service for the Fanta app",
 *         @SWG\Contact(
 *             email="george@initsng.com"
 *         ),
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find out more about Fanta",
 *         url="http://fanta.inits.xyz"
 *     )
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $years = array('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec');

    public function treatChartData($params)
    {
        $y = $this->years;
        $gReport = array();
        foreach ($params as $rp) {
            $gReport[$rp[0]['month_name']] = $rp[0]['count'];
        }

        foreach ($y as $key => $value) {
            $number = $key + 1;
            if (isset($gReport[$number])) {
                $gReportData[] = $gReport[$number];
            } else {
                $gReportData[] = 0;
            }
        }
        $barData = "[" . implode(',', $gReportData) . "]";
        return $barData;
    }

}
