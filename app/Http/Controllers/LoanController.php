<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;
use DB;

class LoanController extends Controller
{
    
    public function index(Request $request)
    {
        if($request->ajax()){
            $q = $request->search_input;
            if($q != ""){
                $loans = DB::table('loans AS l')
                    ->select('l.id','lp.name','u.phone','l.amount','ls.name as status')
                    ->join('users AS u', 'user_id', '=', 'u.id')
                    ->join('loan_providers AS lp', 'loan_provider_id', '=', 'lp.id') 
                    ->join('loan_status AS ls', 'l.status', '=', 'ls.value') 
                    ->where('lp.name', 'LIKE', '%' . $q . '%' )
                    ->orWhere('u.phone', 'LIKE', '%' . $q . '%' )
                    ->orWhere('ls.name', 'LIKE', '%' . $q . '%' )
                    ->orWhere('l.amount', 'LIKE', '%' . $q . '%' )->paginate(20)->setPath ('');
                    $pagination = $loans->appends(array('search_input' => $request->search_input));
            }
            return view ('loans.ajax_table',compact('loans'))->withQuery($q);
        }
        $loans = DB::table('loans AS l')
                ->select('l.id','lp.name','u.phone','l.amount','ls.name as status')
                ->join('users AS u', 'user_id', '=', 'u.id')
                ->join('loan_providers AS lp', 'loan_provider_id', '=', 'lp.id')
                ->join('loan_status AS ls', 'l.status', '=', 'ls.value')
                ->paginate(20)->setPath ('');
        return view('loans.list', compact('loans'));
    }
}
