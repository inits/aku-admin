<?php

namespace App\Http\Controllers;

use App\Models\CardlessWithdrawal;
use App\Models\Loan;
use App\Models\TransactionType;
use App\Models\Wallet;
use App\Models\WalletTransactionLog;
use Illuminate\Http\Request;
use App\Models\TransactionLog;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $q = $request->search_input;

            $transactions = DB::table('wallet_transaction_logs AS t')
                ->select('t.id', 't.response_code', 'tt.name', 'u.phone', 't.amount', 't.response_type', 't.created_at', 't.message')
                ->join('users AS u', 'user_id', '=', 'u.id')
                ->join('transaction_types AS tt', 'transaction_type_id', '=', 'tt.id')
                ->where('t.response_code', 'LIKE', '%' . $q . '%')
                ->orWhere('u.phone', 'LIKE', '%' . $q . '%')
                ->orWhere('t.response_type', 'LIKE', '%' . $q . '%')
                ->orWhere('t.amount', 'LIKE', '%' . $q . '%')
                ->orWhere('t.message', 'LIKE', '%' . $q . '%')
                ->paginate(20)->appends(['search_input' => $q]);

            return view('transactions.ajax_table', compact('transactions'));
        }
        $transactions = DB::table('wallet_transaction_logs AS t')
            ->select('t.id', 't.response_code', 'tt.name', 'u.phone', 't.amount', 't.response_type', 't.created_at', 't.message')
            //->select('t.id','t.reference','tt.name','u.phone','t.amount','t.status','t.transaction_date', 't.message')
            ->join('users AS u', 'user_id', '=', 'u.id')
            ->join('transaction_types AS tt', 'transaction_type_id', '=', 'tt.id')
            ->paginate(20);

        return view('transactions.list', compact('transactions'));
    }

    public function bankTransactions(Request $request)
    {
        if ($request->ajax()) {
            $q = $request->search_input;

            $transactions = DB::table('wallet_transaction_logs AS t')
                ->select('t.id', 't.response_code', 'tt.name', 'u.phone', 't.amount', 't.response_type', 't.created_at', 't.message')
                ->join('users AS u', 'user_id', '=', 'u.id')
                ->join('transaction_types AS tt', 'transaction_type_id', '=', 'tt.id')
                ->where('t.response_code', 'LIKE', '%' . $q . '%')
                ->orWhere('u.phone', 'LIKE', '%' . $q . '%')
                ->orWhere('t.response_type', 'LIKE', '%' . $q . '%')
                ->orWhere('t.amount', 'LIKE', '%' . $q . '%')
                ->orWhere('t.message', 'LIKE', '%' . $q . '%')
                ->paginate(20);

            return view('transactions.ajax_table', compact('transactions'));
        }
        $transactions = DB::table('wallet_transaction_logs AS t')
            ->select('t.id', 't.response_code', 'tt.name', 'u.phone', 't.amount', 't.response_type', 't.created_at', 't.message')
            //->select('t.id','t.reference','tt.name','u.phone','t.amount','t.status','t.transaction_date', 't.message')
            ->join('users AS u', 'user_id', '=', 'u.id')
            ->join('transaction_types AS tt', 'transaction_type_id', '=', 'tt.id')
            ->paginate(20);

        return view('transactions.list', compact('transactions'));

    }

    public function sort(Request $request)
    {
        $currentMonth = date('m/1/Y');
        $currentDayTime = date('m/j/Y');

        if ($request->method() == "POST") {
            $requestData = $request->all();
            $from = date('Y-m-d', strtotime($requestData['from']));
            $to = date('Y-m-d', strtotime($requestData['to']));

            $i = date('m/j/Y', strtotime($from));
            $e = date('m/j/Y', strtotime($to));

        } else {
            $from = date('Y-m-d', strtotime($currentMonth));
            $to = date('Y-m-d', strtotime($currentDayTime));

            $i = date('m/j/Y', strtotime($currentMonth));
            $e = date('m/j/Y', strtotime($currentDayTime));
        }
        $f = $from . ' 00:00:00';
        $t = $to . ' 23:59:59';

        $datepickerFormatCurrentMonth = $i;
        $datepickerFormatCurrentDayTime = $e;

        $transactionDetails = TransactionType::pluck('name', 'id');
        $wallletlogsByType = WalletTransactionLog::groupBy('transaction_type_id')
            ->selectRaw('COUNT(transaction_type_id) AS transaction_type, transaction_type_id')
            ->whereBetween('created_at', array($f, $t))
            ->pluck('transaction_type', 'transaction_type_id');

        $walletsLgas = Wallet::select(DB::raw('COUNT(id) AS count ,SUM(amount) AS amount_to_paid'))
            //->whereYear('created_at', $currentYear)
            ->whereBetween('created_at', array($f, $t))
            ->get()
            ->toArray();
        $amountToPay = end($walletsLgas);


        $rawLgas = WalletTransactionLog::select(DB::raw('COUNT(id) AS count, MONTH(created_at) AS month_name, SUM(amount) AS amount_paid'))
            ->where('response_type', SUCCESS_TRANSACTION)
//            ->whereYear('created_at', $currentYear)
//            ->groupBy('created_at')
            ->whereBetween('created_at', array($f, $t))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get()
            ->toArray();

        //dd($rawLgas);

        $failedRawLgas = WalletTransactionLog::select(DB::raw('COUNT(id) AS count, MONTH(created_at) AS month_name, SUM(amount) AS amount_paid'))
            ->where('response_type', FAILED_TRANSACTION)
            ->orWhere('transaction_type_id', 2)
//            ->whereYear('created_at', $currentYear)
//            ->groupBy('created_at')
            ->whereBetween('created_at', array($f, $t))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get()
            ->toArray();

        $failedRawLgas = array_group_by($failedRawLgas, 'month_name');
        $rawLgas = array_group_by($rawLgas, 'month_name');

        $walletTransactionData = $this->treatChartData($rawLgas);
        $failedRawLgas = $this->treatChartData($failedRawLgas);

        $gReport = [];
        foreach ($rawLgas as $rp) {
            $gReport[$rp[0]['month_name']] = $rp[0]['count'];
        }
        $amountPaidTotalGet = [];
        foreach ($rawLgas as $rawPaymentga) {
            $amountPaidTotalGet[] = $rawPaymentga[0]['amount_paid'];
        }

        $amountPaidTotal = array_sum($amountPaidTotalGet);
        $users = DB::table('users')->whereBetween('created_at', array($f, $t))->count();


        //cardless withdraw
        $cardlessW = CardlessWithdrawal::select(DB::raw('COUNT(id) AS count, MONTH(created_at) AS month_name, SUM(amount) AS amount'))
            ->whereBetween('created_at', array($f, $t))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get()
            ->toArray();

        $allCardlessWCraeted = [];
        $allCardlessWAmount = [];
        foreach ($cardlessW as $item) {
            $allCardlessWCraeted[] = $item['count'];
            $allCardlessWAmount[] = $item['amount'];
        }

        $cardLessWithdrawCreated = array_sum($allCardlessWCraeted);
        $allCardlessWithdrawAmount = array_sum($allCardlessWAmount);

        return view('transactions.sort', compact(
            'datepickerFormatCurrentMonth', 'datepickerFormatCurrentDayTime', 'currentMonth', 'failedRawLgas', 'users', 'walletTransactionData', 'allCardlessWithdrawAmount',
            'currentDayTime', 'wallletlogsByType', 'transactionDetails', 'amountToPay', 'amountPaidTotal', 'cardlessW', 'allCardlessWAmount', 'cardLessWithdrawCreated'
        ));
    }

    public function loans(Request $request) {
        $loanData = Loan::paginate(100);
        return view('transactions.loan', compact('loanData'));
    }
}
