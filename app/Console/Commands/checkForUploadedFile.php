<?php

namespace App\Console\Commands;

use App\Helper\Meta;
//use Cyberduck\LaravelExcel\Exporter\Excel;
use App\Models\Loan;
use App\Models\SmsQueue;
use App\Models\Unsave;
use App\Models\Wallet;
use App\Upload;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

//use Maatwebsite\Excel\Facades\Excel;

class checkForUploadedFile extends Command
{
    public $fileId = false;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkForUploadedFile:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for uploaded files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = DB::table('uploads')->where('process', '=', Meta::PENDING)->first();
        if (empty($file) || is_null($file)) {
            ddd('Nothing to do');
        }

        $path = "storage/files/" . $file->upload_name;
        $this->fileId = $file->id;

        self::updateUploadQueue($file->id, "File uploading", Meta::UPLOADING);

        Excel::filter('chunk')->load($path)->chunk(50, function ($results) use ($file) {

            foreach ($results as $row) {
                $requestDataSet = (Array)$row;
                $requestDataInsert = (Array)$requestDataSet;
                $requestData = end($requestDataInsert);
                $phone = $requestData['phone_number'];

                if (isset($phone) && !empty($phone)) {
                    if ((strlen(trim(" " . $phone)) == 11)) {
                        $phone = cleanUpPhone($phone);

                        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
                        try {
                            $phone = $phoneUtil->parse($phone, "NG");
                        } catch (\libphonenumber\NumberParseException $e) {
                            checkForUploadedFile::updateUploadQueue($file->id, "'phone_number' Invalid AccountNumberToCredit or Phone number", Meta::ERROR);
                            ddd('phone_number invalid');
                        }
                        $phone = $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::E164);

                        if (!isset($requestData['firstname']) || empty($requestData['firstname'])) {
                            // die here
                            checkForUploadedFile::updateUploadQueue($file->id, "'firstname' field does not exits", Meta::ERROR);
                            ddd('firstname not set');
                        }
                        if (!isset($requestData['lastname']) || empty($requestData['lastname'])) {
                            // die here
                            checkForUploadedFile::updateUploadQueue($file->id, "'lastname' field does not exits", Meta::ERROR);
                            ddd('lastname not set');
                        }

                        if (!isset($requestData['clean_market_location']) || empty($requestData['clean_market_location'])) {
                            // die here
                            checkForUploadedFile::updateUploadQueue($file->id, "'clean_market_location' field does not exits", Meta::ERROR);
                            ddd('clean_market_location not set');
                        }

                        if (!isset($requestData['data_source']) || empty($requestData['data_source'])) {
                            // die here
                            checkForUploadedFile::updateUploadQueue($file->id, "'data_source' field does not exits", Meta::ERROR);
                            ddd('data_source not set');
                        }

                        $full_name = $requestData['firstname'] . ' ' . $requestData['lastname'];

                        if (isset($requestData['state']) && !empty($requestData['state'])) {
                            $state = $requestData['state'];
                        } elseif (isset($requestData['clean_state']) && !empty($requestData['clean_state'])) {
                            $state = $requestData['clean_state'];
                        } else {
                            // die here no state
                            checkForUploadedFile::updateUploadQueue($file->id, "'state' field does not exits", Meta::ERROR);
                            ddd('state not set');
                        }

                        $otp = str_shuffle("" . substr(time(), -6));
                        $userData = [
                            'email' => substr($phone, -10) . '@inits.xyz',
                            'phone' => $phone,
                            'current_otp' => $otp,
                            'status' => Meta::ACCOUNT_ACTIVE,
                            'is_phone_verified' => Meta::ACCOUNT_ACTIVE,
                            'name' => $full_name,
                            'clean_market_location' => $requestData['clean_market_location'],
                            'state' => $state,
                            'data_source' => $requestData['data_source'],
                            'tradermoni_whitelisted' => 1,
                        ];

                        $user = DB::table('users')->where('phone', '=', $phone)->first();
                        if (empty($user) || !isset($user) || is_null($user)) {

                            $userRes = DB::table('users')->insertGetId($userData);

                            //if (isset($userRes) && !empty($userRes)) {
                            $amount = '10000';
                            $ref = checkForUploadedFile::generateRandomString(8);

                            $reasons = 'GEEP loan disbursement transfer, from to ' . $phone . ' on ' . date("'Y-m-d H:i:s'") . ' ref : ' . $ref;
                            Loan::create([
                                'user_id' => $userRes,
                                'loan_provider_id' => Meta::BOI,
                                'amount' => $amount,
                                'balance' => $amount,
                                'reason' => $reasons,
                                'meta' => json_encode([]),
                                'status' => 0,
                                'interest' => 0,
                                'reference' => $ref,
                                'provider_status' => 0,
                            ]);

                            Wallet::create(['user_id' => $userRes, 'amount' => $amount]);

                            SmsQueue::create([
                                'phone' => $phone,
                                'message' => 'You just received N' . number_format($amount) . ' from TraderMoni, to access your cash on AKU, please dial *347*111# or call 07001000258',
                            ]);

                            var_dump($phone . ' phone number / user added');

                        } else {
                            Unsave::create([
                                'phone' => $phone,
                                'reason' => 'duplicated Phone number/user',
                            ]);
                            var_dump($phone . ' duplicated Phone number');
                        }

                    } else {
                        // error number is more then 11 digits
                        Unsave::create([
                            'phone' => $phone,
                            'reason' => 'Phone number is more/less than 11 digits',
                        ]);
                        var_dump($phone . ' Phone number is more/less than 11 digits');
                    }
                } else {
                    // error number missing
                    Unsave::create([
                        'phone' => $phone,
                        'reason' => "phone_number empty",
                    ]);
                    var_dump($phone . ' phone_number empty');
                }
            }
        });

        checkForUploadedFile::updateUploadQueue($file->id, "File uploaded ", Meta::DONE_UPLOADING);
        var_dump('Upload done');
        //DB::table('uploads')->delete(4);
    }

    public static function updateUploadQueue($id, $message, $process)
    {
        DB::table('uploads')->where('id', $id)->update([
            'message' => $message,
            'process' => $process,
        ]);
        var_dump('updateUploadQueue');
    }

}
