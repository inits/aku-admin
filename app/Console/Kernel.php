<?php

namespace App\Console;

use App\Classes\InterswitchTransactions;
use App\Models\SmsQueue;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Classes\BankTransfer;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        '\App\Console\Commands\checkForUploadedFile',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('checkForUploadedFile:upload')
            ->everyFiveMinutes();

        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            BankTransfer::processTransfers();
        })->everyMinute()->name('Bank Transfers')->withoutOverlapping();

        $schedule->call(function () {
            processSmsQueue();
        })->everyMinute()->name('SMS queue')->withoutOverlapping();

        $schedule->call(function (){
            InterswitchTransactions::checkExpiredPaycodes();
        })->everyMinute()->name('Check Expired Paycodes')->withoutOverlapping();

        $schedule->call(function (){
            InterswitchTransactions::checkCancelledPaycodes();
        })->everyMinute()->name('Check Cancelled Paycodes')->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
